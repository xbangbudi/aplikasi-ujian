<!DOCTYPE html>
<html class="no-js">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Registrasi</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- ============================================ -->
        <link rel="stylesheet" href="kit/css/bootstrap.css">
    <!-- ============================================ -->
        <link rel="stylesheet" href="kit/css/font-awesome.css">
    <!-- ============================================ -->
        <link rel="stylesheet" href="kit/css/hover.css">
    <!-- ============================================ -->
        <link rel="stylesheet" href="kit/css/material-design-iconic-font.min.css">
    <!-- ============================================ -->
        <link rel="stylesheet" href="kit/css/util.css">
        <link rel="stylesheet" href="kit/css/main.css">
    <!-- ============================================ -->
    <script src="https://www.google.com/recaptcha/api.js" async defer></script>
    </head>
    <?php
    include "../config/koneksi.php";
    $site_key = '6Lce9dQUAAAAAH-NEnalQ6I4106Nmuv4lMsjurHH';
    $secret_key = '6Lce9dQUAAAAAAJIBtRTAnvhregdRzdFYp43eEUj';
    
    if(isset($_POST['daftar'])){
        if(isset($_POST['g-recaptcha-response']))
        {
            $api_url = 'https://www.google.com/recaptcha/api/siteverify?secret=' . $secret_key . '&response='.$_POST['g-recaptcha-response'];
            $response = file_get_contents($api_url);
            $data = json_decode($response, true);
            // var_dump($data);exit;
            $kd_ujian=$_POST['kd_ujian'];
            $kd_paket=$_POST['kd_paket'];
            $nama=$_POST['nama'];
            $email=$_POST['email'];
            $username=$_POST['username'];
            $pass=md5($_POST['pass']);
            $nohp=$_POST['nohp'];
            $status_pembayaran = 0;
            $status = 10;
            $aql=mysqli_query($conn,"SELECT kode_reg FROM user WHERE kode_reg='$username' ");
            if($data['success'] == true){
                if(mysqli_num_rows($aql)){
                    echo "<script>alert('Gagal mendaftar Username $username sudah ada');</script>";
                }else{
                    $sql=mysqli_query($conn,"INSERT INTO user (kode_reg, password, nama, email, no_hp, kd_ujian, kd_paket, status_pembayaran, status) 
                                        VALUES ('$username','$pass', '$nama','$email','$nohp','$kd_ujian','$kd_paket','$status_pembayaran','$status')");
                    if($sql){
                    echo "<script> alert('Berhasil Mendaftar'); document.location.href = '../siswa/';</script>";
                    }
                    else{
                        echo "<script> alert('Terjadi Kesalahan');</script>";
                    }
                }
            }else{
                $success = false;
            }
    }
}

        if(isset($_GET['kd_ujian']) && isset($_GET['kd_paket'])){
            $kd_ujian = $_GET['kd_ujian'];
            $kd_paket = $_GET['kd_paket'];
        }else{
            echo "PARAMETER TIDAK ADA";die;
        }
    ?>
    <body>
    <div class="limiter">
		<div class="container-login100">
			<div class="login100-more" style="background-image: url('images/img-register2.jpg');"></div>

			<div class="wrap-login100 p-l-50 p-r-50 p-t-72 p-b-50">
                <?php if(isset($success)){ 
                        if (!$success) {?>
                    <div class="alert alert-danger">Captcha Tidak Boleh Kosong!</div>
                <?php }
                } ?>
				<form class="login100-form validate-form" action="" method="POST">
					<span class="login100-form-title p-b-59">
						Buat Akun Anda Sekarang
					</span>
                    <input type="hidden" name="kd_ujian" value="<?= $kd_ujian ?>">
                    <input type="hidden" name="kd_paket" value="<?= $kd_paket ?>">

					<div class="wrap-input100 validate-input" data-validate="Nama Lengkap tidak boleh kosong">
						<span class="label-input100">Nama Lengkap</span>
						<input class="input100" type="text" name="nama" placeholder="Nama...">
						<span class="focus-input100"></span>
					</div>

					<div class="wrap-input100 validate-input" data-validate = "E-mail tidak valid">
						<span class="label-input100">Email</span>
						<input class="input100" type="text" name="email" placeholder="Email...">
						<span class="focus-input100"></span>
					</div>

					<div id="val" class="wrap-input100 validate-input" data-validate="Username tidak boleh kosong">
						<span class="label-input100">Username</span>
						<input id="vals" class="input100" type="text" name="username" placeholder="Username...">
						<span class="focus-input100"></span>
					</div>

					<div class="wrap-input100 validate-input" data-validate = "Password tidak boleh kosong">
						<span class="label-input100">Password</span>
						<input class="input100" type="password" name="pass" placeholder="*************">
						<span class="focus-input100"></span>
                    </div>
                    
                    <div class="wrap-input100 validate-input" data-validate="No. Handphone tidak boleh kosong">
						<span class="label-input100">No. Handphone</span>
						<input class="input100" type="number" name="nohp" placeholder="No. Handphone...">
						<span class="focus-input100"></span>
                    </div>
                    <div class="g-recaptcha" data-sitekey="6Lce9dQUAAAAAH-NEnalQ6I4106Nmuv4lMsjurHH"></div><br/>
					<div class="container-login100-form-btn">
						<div class="wrap-login100-form-btn">
							<div class="login100-form-bgbtn"></div>
							<button type="submit" name="daftar" class="login100-form-btn">
								Daftar Sekarang
							</button>
						</div>
					</div>
				</form>
			</div>
		</div>
    </div>
    <!-- ============================================ -->
        <script src="kit/js/bootstrap.js" async defer></script>
    <!-- ============================================ -->
        <script src="kit/js/jquery.js" async></script>
    <!-- ============================================ -->
        <script src="jquery.js" async></script>
    <!-- ============================================ -->
        <!-- <script src="kit/js/ajax.js"></script> -->
    <!-- ============================================ -->
        <script src="kit/js/main.js" async></script>
        <!-- <script src="kit/js/map-custom.js"></script> -->
    <!-- ============================================ -->
    </body>
</html>