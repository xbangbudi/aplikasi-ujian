<!DOCTYPE html>
<html class="no-js">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Registrasi</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="kit/css/bootstrap.css">
        <link rel="stylesheet" href="kit/css/font-awesome.css">
        <link rel="stylesheet" href="kit/css/hover.css">
        <style>
            body {
                font-family: calibri;
            }

            .kotak {
                border: 1px solid #f3f3f3;
                padding: 20px;
                border-radius: 3px;
                margin-top: -15px;
                transition: 0.5s;
            }

            .kotak:hover {
                box-shadow: 0 5px 10px black;
                transition: 0.5s;
            }

            #tombol {
                margin-bottom: 10px;
            }

            .textbox {
                width: 250px;
                height: 40px;
                margin-bottom: 15px;
                border: 1px solid #f3f3f3;
                transition: 0.5s;
            }

            .textbox:focus {
                box-shadow: 0 2px 1px blueviolet;
                transition: 0.2s;
            }
        </style>
    </head>
    <body>
        <div
            class="jumbotron jumbotron-fluid"
            style="text-align: center; background-color: blueviolet; color: white;">
            <h2>Halaman Pendaftaran</h2>
        </div>
        <div class="d-flex justify-content-center">
            <div class="kotak">
                <form action="daftar.php" method="POST">
                    <label>
                        <b>Nama Lengkap
                        </b><br>
                        <input class="textbox" type="text" name="nama" placeholder="Contoh: Muhammad Habib" required="required"></label><br>
                    <label>
                        <b>Email
                        </b><br>
                        <input class="textbox" type="email" name="email" placeholder="Misalkan: habib@gmail.com" required="required"></label><br>

                    <!-- <label> <b>Password </b><br> <input class="textbox" type="text"
                    name="password" placeholder="Misalkan: H@b1B" required="required"></label><br>
                    -->
                    <label>
                        <b>Nomor Handphone
                        </b><br>
                        <input class="textbox" type="text" name="hp" placeholder="Misalkan: 0813 6212 3120" required="required"></label><br>
                    <label>
                        <pre>

                        </pre>
                    </label>
                    <br>
                    <div class="pull-right">
                        <button type="reset" class="btn btn-danger">Reset</button>
                        <button type="submit" name="daftar" class="btn btn-primary">Daftar</button>
                    </div>

                </form>
            </span>
        </div>

        <script src="kit/js/bootstrap.js"></script>
        <script src="kit/js/jquery.js"></script>
        <script src="jquery.js"></script>
        <script src="kit/js/ajax.js"></script>
    </body>
</html>