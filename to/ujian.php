<?php

?>
<!DOCTYPE html>
<html class="no-js">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="../kit/css/bootstrap.css">
        <link rel="stylesheet" href="../kit/css/font-awesome.css">
        <link rel="stylesheet" href="../kit/css/hover.css">
        <style>
            .kotak {
                border: 1px solid #f3f3f3;
                padding: 9px;
                border-radius: 3px;
                margin-top: -15px;
            }

            #tombol{
                margin-bottom: 10px;
            }
        </style>
    </head>
    <body>
        <div
            class="jumbotron jumbotron-fluid"
            style="text-align: center; background-color: blueviolet; color: white; font-family: calibri;">
            <h2>Aplikasi Ujian</h2>
        </div>
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-9">
                    <div class="kotak">
                        Bagan Soal
                        <br><br>
                        <?php
                        while ($rtwk=mysqli_fetch_array($qsoal_twk)) {
                            ?>
                                            <div id="soal" class="<?php echo $no; ?>">
                                                <?php echo $no.".".$rtwk['soal'];?><br>
                                                <input  type="radio" name="<?php echo $rtwk['id_soal']; ?>" value="a"><?php echo $rtwk['a']; ?><br>
                                                <input  type="radio" name="<?php echo $rtwk['id_soal']; ?>" value="b"><?php echo $rtwk['b']; ?><br>
                                                <input  type="radio" name="<?php echo $rtwk['id_soal']; ?>" value="c"><?php echo $rtwk['c']; ?><br>
                                                <input  type="radio" name="<?php echo $rtwk['id_soal']; ?>" value="d"><?php echo $rtwk['d']; ?><br>
                                                <input  type="radio" name="<?php echo $rtwk['id_soal']; ?>" value="e"><?php echo $rtwk['e']; ?>
                                            </div>
                            <?php
                            $no++;
                            }
                            while ($rtiu=mysqli_fetch_array($qsoal_tiu)) {
                                ?>
                                                <div id="soal" class="<?php echo $no; ?>">
                                                    <?php echo $no.".".$rtiu['soal'];?><br>
                                                    <input  type="radio" name="<?php echo $rtiu['id_soal']; ?>" value="a"><?php echo $rtiu['a']; ?><br>
                                                    <input  type="radio" name="<?php echo $rtiu['id_soal']; ?>" value="b"><?php echo $rtiu['b']; ?><br>
                                                    <input  type="radio" name="<?php echo $rtiu['id_soal']; ?>" value="c"><?php echo $rtiu['c']; ?><br>
                                                    <input  type="radio" name="<?php echo $rtiu['id_soal']; ?>" value="d"><?php echo $rtiu['d']; ?><br>
                                                    <input  type="radio" name="<?php echo $rtiu['id_soal']; ?>" value="e"><?php echo $rtiu['e']; ?>
                                                </div>
                                <?php
                                $no++;
                                }
                                while ($rtkp=mysqli_fetch_array($qsoal_tkp)) {
                                    ?>
                                                    <div id="soal" class="<?php echo $no; ?>">
                                                        <?php echo $no.".".$rtkp['soal'];?><br>
                                                        <input  type="radio" name="<?php echo $rtkp['id_soal']; ?>" value="a"><?php echo $rtkp['a']; ?><br>
                                                        <input  type="radio" name="<?php echo $rtkp['id_soal']; ?>" value="b"><?php echo $rtkp['b']; ?><br>
                                                        <input  type="radio" name="<?php echo $rtkp['id_soal']; ?>" value="c"><?php echo $rtkp['c']; ?><br>
                                                        <input  type="radio" name="<?php echo $rtkp['id_soal']; ?>" value="d"><?php echo $rtkp['d']; ?><br>
                                                        <input  type="radio" name="<?php echo $rtkp['id_soal']; ?>" value="e"><?php echo $rtkp['e']; ?>
                                                    </div>
                                    <?php
                                    $no++;
                                    }
                                    ?>
                        <br><br>
                        <a href="#" class="btn btn-success">Soal Berikutnya <i class="fa fa-arrow-right"></i></a>
                        <a href="#" class="btn btn-warning">Kumpulkan <i class="fa fa-check"></i></a>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="kotak">
                        <div class="kotak" style="margin-top: 15px; margin-bottom: 10px;">
                            Skor : 
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <a id="tombol" href="#" class="btn btn-primary">1</a>
                                <a id="tombol" href="#" class="btn btn-primary">2</a>
                                <a id="tombol" href="#" class="btn btn-primary">3</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <script src="../kit/js/bootstrap.js"></script>
        <script src="../kit/js/jquery.js"></script>
    </body>
</html>