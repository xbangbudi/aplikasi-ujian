$(document).ready(function(){
    $(".preloader").fadeOut();

    $('.kotak #soal:not(:first)').hide();
    $('#tombol a:first').addClass('active');

    
    $('#tombol a').click(function(){
        $('#tombol a').removeClass('active');
        $(this).addClass('active');
        $('.kotak #soal').hide();
        var target=$(this).attr('rel');
        $('.kotak .soal'+target).show();
    });
    $('#tombol a:not(:last)').click(function(){
        $('#next').removeClass('disabled');
    });
    $('#tombol a:last').click(function(){
        $('#next').addClass('disabled');
    });
    $('#next').click(function(){
        var n =$('#tombol .active').attr('rel');
        var s=parseFloat(n)+1;
        $('#tombol a').removeClass('active');
        $('.kotak #soal').hide();
        $('.kotak .soal'+s).show();
        $('#tombol a[rel="'+ s +'"]').addClass('active');
        if($('#tombol a:last').attr('rel')==s){
            $('#next').addClass('disabled');
        }
    });
    $('#kumpul').click(function(){
        $.ajax({
            url : 'konten/jawab.php',
            type : 'POST',
            data : $('#jawaban').serialize(),
            success:function(hasils){
                $('#hasilx').html(hasils);
                console.log('berhasil');
            },
            error: function(){
                alert("Terjadi kesalahan!");
            }
        });
    });
});
