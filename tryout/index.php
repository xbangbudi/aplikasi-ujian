<?php
include "../config/koneksi.php";
session_start();
if($_SESSION['status_login'] != 'success'){
    header("location:../siswa/index.php");
    // echo $_SESSION["status_pembayaran"];die();
}
if($_SESSION['status_bayar']!='true'){
    header("location:../siswa/dashboard");
}

    $username=$_SESSION['kode_reg'];
    $aql=mysqli_fetch_assoc(mysqli_query($conn,"SELECT id FROM user WHERE kode_reg='$username'"));
    $id_user=$aql['id'];
    $_SESSION['id_user']=$id_user;
        $kd_to=$_GET['to'];
        $kd_uji=$_SESSION['kd_ujian'];
        $_SESSION['kd_tryout']=$kd_to;

function countnum($aa)
{
    $conn=mysqli_connect("127.0.0.1","root","","ujian_1");
    $kd_uji=$_SESSION['kd_ujian'];
    $sqlrd=mysqli_query($conn,"SELECT a.*, b.* FROM ta_tryout a LEFT JOIN ref_soal b ON a.kd_soal=b.kd_soal AND a.kd_jenis=b.kd_jenis  WHERE kd_tryout='$aa' and kd_ujian='$kd_uji'");
    $count=mysqli_num_rows($sqlrd);
    return $count;
}
function cekjawaban($kd_soal,$kd_jenis,$jbs){
    $conn=mysqli_connect("127.0.0.1","root","","ujian_1");
    $kd_uji=$_SESSION['kd_ujian'];
    $id_user=$_SESSION['id_user'];
    $kd_to=$_SESSION['kd_tryout'];
    $sql=mysqli_query($conn,"SELECT list_id_soal, list_kd_jenis, list_kd_soal, list_jawaban from ta_riwayat where kd_ujian='$kd_uji' and id_user='$id_user' and kd_tryout='$kd_to'"); 
if(mysqli_num_rows($sql)){
    $ssql=mysqli_fetch_assoc(mysqli_query($conn,"SELECT id FROM ref_soal WHERE kd_soal='$kd_soal' AND kd_jenis='$kd_jenis'"));
$id_soal=$ssql['id'];
    $rd=mysqli_fetch_assoc($sql);
    $list_id_soal=$rd['list_id_soal'];
    $list_kd_jenis=$rd['list_kd_jenis'];
    $list_kd_soal=$rd['list_kd_soal'];
    $list_jawaban=$rd['list_jawaban'];
    if($list_id_soal!=""){
    $ar_id_soal=explode("|",$list_id_soal);
    $ar_kd_jenis=explode("|",$list_kd_jenis);
    $ar_kd_soal=explode("|",$list_kd_soal);
    $ar_jawaban=explode("|",$list_jawaban);
    $ids=array_search($id_soal,$ar_id_soal);
    if($jbs==$ar_jawaban[$ids]){
        echo "checked";
    }
    }
}
}
function tampilsoal($aa,$ab,$ac,$ad){
    $conn=mysqli_connect("127.0.0.1","root","","ujian_1");
    $kd_uji=$_SESSION['kd_ujian'];
    
     if($kd_uji==1){
         $rn=3;
     }
     elseif ($kd_uji==2) {
         $rn=5;
     }
     $num=1;
     $a=0;
     for ($i=1; $i <=$rn  ; $i++) { 
        $sqlrd=mysqli_query($conn,"SELECT a.*, b.*, c.keterangan FROM ta_tryout a LEFT JOIN ref_soal b ON a.kd_soal=b.kd_soal AND a.kd_jenis=b.kd_jenis LEFT JOIN ref_jenis_soal c ON b.kd_jenis=c.kd_jenis  WHERE kd_tryout='$aa' and kd_ujian='$kd_uji' and a.kd_jenis='$i'");
         if(mysqli_num_rows($sqlrd)){
            while ($rsoal=mysqli_fetch_array($sqlrd)) {
                ?>
<div id="tsoal" class="soal<?= $num ?>">
    <div class="header_soal">
        <b>Soal
            <?=  $num." - ".$rsoal['keterangan'] ?>
        </b>
    </div>
    <br>
    <div class="soal">
        <?=  $rsoal['soal'];?>
    </div>
    <br>
    <div class="row" style="padding: 20px;">
        <!-- Pilihan A -->
        <div class="col-sm-4">
            <input
                type="radio"
                class="input-hidden"
                name="jb<?=  $num; ?>" <?php cekjawaban($rsoal['kd_soal'],$rsoal['kd_jenis'],"a") ?>
                id="jba<?=  $num; ?>"
                value="a_<?=  $rsoal['kd_soal']."_".$rsoal['kd_jenis'] ?>">
            <label for="jba<?=  $num; ?>">
                <img src="img/a.png" alt="A" style="width: 20px; height: 20px;"/>
                <?=  $rsoal['a']; ?>
            </label><br>
        </div>

        <!-- Pilihan B -->
        <div class="col-sm-4">
            <input
                type="radio"
                class="input-hidden"
                name="jb<?=  $num; ?>" <?php cekjawaban($rsoal['kd_soal'],$rsoal['kd_jenis'],"b") ?>
                id="jbb<?=  $num; ?>"
                value="b_<?=  $rsoal['kd_soal']."_".$rsoal['kd_jenis'] ?>">
            <label for="jbb<?=  $num; ?>">
                <img src="img/b.png" alt="B" style="width: 20px; height: 20px;"/>
                <?=  $rsoal['b']; ?>
            </label><br>
        </div>

        <!-- Pilihan C -->
        <div class="col-sm-4">
            <input
                type="radio"
                class="input-hidden"
                name="jb<?=  $num; ?>" <?php cekjawaban($rsoal['kd_soal'],$rsoal['kd_jenis'],"c") ?>
                id="jbc<?=  $num; ?>"
                value="c_<?=  $rsoal['kd_soal']."_".$rsoal['kd_jenis'] ?>">
            <label for="jbc<?=  $num; ?>">
                <img src="img/c.png" alt="C" style="width: 20px; height: 20px;"/>
                <?=  $rsoal['c']; ?>
            </label><br>
        </div>

        <!-- Pilihan D -->
        <div class="col-sm-4">
            <input
                type="radio"
                class="input-hidden"
                name="jb<?=  $num; ?>" <?php cekjawaban($rsoal['kd_soal'],$rsoal['kd_jenis'],"d") ?>
                id="jbd<?=  $num; ?>"
                value="d_<?=  $rsoal['kd_soal']."_".$rsoal['kd_jenis'] ?>">
            <label for="jbd<?=  $num; ?>">
                <img src="img/d.png" alt="D" style="width: 20px; height: 20px;"/>
                <?=  $rsoal['d']; ?>
            </label><br>
        </div>

        <!-- Pilihan E -->
        <div class="col-sm-4">
            <input
                type="radio"
                class="input-hidden"
                name="jb<?=  $num; ?>" <?php cekjawaban($rsoal['kd_soal'],$rsoal['kd_jenis'],"e") ?>
                id="jbe<?=  $num; ?>"
                value="e_<?=  $rsoal['kd_soal']."_".$rsoal['kd_jenis'] ?>">
            <label for="jbe<?=  $num; ?>">
                <img src="img/e.png" alt="E" style="width: 20px; height: 20px;"/>
                <?=  $rsoal['e']; ?>
            </label>
        </div>
    </div>
</div>
<?php
                            $num++; $a++;
            } 
         }
     }
     $ba=$a;
 }
?>
<!DOCTYPE html>

<html class="no-js">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="kit/css/bootstrap.css">
        <link rel="stylesheet" href="kit/css/font-awesome.css">
        <link rel="stylesheet" href="kit/css/hover.css">
        <style>

            body {
                font-family: calibri;
                background-color: #347aeb;
            }

            .kotak {
                border: 1px solid #f3f3f3;
                padding: 9px;
                border-radius: 5px;
                margin-top: -15px;
                box-shadow: 1px 3px 5px;
                background-color: white;
            }

            .nilai {
                border: 1px solid #dd1e31;
                background-color: #dd1e31;
                padding: 9px;
                border-radius: 5px;
                margin-top: -15px;
                color: white;
            }

            .scroll {
                width: 100%;
                background-color: whitesmoke;
                padding: 10px;
                overflow: auto;
                height: 300px;
                border-radius: 5px;
            }

            #tombol {
                margin-bottom: 10px;
            }

            .opsi {
                width: 100px;
            }

            .input-hidden {
                position: absolute;
                left: -9999px;
            }

            input[type=radio]:checked + label > img {
                border: 0 solid #fff;
                border-radius: 100%;
                box-shadow: 0 0 3px 3px blue;
            }

            input[type=radio] + label > img {
                border: 0 dashed #444;
                width: 150px;
                height: 150px;
                transition: 500ms all;
            }

            input[type=radio]:checked + label > img {
                transform: rotateZ(-10deg) rotateX(10deg);
            }

            .header_soal {
                border: 1px solid #347aeb;
                background-color: #347aeb;
                padding: 9px;
                border-radius: 10px;
                color: white;
                text-align: center;
            }

            .soal {
                padding: 20px;
            }
            .preloader {
                position: fixed;
                top: 0;
                left: 0;
                width: 100%;
                height: 100%;
                z-index: 9999;
                background-color: #fff;
            }
            .preloader .loading {
                position: absolute;
                left: 50%;
                top: 50%;
                transform: translate(-50%,-50%);
                font: 14px arial;
            }
        </style>
    </head>
    <body>
        <div class="preloader"> <div class="loading"> <img
        src="konten/preloader.gif" width="80"> <p>Harap Tunggu</p> </div> </div>
    <?php
    if(isset($_SESSION["mulai_waktu"])){
        $telah_berlalu = time() - $_SESSION["mulai_waktu"];
        if($telah_berlalu==0){
            $_SESSION['st']=="habis";
        }
        }
       else {
        $_SESSION["mulai_waktu"] = time();
        $_SESSION['st']= "mulai";
        $telah_berlalu = 0;
    }
        ?>

        <div
            class="jumbotron jumbotron-fluid"
            style="text-align: center; background-color : #347aeb; color: white; font-family: calibri;">
            <hr style="height: 20px; background-color: white;">
            <hr style="height: 5px; background-color: white; margin-top: -10px;">
            <h2><img src="img/logo2.png" style="margin-top: -125px;"></h2>
        </div>
        <div class="container-fluid" style="margin-top: -50px;">
            <div class="row">
                <!-- Bilah Kiri -->
                <div class="col-sm-9">
                    <form id="jawaban" name="jawaban" action="" method="POST">
                        <div class="kotak">
                        <?php
                        
        $sql_riwayat=mysqli_query($conn,"SELECT * FROM ta_riwayat where id_user='$id_user' and kd_ujian='$kd_uji' and kd_tryout='$kd_to'");
        if(mysqli_num_rows($sql_riwayat)){
            $rds=mysqli_fetch_assoc($sql_riwayat);
            tampilsoal($kd_to, $rds['list_kd_jenis'],$rds['list_kd_soal'],$rds['list_jawaban']);
            $wto=$rds['sisa_waktu'];
            $tm=explode(":",$wto);
            $jam=$tm[0];
                    $mnt=$tm[1];
                    $dt=$tm[2];
                    $tjam=$jam * 3600;
                    $tmnt=$mnt * 60;
                    $twaktu=$tjam+$tmnt+$dt;
        }
        else{
            $time=mysqli_query($conn,"SELECT jam FROM ref_tryout WHERE kd_tryout='$kd_to'");
            $rd=mysqli_fetch_assoc($time);
            $jams=$rd['jam'];
            $tm=explode(":",$jams);
            $jam=$tm[0];
                    $mnt=$tm[1];
                    $dt=$tm[2];
                    $tjam=$jam * 3600;
                    $tmnt=$mnt * 60;
                    $twaktu=$tjam+$tmnt+$dt;
            $riwayat_buat=mysqli_query($conn,"INSERT INTO ta_riwayat(kd_ujian, id_user, kd_tryout, sisa_waktu)VALUES ('$kd_uji','$id_user','$kd_to', '$jams') ") ;
            if($riwayat_buat){
                
                tampilsoal($kd_to,"","","");

            }
        }
                        ?>
                            <input type="hidden" id="ist" name="waktu">
                        </form>
                        <br><br>
                        <div style="text-align: right;">
                        <a id="jawab" rel="" href="#" class="btn btn-info">Jawab
                            </a>
                            <a id="next" href="#" class="btn btn-success">Soal Berikutnya
                                <i class="fa fa-arrow-right"></i>
                            </a>

                            <a
                                id="kumpul"
                                data-toggle="modal"
                                data-target="#myModal"
                                href="#"
                                class="btn btn-warning">Kumpulkan
                                <i class="fa fa-check"></i>
                            </a>
                        </div>
                    </div>

                </div>


            <!-- Bilah Kanan -->
            <div class="col-sm-3">
                <div class="kotak">
                    <div class="nilai" style="margin-top: 1px; margin-bottom: 10px;">
                        <b>Sisa Waktu :
                            <span id="timer"></span></b>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="scroll">
                                <div id="buton">
                                    <?php
                                    $ba=countnum($kd_to);
                                        for ($ax=1; $ax <= $ba; $ax++) { 
                                            ?>
                                    <a id="tombol" href="#" class="btn btn-primary" rel="<?=  $ax; ?>"><?=  $ax; ?></a>
                                    <?php
                                        }
                                        ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="myModal">
        <div class="modal-dialog modal-lg modal-dialog-centered">
            <div class="modal-content">

                <!-- Modal Header -->
                <div class="modal-header">
                    <h4 class="modal-title">Skor Hasil</h4>
                    <a
                        href=""
                        onclick=" return document.location.href = '../siswa/dashboard'"
                        type="button"
                        class="close"
                        data-dismiss="modal">&times;</a>
                </div>

                <!-- Modal body -->
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm">
                            <div class="kotak">
                                <!-- <h3>jawaban</h3> -->
                                <div id="hasilx">
                                    Mohon ditunggu...
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                    <!-- Modal footer -->
                    <div class="modal-footer">
                        <a
                            href=""
                            onclick=" return document.location.href = '../siswa/dashboard'"
                            class="btn btn-danger"
                            data-dismiss="modal">Tutup</a>
                    </div>
            </div>

        </div>
    </div>
        <script src="kit/js/bootstrap.js"></script>
        <script src="kit/js/jquery.js"></script>
        <script src="kit/countdown/js/jquery.plugin.min.js"></script>
        <script src="kit/countdown/js/jquery.countdown.js"></script>
        <script
            src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
        <script
            src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
        <script src="konten/ajax.js"></script>
        <script>
            function waktuHabis() {
                $('#myModal').modal('show');
                $.ajax({
                    url: 'konten/jawab.php',
                    type: 'POST',
                    data: $('#jawaban').serialize(),
                    success: function (hasils) {
                        $('#hasilx').html(hasils);
                        console.log('berhasil');
                    },
                    error: function () {
                        alert("Terjadi kesalahan!");
                    }
                });
            }
            function hampirHabis(periods) {
                var s = $("#timer").text();
                $("#ist").val(s);
                if ($.countdown.periodsToSeconds(periods) == 60) {
                    $(this).css({color: "yellow"});
                }
            }
            $(function () {
                var waktu = <?= $twaktu ?>; var sisa_waktu = waktu -
                <?= $telah_berlalu ?>; var longWayOff = sisa_waktu; $("#timer").countdown({
                    until: longWayOff,
                    compact: true,
                    onExpiry: waktuHabis,
                    onTick: hampirHabis
                });
            //  var s=$("#timer").text();
            //  $("#ist").val(s);
             })
        </script>
    </body>
</html>