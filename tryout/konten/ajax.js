$(document).ready(function(){
    $(".preloader").fadeOut();

    $('.kotak #tsoal:not(:first)').hide();
    $('#buton a:first').addClass('active');
    $('#jawab').attr('rel',1);

    
    $('#buton a').click(function(){
        $('#buton a').removeClass('active');
        $(this).addClass('active');
        var ab=$(this).attr('rel');
        $('#jawab').attr('rel',ab);
        $('.kotak #tsoal').hide();
        var target=$(this).attr('rel');
        $('.kotak .soal'+target).show();
    });
    $('#buton a:not(:last)').click(function(){
        $('#next').removeClass('disabled');
    });
    $('#buton a:last').click(function(){
        $('#next').addClass('disabled');
    });
    $('#next').click(function(){
        var n =$('#buton .active').attr('rel');
        var s=parseFloat(n)+1;
        $('#buton a').removeClass('active');
        $('.kotak #tsoal').hide();
        $('.kotak .soal'+s).show();
        $('#buton a[rel="'+ s +'"]').addClass('active');
        $('#jawab').attr('rel',s);
        if($('#buton a:last').attr('rel')==s){
            $('#next').addClass('disabled');
        }
    });
    $('#kumpul').click(function(){
        var s=$('#timer').text();
        $.ajax({
            url : 'konten/jawab.php',
            type : 'POST',
            data : 'time='+s,
            success:function(hasils){
                $('#hasilx').html(hasils);
                console.log('berhasil');
            },
            error: function(){
                alert("Terjadi kesalahan!");
            }
        });
    });
    $('#jawab').click(function(){
        var y=$(this).attr('rel');
        var x='jb'+y;
        var z=$('input:radio[name='+x+']:checked').val();
        var t=$('#timer').text();
        $.ajax({
            url : 'konten/jawab1.php',
            type : 'POST',
            data : 'jb='+z+'&jam='+t,
            success:function(hasils){
                console.log(hasils);
            },
            error: function(){
                alert("Terjadi kesalahan!");
            }
        });
    });
});
