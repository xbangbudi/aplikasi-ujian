<?php
include "../config/koneksi.php";
if($_SESSION['identitas']){
    $id=$_SESSION['identitas'];
    $sql=mysqli_query($conn,"SELECT * FROM akun WHERE no_reg='$id' ");
    $r=mysqli_fetch_array($sql);
    $kuota=$r['kuota'];
    $terpakai=$r['terpakai'];
    $sisa=$kuota-$terpakai;
    if($r['status']=="biasa"){
        if($sisa>0){
        $text="Anda memiliki kesempatan Try Out<br> ".$sisa."X lagi.";
        $nem=1;
        }
        else {
            $text="Anda tidak memiliki kuota Try Out.<br> Akun ini akan dihapus paling lama 6 hari";
            $nem=0;
        }
    }
    else{
        $text="Anda mempunyai kesempatan ujian selamanya";
        $nem=1;
    }
?>
<div class="container-fluid">
    <div class="row">
        <div class="col-sm-10">
            <div class="kotak">
                <center>
                    <h2 style="text-align: center;"><?php echo $text; ?></h2><br>
                    <?php
                    if($nem!=0){
                    ?>
                        <a href="../ujianto" class="btn btn-primary">Ke Halaman Ujian <i class="fa fa-arrow-right"></i></a>
                        <?php
                        
                    }
                    $tgl=date("Y-m-d");
                    $jam=date("H");
                    $menit=date("i");
                    $detik=date("s");
                    $asql=mysqli_query($conn,"SELECT * FROM paketsoalserentak WHERE tgl='$tgl' ");
                    if(mysqli_num_rows($asql)){
                        $rt=mysqli_fetch_array($asql);
                        $jmulai=$rt['jam'];
                        $arj=explode(":",$jmulai);
                        $mjam=$arj[0];
                        $mmenit=$arj[1];
                        $mdetik=$arj[2];
                        if($jam<=$mjam && $menit <= $mmenit){
                        ?>
                        <br>
                        <br>
                        <a href="../toserentak" class="btn btn-primary">Ke Halaman Ujian Serentak <i class="fa fa-arrow-right"></i></a>
                        <?php
                        }
                    }

                        ?>

                    </center>
                </div>
            </div>
        </div>
<?php
}
?>