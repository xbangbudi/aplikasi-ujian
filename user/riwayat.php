<?php
include "../config/koneksi.php";
if($_SESSION['identitas']){
    $id=$_SESSION['identitas'];
    ?>
<div class="container-fluid">
    <div class="row">
        <div class="col-sm-10">
            <div class="kotak">
                <center>
                    <h2>Riwayat Nilai</h2>
                    <table id="tabel1" class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th rowspan="2">No</th>
                                <th rowspan="2">Sisa Waktu</th>
                                <th colspan="4">Nilai</th>
                                <th rowspan="2">Keterangan</th>
                            </tr>
                            <tr>
                            <th>TWK</th>
                            <th>TIU</th>
                            <th>TKP</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $sql=mysqli_query($conn,"SELECT * FROM peserta WHERE id_user='$id' and status ='done' ORDER BY id DESC");
                            if(mysqli_num_rows($sql)){
                                $no=1;
                                while ($r=mysqli_fetch_array($sql)) {
                                    ?>
                            <tr>
                                <td><?php echo $no; ?></td>
                                <td><?php echo $r['sisawaktu']; ?></td>
                                <td><?php echo $r['twk']; ?></td>
                                <td><?php echo $r['tiu']; ?></td>
                                <td><?php echo $r['tkp']; ?></td>
                                <td><?php echo $r['nilai']; ?></td>
                                <td><?php ?></td>
                            </tr>
                        <?php
                            $no++;    
                            }
                            }
                            else{
                                ?>
                            <tr>
                                <td colspan="3">
                                    Belum ada hasil Tryout
                                </td>
                            </tr>
                            <?php
                            }
                            ?>
                        </tbody>
                    </table>
                </center>
            </div>
        </div>
    </div>
</div>
<?php
}
?>