<!DOCTYPE html>
<html>
<head>
	<title>Chart</title>
	<script type="text/javascript" src="chartjs/Chart.min.js"></script>
</head>
<body>
	<div style="width: 50%">
		<canvas id="canvas" height="450" width="600"></canvas>
	</div>
	<script>
		var barChartData = {
		labels : [
        <?php
        include 'koneksi.php';
        $kuery=mysqli_query($connect,"SELECT bln FROM `tbl_graf` WHERE thn = 2005");
        $x=array();
        while($d=mysqli_fetch_array($kuery)){
            array_push($x,$d['bln']);
        }
        echo implode(",",$x);  
        ?>
        ],
		datasets : [
			{
				fillColor : "rgba(220,220,220,0.5)",
				strokeColor : "rgba(220,220,220,0.8)",
				highlightFill: "rgba(220,220,220,0.75)",
				highlightStroke: "rgba(220,220,220,1)",
				data : [
				<?php
				$query=mysqli_query($connect,"SELECT jlh FROM `tbl_graf` WHERE thn = 2005 ");
				$a=array();
				while($r=mysqli_fetch_array($query)){
					array_push($a,$r['jlh']);
				}
				echo implode(",",$a);
				?>
				]
			}
		]

	}
	window.onload = function(){
		var ctx = document.getElementById("canvas").getContext("2d");
		window.myBar = new Chart(ctx).Bar(barChartData, {
			responsive : true
		});
	}
	</script>
</body>
</html>