<?php
include_once "../config/koneksi.php";
if(isset($_GET['bln'])){
	$bln=$_GET['bln'];
	?>
	<!-- <!DOCTYPE html>
<html>
<head>
	<title>Chart</title> -->
	<script src="chart/chartjs/Chart.min.js"></script>
<!-- </head>
<body> -->
	<div style="width: 50%">
		<canvas id="canvas" height="450" width="600"></canvas>
	</div>
	<script>
		var barChartData = {
		labels : [
        <?php
        $kuery=mysqli_query($conn,"SELECT tgl FROM `trade` WHERE bln = '$bln' ");
        $x=array();
        while($d=mysqli_fetch_array($kuery)){
            array_push($x,$d['tgl']);
        }
        echo implode(",",$x);  
        ?>
        ],
		datasets : [
			{
				fillColor : "rgba(60,141,188,0.9)",
				strokeColor : "#00a65a",
				pointColor  = '#00a65a'
				highlightFill: "rgba(60,141,188,0.9)",
				highlightStroke: "rgba(220,220,220,1)",
				data : [
				<?php
				$query=mysqli_query($conn,"SELECT jlh FROM `trade` WHERE bln ='$bln' ");
				$a=array();
				while($r=mysqli_fetch_array($query)){
					array_push($a,$r['jlh']);
				}
				echo implode(",",$a);
				?>
				]
			}
		]

	}
	$(function() {
		var barChartCanvas                   = $('#canvas').get(0).getContext('2d')
    var barChart                         = new Chart(barChartCanvas)
    // var barChartData                     = areaChartData
    // barChartData.datasets[1].fillColor   = '#00a65a'
    // barChartData.datasets[1].strokeColor = '#00a65a'
    // barChartData.datasets[1].pointColor  = '#00a65a'
    var barChartOptions                  = {
      //Boolean - Whether the scale should start at zero, or an order of magnitude down from the lowest value
      scaleBeginAtZero        : true,
      //Boolean - Whether grid lines are shown across the chart
      scaleShowGridLines      : true,
      //String - Colour of the grid lines
      scaleGridLineColor      : 'rgba(0,0,0,.05)',
      //Number - Width of the grid lines
      scaleGridLineWidth      : 1,
      //Boolean - Whether to show horizontal lines (except X axis)
      scaleShowHorizontalLines: true,
      //Boolean - Whether to show vertical lines (except Y axis)
      scaleShowVerticalLines  : true,
      //Boolean - If there is a stroke on each bar
      barShowStroke           : true,
      //Number - Pixel width of the bar stroke
      barStrokeWidth          : 2,
      //Number - Spacing between each of the X value sets
      barValueSpacing         : 5,
      //Number - Spacing between data sets within X values
      barDatasetSpacing       : 1,
      //String - A legend template
      legendTemplate          : '<ul class="<%=name.toLowerCase()%>-legend"><% for (var i=0; i<datasets.length; i++){%><li><span style="background-color:<%=datasets[i].fillColor%>"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>',
      //Boolean - whether to make the chart responsive
      responsive              : true,
      maintainAspectRatio     : true
    }

    barChartOptions.datasetFill = false
    barChart.Bar(barChartData, barChartOptions)
	})
	// window.onload = function(){
	// 	var ctx = document.getElementById("canvas").getContext("2d");
	// 	window.myBar = new Chart(ctx).Bar(barChartData, {
	// 		responsive : true
	// 	});
	// }
	</script>
<!-- </body>
</html> -->
	<?php
}
else{
	$bulan=date("m");
?>
<!-- <!DOCTYPE html>
<html>
<head>
	<title>Chart</title> -->
	<script src="chart/chartjs/Chart.min.js"></script>
<!-- </head>
<body> -->
	<div style="width: 50%">
		<canvas id="canvas" height="450" width="600"></canvas>
	</div>
	<script>
		var barChartData = {
		labels : [
        <?php
        $kuery=mysqli_query($conn,"SELECT tgl FROM `trade` WHERE bln = $bulan");
        $x=array();
        while($d=mysqli_fetch_array($kuery)){
            array_push($x,$d['tgl']);
        }
        echo implode(",",$x);  
        ?>
        ],
		datasets : [
			{
				fillColor : "rgba(60,141,188,0.9)",
				strokeColor : "#00a65a",
				pointColor  : '#00a65a',
				highlightFill: "rgba(60,141,188,0.9)",
				highlightStroke: "rgba(220,220,220,1)",
				data : [
				<?php
				$query=mysqli_query($conn,"SELECT jlh FROM `trade` WHERE bln = $bulan ");
				$a=array();
				while($r=mysqli_fetch_array($query)){
					array_push($a,$r['jlh']);
				}
				echo implode(",",$a);
				?>
				]
			}
		]

	}
	// window.onload = function(){
	// 	var ctx = document.getElementById("canvas").getContext("2d");
	// 	window.myBar = new Chart(ctx).Bar(barChartData, {
	// 		responsive : true
	// 	});
	// }
	window.onload = function(){
		var barChartCanvas                   = $('#canvas').get(0).getContext('2d')
    var barChart                         = new Chart(barChartCanvas)
    // var barChartData                     = areaChartData
    // barChartData.datasets[1].fillColor   = '#00a65a'
    // barChartData.datasets[1].strokeColor = '#00a65a'
    // barChartData.datasets[1].pointColor  = '#00a65a'
    var barChartOptions                  = {
      //Boolean - Whether the scale should start at zero, or an order of magnitude down from the lowest value
      scaleBeginAtZero        : true,
      //Boolean - Whether grid lines are shown across the chart
      scaleShowGridLines      : true,
      //String - Colour of the grid lines
      scaleGridLineColor      : 'rgba(0,0,0,.05)',
      //Number - Width of the grid lines
      scaleGridLineWidth      : 1,
      //Boolean - Whether to show horizontal lines (except X axis)
      scaleShowHorizontalLines: true,
      //Boolean - Whether to show vertical lines (except Y axis)
      scaleShowVerticalLines  : true,
      //Boolean - If there is a stroke on each bar
      barShowStroke           : true,
      //Number - Pixel width of the bar stroke
      barStrokeWidth          : 2,
      //Number - Spacing between each of the X value sets
      barValueSpacing         : 5,
      //Number - Spacing between data sets within X values
      barDatasetSpacing       : 1,
      //String - A legend template
      legendTemplate          : '<ul class="<%=name.toLowerCase()%>-legend"><% for (var i=0; i<datasets.length; i++){%><li><span style="background-color:<%=datasets[i].fillColor%>"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>',
      //Boolean - whether to make the chart responsive
      responsive              : true,
      maintainAspectRatio     : true
    }

    barChartOptions.datasetFill = false
    barChart.Bar(barChartData, barChartOptions)
	} 
	
	</script>
<!-- 	
</body>
</html> -->
<?php
}
?>