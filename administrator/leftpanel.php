  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
<img class='img-circle' src='../foto_user/small_avatar.png' >
			

        </div>
        <div class="pull-left info">
          <p><?php echo $_SESSION['username']; ?></p>
          <i class="fa fa-circle text-success"></i> 
        </div>
      </div>
      
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MENU</li>
        <?php
        if(isset($_GET['module'])){
          $modul=$_GET['module'];
        }else{
          $modul="";
        }
        ?>
        <li <?php if($modul=="home"){echo "class='active'";} ?>>
          <a href='?module=home'>
             <i class='fa fa-dashboard'></i> <span>Dashboard</span>
          </a>
        </li>
        <li <?php if($modul=="hargapaket"){echo "class='active'";} ?>>
          <a href='?module=hargapaket'>
             <i class='fa fa-dollar'></i> <span>Harga Paket</span>
          </a>
        </li>
        <li <?php if($modul=="pilih_jenis"){echo "class='active'";} ?>>
          <a href='?module=pilih_jenis'>
             <i class='fa fa-file-text'></i> <span>Tryout</span>
          </a>
        </li>
        <li <?php if($modul=="ta_hasil"){echo "class='active'";} ?>>
          <a href='?module=ta_hasil'>
             <i class='fa fa-file-text'></i> <span>Kelompok Paket</span>
          </a>
        </li>
        <li <?php if($modul=="verif"){echo "class='active'";} ?>>
          <a href='?module=verif'>
             <i class='fa fa-money'></i> <span>Konfirmasi Pembayaran</span>
          </a>
        </li>
        <li <?php if($modul=="datauser"){echo "class='active'";} ?>>
          <a href='?module=datauser'>
             <i class='fa fa-user'></i> <span>Data User</span>
          </a>
        </li>
        <li <?php if($modul=="admin"){echo "class='active'";} ?>>
          <a href='?module=admin'>
             <i class='fa fa-briefcase'></i> <span>Data Admin</span>
          </a>
        </li>
        <li <?php if($modul=="config"){echo "class='active'";} ?>>
          <a href='?module=config'>
             <i class='fa fa-gears'></i> <span>Setting</span>
          </a>
        </li>
        <li class="header">DATA MASTER</li>
        <li <?php if($modul=="ref_jenis_ujian"){echo "class='active'";} ?>>
          <a href='?module=ref_jenis_ujian'>
             <i class='fa fa-file'></i> <span>Ref Jenis Ujian</span>
          </a>
        </li>
        <li <?php if($modul=="ref_paket"){echo "class='active'";} ?>>
          <a href='?module=ref_paket'>
             <i class='fa fa-file'></i> <span>Ref Paket</span>
          </a>
        </li>
        <li <?php if($modul=="ref_tryout"){echo "class='active'";} ?>>
          <a href='?module=ref_tryout'>
             <i class='fa fa-file'></i> <span>Ref Tryout</span>
          </a>
        </li>
        <li <?php if($modul=="ref_jenis_soal"){echo "class='active'";} ?>>
          <a href='?module=ref_jenis_soal'>
             <i class='fa fa-bank'></i> <span>Bank Soal</span>
          </a>
        </li>
 </ul>
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- URL LAMA -->
  <!-- <li <?php if($modul=="trade"){echo "class='active'";} ?>>
          <a href='?module=trade'>
             <i class='fa fa-line-chart'></i> <span>Trade</span>
          </a>
        </li>
        <li <?php if($modul=="paketsoal"){echo "class='active'";} ?>>
          <a href='?module=paketsoal'>
             <i class='fa fa-file-text'></i> <span>Paket Soal</span>
          </a>
        </li> 
        <li <?php if($modul=="request"){echo "class='active'";} ?>>
          <a href='?module=request'>
             <i class='fa fa-th-list'></i> <span>Request Paket</span>
          </a>
        </li> 
        <li <?php if($modul=="paketsoalserentak"){echo "class='active'";} ?>>
          <a href='?module=paketsoalserentak'>
             <i class='fa fa-th-list'></i> <span>Ujian Serentak</span>
          </a>
        </li>-->
       
