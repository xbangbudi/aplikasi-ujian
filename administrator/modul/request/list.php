<?php
include_once "../config/koneksi.php";
?>
<div class='content-wrapper'>
    <section class='content-header'>
        <h1>
            Data
            <small>Request Paket</small>
        </h1>
        <ol class='breadcrumb'>
            <li>
                <a href='#'>
                    <i class='fa fa-dashboard'></i>
                    Dashboard</a>
            </li>
            <li class='active'>Request</li>
        </ol>
    </section>
    
    <section class='content'>
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class='box-title'>
                            Data Request Paket
                        </h3>
                    </div>
                    <div class="box-body">
                        <table id='example1' class='table table-bordered table-striped'>
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Kode Registrasi</th>
                                    <th>Tgl Pembayaran</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                            $sql=mysqli_query($conn,"SELECT * FROM request WHERE tgl_bayar!='	0000-00-00' AND status!='disetujui'");
                            if(mysqli_num_rows($sql)){
                                $no=1;
                                while ($r=mysqli_fetch_array($sql)) {
                                    ?>
                                <tr>
                                    <td><?php echo $no; ?></td>
                                    <td><?php echo $r['kode_reg']; ?></td>
                                    <td><?php echo $r['tgl_bayar']; ?></td>
                                    <td>
                                        <a href="?module=confirm&id=<?php echo $r['kode_reg']; ?>" class='btn btn-info'>
                                            <i class='fa  fa-eye'></i> <span>Lihat</span>
                                        </a>
                                        </a>
                                    </td>
                                </tr>
                            <?php
                            $no++;
                                }
                            }
                            else{
                                ?>
                                <tr>
                                    <td colspan="7" class="text-center">Belum Ada Request</td>
                                </tr>
                                <?php
                            }
                            ?>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>