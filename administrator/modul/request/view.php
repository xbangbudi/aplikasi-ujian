<?php
include_once "../config/koneksi.php";

if(isset($_GET['id'])){
    $id=$_GET['id'];
    $sql=mysqli_query($conn,"SELECT * FROM request WHERE kode_reg='$id' ");
    if(mysqli_num_rows($sql)){
        $r=mysqli_fetch_array($sql);
        $paket=$r['paket'];
        $psql=mysqli_query($conn,"SELECT * FROM paket WHERE id='$paket' ");
        $rd=mysqli_fetch_array($psql);
        $harga=$rd['harga'];
        $diskon=$rd['diskon'];
        $potong=$harga/100*$diskon;
        $total=$harga-$potong;
        if($rd['status']=="member"){
            $text= $rd['paket']." -- Unlimited Kuota";
            $kuota="";
        }
        else{
            $text= $rd['paket']."-- Kuota ".$rd['kuota']."X TryOut";
            $kuota=$rd['kuota'];
        }
?>
<div class='content-wrapper'>
    <section class='content-header'>
        <h1>
            Data
            <small>Request Paket</small>
        </h1>
        <ol class='breadcrumb'>
            <li>
                <a href='#'>
                    <i class='fa fa-dashboard'></i>
                    Dashboard</a>
            </li>
            <li class='active'>Request</li>
        </ol>
    </section>

    <section class='content'>
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title">Verivikasi Pembayaran dan Persetujuan Akun</h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    <form class="form-horizontal" action="?module=checked" method="POST">
                    <input type="hidden" name="kuota" value="<?php echo $kuota; ?>">
                    <input type="hidden" name="status" value="<?php echo $rd['status']; ?>">
                        <div class="box-body">
                            <div class="col-xs-12 col-md-6">
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-2 control-label">Kode Registrasi</label>
                                    <div class="col-sm-10">
                                        <input
                                            type="text"
                                            class="form-control"
                                            name="kode_reg"
                                            id="inputEmail3"
                                            value="<?php echo $id; ?>"
                                            readonly
                                            placeholder="Kode Registrasi">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputPassword3" class="col-sm-2 control-label">Nama</label>
                                    <div class="col-sm-10">
                                        <input
                                            type="text"
                                            class="form-control"
                                            id="inputPassword3"
                                            name="nama"
                                            value="<?php echo $r['nama']; ?>"
                                            readonly
                                            placeholder="Nama Lengkap">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputPassword3" class="col-sm-2 control-label">Email</label>
                                    <div class="col-sm-10">
                                        <input
                                            type="email"
                                            class="form-control"
                                            id="inputPassword3"
                                            name="email"
                                            value="<?php echo $r['email']; ?>"
                                            readonly
                                            placeholder="Email">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputPassword3" class="col-sm-2 control-label">No. Hp</label>
                                    <div class="col-sm-10">
                                        <input
                                            type="text"
                                            class="form-control"
                                            id="inputPassword3"
                                            name="hp"
                                            value="<?php echo $r['no_hp']; ?>"
                                            readonly
                                            placeholder="Nomor Handphone">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputPassword3" class="col-sm-2 control-label">Paket</label>
                                    <div class="col-sm-10">
                                        <input
                                            type="text"
                                            class="form-control"
                                            id="inputPassword3"
                                            name="paket"
                                            value="<?php echo $text; ?>"
                                            readonly
                                            placeholder="Paket yang dipilih">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputPassword3" class="col-sm-2 control-label">Total Biaya</label>
                                    <div class="col-sm-10">
                                        <input
                                            type="number"
                                            class="form-control"
                                            id="inputPassword3"
                                            name="harga"
                                            value="<?php echo $total; ?>"
                                            readonly
                                            placeholder="Total biaya">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputPassword3" class="col-sm-2 control-label">Tgl Kirim Bukti
                                    </label>
                                    <div class="col-sm-10">
                                        <input
                                            type="date"
                                            class="form-control"
                                            id="inputPassword3"
                                            name="tgl"
                                            value="<?php echo $r['tgl_bayar']; ?>"
                                            readonly
                                            placeholder="">
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-md-6">
                                <div class="form-group" data-toggle="modal" data-target="#modal-default">
                                    <label for="gambar">Foto Bukti Pembayaran</label>
                                    <img
                                        class="img-responsive pad"
                                        src="../apk-ujian/images/<?php echo $r['foto']; ?>"
                                        alt="Photo Bukti Pembayaran">
                                </div>
                            </div>
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer">
                            <a href="?module=request" class="btn btn-default">
                                <i class="fa fa-mail-reply"></i>
                                <span>Kembali</span></a>
                            <button type="submit" class="btn btn-info pull-right" name="create">
                                <i class="fa fa-check"></i>
                                <span>Setujui Akun</span></button>
                        </div>
                        <!-- /.box-footer -->
                    </form>
                </div>
            </div>
        </div>
        <div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" id="modal-default">
            <div class="modal-dialog  modal-dialog-centered modal-lg">
                <div class="modal-content" role="document">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Lihat Foto Bukti Pembayaran</h4>
                    </div>
                    <div class="modal-body">
                        <img
                            class="img-responsive pad"
                            src="../apk-ujian/images/<?php echo $r['foto']; ?>"
                            alt="Photo Bukti Pembayaran">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                        <!-- <button type="button" class="btn btn-primary">Save changes</button> -->
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
    </section>
</div>
<?php
    }
}
?>