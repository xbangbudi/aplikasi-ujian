<?php
include_once "../config/koneksi.php";
?>
<script>
    function kirim(abs) {
                   var id=abs;
                   var aksi= $('#stganti[rel="'+id+'"]').text();
        $.ajax({
            url: "set/ganti.php",
            type: "POST",
            data: "ganti=" + aksi + "&id=" + id,
            success: function(data){
               $('#stganti[rel="'+id+'"]').text(data);
            },
            error: function(){
               alert('Gagal mengaktifkan');
               if($('#stga["'+id+'"]').attr('checked')=="true"){
                   $('#stga["'+id+'"]').removeAttr('checked');
               }
               else{
                   $('#stga["'+id+'"]').addAttr('checked');
               }
            }
         });
                }  
</script>
<div class='content-wrapper'>
    <section class='content-header'>
        <h1>
            Daftar
            <small>Paket Soal</small>
        </h1>
        <ol class='breadcrumb'>
            <li>
                <a href='#'>
                    <i class='fa fa-dashboard'></i>
                    Dashboard</a>
            </li>
            <li class='active'>Paket Soal</li>
        </ol>
    </section>
    
    <section class='content'>
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class='box-title'>
                            <a href='?module=tambahpaket'>
                                <button type='button' class='btn btn-block btn-primary'>
                                    <i class='glyphicon glyphicon-pencil'></i>
                                    Tambah Paket</button>
                            </a>
                        </h3>
                    </div>
                    <div class="box-body table-responsive">
                        <table id='example1' class='table table-bordered table-striped'>
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Judul Paket</th>
                                    <th>Waktu Try Out</th>
                                    <th>Bank Soal</th>
                                    <th>Tanggal dibuat</th>
                                    <th>Terakhir diubah</th>
                                    <th>Aksi</th>
                                    <th>Aktif/Nonaktif</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                            $sql=mysqli_query($conn,"SELECT * FROM `paketsoal` ORDER BY id_paket ASC");
                            if(mysqli_num_rows($sql)){
                                $no=1;
                                while ($r=mysqli_fetch_array($sql)) {
                                    $id=$r['id_paket'];
                                    $soal = mysqli_query($conn, "SELECT * FROM soal WHERE id_paket ='$id' ");
                                    $jmlsoal = mysqli_num_rows($soal);
                                    ?>
                                <tr>
                                    <td><?php echo $no; ?></td>
                                    <td><?php echo $r['judul']; ?></td>
                                    <td><?php echo $r['waktu']; ?></td>
                                    <td><a href="?module=soal&id=<?php echo $r['id_paket'];?>" class='btn btn-info'><i class='fa  fa-server'></i> Jumlah Soal <?php echo $jmlsoal; ?></a></td>
                                    <td><?php echo $r['dibuat']; ?></td>
                                    <td><?php echo $r['diubah']; ?></td>
                                    <td>
                                        <a href="export/soal.php?id=<?php echo $r['id_paket']; ?>" class='btn btn-info'>
                                            <i class='fa  fa-print'></i>
                                        </a>
                                        <a href="?module=editpaket&id=<?php echo $r['id_paket']; ?>" class='btn btn-info'>
                                            <i class='fa fa-pencil'></i>
                                        </a>
                                        <a
                                            href="?module=hapuspaket&id=<?php echo $r['id_paket'];?>"
                                            class='btn btn-danger'
                                            onclick="return confirm('Menghapus Paket Soal akan turut serta menghapus soal-soal didalamnya. \n Yakin ingin menghapus paket ini?');">
                                            <i class='fa fa-trash'></i>
                                        </a>
                                    </td>
                                    <td>
                                    <div class="form-group">
                <label class="" id="ganti" rel="<?php echo $id; ?>" onclick="kirim(<?php echo $id; ?>)">
                  <div class="icheckbox_flat-green" aria-checked="false" aria-disabled="false" style="position: relative;"><input type="checkbox" id="stga" class="flat-red" <?php if($r['sta']=="aktif"){ echo "checked";} else{ echo "";} ?> style="position: absolute; opacity: 0;" rel="<?php echo $id; ?>"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins></div>
                  <span id="stganti" rel="<?php echo $id; ?>"><?php echo $r['sta']; ?></span>
                </label>
              </div>
                                    </td>
                                </tr>
                            <?php
                            $no++;
                                }
                            }
                            else{
                                ?>
                                <tr>
                                    <td colspan="7" class="text-center">Belum Ada Paket Tersedia</td>
                                </tr>
                                <?php
                            }
                            ?>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
