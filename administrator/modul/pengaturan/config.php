<?php
include_once "../config/koneksi.php";
?>
<div class='content-wrapper'>
    <section class='content-header'>
        <h1>
            Pengaturan
            <small>Administrasi</small>
        </h1>
        <ol class='breadcrumb'>
            <li>
                <a href='#'>
                    <i class='fa fa-dashboard'></i>
                    Dashboard</a>
            </li>
            <li class='active'>Konfigurasi</li>
        </ol>
    </section>

    <section class='content'>
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class='box-title'>
                            Data Rekening BANK
                        </h3>
                        <a class="btn btn-success pull-right" href="?module=addrekening">
                            <i class="fa fa-plus"></i>
                            <span>Tambah Rekening Bank</span></a>
                    </div>
                    <div class="box-body">
                        <table id='example1' class='table table-bordered table-striped'>
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Nama BANK</th>
                                    <th>No. Rekening</th>
                                    <th>Atas Nama</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                            $sql=mysqli_query($conn,"SELECT * FROM `rekening` WHERE jenis='bank' ORDER BY id ASC");
                            if(mysqli_num_rows($sql)){
                                $no=1;
                                while ($r=mysqli_fetch_array($sql)) {
                                    $id=$r['id'];
                                    ?>
                                <tr>
                                    <td><?php echo $no; ?></td>
                                    <td><?php echo $r['nama']; ?></td>
                                    <td><?php echo $r['no_rekening']; ?></td>
                                    <td><?php echo $r['atasnama']; ?></td>
                                    <td>
                                        <a
                                            href="?module=editrekening&id=<?php echo $r['id']; ?>&type=bank"
                                            class='btn btn-info'>
                                            <i class='fa fa-pencil'></i>
                                        </a>
                                        <a
                                            href="?module=hapusrekening&id=<?php echo $r['id'];?>"
                                            class='btn btn-danger'
                                            onclick="return confirm('Yakin ingin menghapus rekening ini?');">
                                            <i class='fa fa-trash'></i>
                                        </a>
                                    </td>
                                </tr>
                            <?php
                            $no++;
                                }
                            }
                            else{
                                ?>
                                <tr>
                                    <td colspan="7" class="text-center">Belum ada Rekening</td>
                                </tr>
                                <?php
                            }
                            ?>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-md-6">
                <div class="box">
                    <div class="box-header">
                        <h3 class='box-title'>
                            Data Rekening OVO
                        </h3>
                    </div>
                    <div class="box-body">
                    <?php
                        $sql=mysqli_query($conn,"SELECT * FROM `rekening` WHERE jenis='ovo' ORDER BY id ASC");
                        if(mysqli_num_rows($sql)){
                            $r=mysqli_fetch_array($sql)
                        ?>
                        <div class="col-md-6">
                            <img class="img-responsive pad" src="../apk-ujian/images/<?php echo $r['gambar']; ?>">
                        </div>
                        <div class="col-md-6">
                            <h2>
                                <b><?php echo $r['no_rekening']; ?></b>
                            </h2>
                            <a class="btn btn-info btn-flat pull-right" href="?module=editrekening&type=ovo&id=<?php echo $r['id']; ?>">
                            <i class="fa fa-pencil"></i>
                            <span>Ganti</span></a>
                        </div>
                        <?php
                            
                        }
                        ?>

                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-md-6">
                <div class="box">
                    <div class="box-header">
                        <h3 class='box-title'>
                            Data Rekening Go-Pay
                        </h3>
                    </div>
                    <div class="box-body">
                    <?php
                        $sql=mysqli_query($conn,"SELECT * FROM `rekening` WHERE jenis='gopay' ORDER BY id ASC");
                        if(mysqli_num_rows($sql)){
                            $r=mysqli_fetch_array($sql)
                        ?>
                        <div class="col-md-6">
                            <img class="img-responsive pad" src="../apk-ujian/images/<?php echo $r['gambar']; ?>">
                        </div>
                        <div class="col-md-6">
                            <h2>
                                <b><?php echo $r['no_rekening']; ?></b>
                            </h2>
                            <a class="btn btn-info btn-flat pull-right" href="?module=editrekening&type=ovo&id=<?php echo $r['id']; ?>">
                            <i class="fa fa-pencil"></i>
                            <span>Ganti</span></a>
                        </div>
                        <?php
                            
                        }
                        ?>

                    </div>
                </div>
            </div>
        </div>
    </section>
</div>