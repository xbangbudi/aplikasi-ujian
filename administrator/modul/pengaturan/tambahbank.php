<?php
include_once "../config/koneksi.php";
if(isset($_POST['save'])){
    $no=$_POST['no_rek'];
    $bank=$_POST['bank'];
    $nama=$_POST['nama'];
    $sql=mysqli_query($conn,"INSERT INTO rekening(jenis, nama, no_rekening, atasnama) VALUES ('bank','$bank','$no','$nama')");
    if($sql){
        echo "<script> alert('Rekening Berhasil ditambahkan'); document.location.href = '?module=config';</script>";
    }
    else{
        echo "<script> alert('Terjadi Kesalahan');</script>";
    }
}
?>
<div class='content-wrapper'>
    <section class='content-header'>
        <h1>
            Pengaturan
            <small>Administrasi</small>
        </h1>
        <ol class='breadcrumb'>
            <li>
                <a href='#'>
                    <i class='fa fa-dashboard'></i>
                    Dashboard</a>
            </li>
            <li class='active'>Konfigurasi</li>
        </ol>
    </section>

    <section class='content'>
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class='box-title'>
                            Tambah Rekening BANK
                        </h3>
                    </div>
                    <div class="box-body">
                        <form method="POST" action="" class="form-horizontal">
                            <div class="box-body">
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-2 control-label">No Rekening</label>

                                    <div class="col-sm-10">
                                        <input type="text" name="no_rek" class="form-control" id="inputEmail3" placeholder="Nomor Rekening Bank">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputPassword3" class="col-sm-2 control-label">Bank</label>

                                    <div class="col-sm-10">
                                        <input
                                            type="text"
                                            name="bank"
                                            class="form-control"
                                            id="inputPassword3"
                                            placeholder="Nama Bank">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputPassword3" class="col-sm-2 control-label">Atas Nama</label>

                                    <div class="col-sm-10">
                                        <input
                                            type="text"
                                            name="nama"
                                            class="form-control"
                                            id="inputPassword3"
                                            placeholder="Nama Pemilik Rekening">
                                    </div>
                                </div>
                            </div>
                            <!-- /.box-body -->
                            <div class="box-footer">
                                <a href="?module=config" class="btn btn-default"><i class="fa fa-mail-reply"></i> <span>Kembali</span></a>
                                <button type="submit" class="btn btn-info pull-right" name="save">Simpan</button>
                            </div>
                            <!-- /.box-footer -->
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>