<?php
include_once "../config/koneksi.php";
if(isset($_POST['save'])){
    $no=$_POST['no_rek'];
    $bank=$_POST['bank'];
    $nama=$_POST['nama'];
    $sql=mysqli_query($conn,"INSERT INTO rekening(jenis, nama, no_rekening, atasnama) VALUES ('bank','$bank','$no','$nama')");
    if($sql){
        echo "<script> alert('Rekening Berhasil ditambahkan'); document.location.href = '?module=config';</script>";
    }
    else{
        echo "<script> alert('Terjadi Kesalahan');</script>";
    }
}
if(isset($_POST['vakun'])){
    $no=$_POST['no_hp'];
    $id=$_GET['id'];
    $gambar=$_FILES['upload']['name']; //nama gambarnya
    $type=$_FILES['upload']['type']; //type filenya
    $semen=$_FILES['upload']['tmp_name']; //nama gambar pada temporary files
    $dir="../apk-ujian/images/"; //direktorinya
    if($type=='image/jpeg' || $type=='image/png'){
        // memindahakan gamabrnya ke dalam folder images
        $upload=move_uploaded_file($semen, $dir.$gambar);

        // memeriksa apakah berhasil terupload
        if($upload){

            // menyimpan ke database
            $result=mysqli_query($conn,"UPDATE rekening SET no_rekening='$no', gambar='$gambar' WHERE id='$id' ");

            // memeriksa apakah tersimpan ke database
            if($result){
                // mengembalikan ke halaman data
                echo "<script> alert('Rekening Berhasil ditambahkan'); document.location.href = '?module=config';</script>";
            }
            else{

                unlink($dir.$gambar); // menghapus gambar yg terupload ke folder images supaya tidak terjadi tabrakan nama file saat upload selanjutnya

                echo "<script> alert('Terjadi Kesalahan');</script>";
            }
        }
    }
}
if(isset($_GET['type'])){
    $id=$_GET['id'];
    if($_GET['type']=="bank"){
        $ql=mysqli_query($conn,"SELECT * FROM rekening WHERE id='$id'");
        if(mysqli_num_rows($ql)){
            $rd=mysqli_fetch_array($ql);
            ?>
<div class='content-wrapper'>
    <section class='content-header'>
        <h1>
            Pengaturan
            <small>Administrasi</small>
        </h1>
        <ol class='breadcrumb'>
            <li>
                <a href='#'>
                    <i class='fa fa-dashboard'></i>
                    Dashboard</a>
            </li>
            <li class='active'>Konfigurasi</li>
        </ol>
    </section>

    <section class='content'>
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class='box-title'>
                            Ubah Rekening BANK
                        </h3>
                    </div>
                    <div class="box-body">
                        <form class="form-horizontal">
                            <div class="box-body">
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-2 control-label">No Rekening</label>

                                    <div class="col-sm-10">
                                        <input type="text" name="no_rek" class="form-control" id="inputEmail3" placeholder="Nomor Rekening Bank" value="<?php echo $rd['no_rekening']; ?>">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputPassword3" class="col-sm-2 control-label">Bank</label>

                                    <div class="col-sm-10">
                                        <input
                                            type="text"
                                            name="bank"
                                            class="form-control"
                                            id="inputPassword3"
                                            value="<?php echo $rd['nama']; ?>"
                                            placeholder="Nama Bank">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputPassword3" class="col-sm-2 control-label">Atas Nama</label>

                                    <div class="col-sm-10">
                                        <input
                                            type="text"
                                            name="nama"
                                            class="form-control"
                                            id="inputPassword3"
                                            value="<?php echo $rd['atasnama']; ?>"
                                            placeholder="Nama Pemilik Rekening">
                                    </div>
                                </div>
                            </div>
                            <!-- /.box-body -->
                            <input type="hidden" name="id" value="<?php echo $id; ?>">
                            <div class="box-footer">
                                <a href="?module=config" class="btn btn-default"><i class="fa fa-mail-reply"></i> <span>Kembali</span></a>
                                <button type="submit" class="btn btn-info pull-right" name="save">Simpan</button>
                            </div>
                            <!-- /.box-footer -->
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<?php
        }
        
    }
    else {
        $ql=mysqli_query($conn,"SELECT * FROM rekening WHERE id='$id'");
        if(mysqli_num_rows($ql)){
            $rd=mysqli_fetch_array($ql);
        ?>
        <div class='content-wrapper'>
    <section class='content-header'>
        <h1>
            Pengaturan
            <small>Administrasi</small>
        </h1>
        <ol class='breadcrumb'>
            <li>
                <a href='#'>
                    <i class='fa fa-dashboard'></i>
                    Dashboard</a>
            </li>
            <li class='active'>Konfigurasi</li>
        </ol>
    </section>

    <section class='content'>
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class='box-title'>
                            Ubah Rekening <?php echo $_GET['type']; ?>
                        </h3>
                    </div>
                    <div class="box-body">
                        <form class="form-horizontal" method="POST" enctype="multipart/form-data">
                            <div class="box-body">
                                <div class="form-group">
                                    <label for="no_hp" class="col-sm-2 control-label">No HP</label>

                                    <div class="col-sm-10">
                                        <input type="text" name="no_hp" class="form-control" id="no_hp" placeholder="Nomor Handphone" value="<?php echo $rd['no_rekening']; ?>">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="bukti" class="col-sm-2 control-label"> Upload Barcode</label>

                                    <div class="col-sm-10">
                                        <input
                                            type="file"
                                            name="upload"
                                            id="bukti"
                                            value="">
                                    </div>
                                </div>
                                <input type="hidden" name="id" value="<?php echo $id; ?>">
                            </div>
                            <!-- /.box-body -->
                            <input type="hidden" name="id" value="<?php echo $id; ?>">
                            <div class="box-footer">
                                <a href="?module=config" class="btn btn-default"><i class="fa fa-mail-reply"></i> <span>Kembali</span></a>
                                <button type="submit" class="btn btn-info pull-right" name="vakun">Simpan</button>
                            </div>
                            <!-- /.box-footer -->
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
        <?php
        }
    }
    
}
?>
