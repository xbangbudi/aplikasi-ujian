<?php
include_once "../config/koneksi.php";
if (isset($_POST['save'])) {
    $judul=$_POST['judul'];
    $waktu=$_POST['waktu'];
    $id=$_POST['id'];
    $tgl=$_POST['tgl'];
    $jam=$_POST['jam'];
    $sql=mysqli_query($conn,"UPDATE paketsoalserentak SET judul='$judul', waktu='$waktu', tgl='$tgl' ,jam='$jam',diubah=CURRENT_TIMESTAMP WHERE id_paket='$id' ");
    if($sql){
        echo "<script> alert('Paket Berhasil diubah'); document.location.href = '?module=paketsoalserentak';</script>";
    }
    else{
        echo "<script> alert('Terjadi Kesalahan');</script>";
    }
}
if(isset($_GET['id'])){
    $id=$_GET['id'];
?>
<div class='content-wrapper'>
    <section class='content-header'>
        <h1>
            Daftar
            <small>Paket Soal</small>
        </h1>
        <ol class='breadcrumb'>
            <li>
                <a href='#'>
                    <i class='fa fa-dashboard'></i>
                    Dashboard</a>
            </li>
            <li class='active'>Paket Soal</li>
        </ol>
    </section>

    <section class='content'>
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class='box-title'>
                            Edit Paket
                        </h3>
                    </div>
                    <div class="box-body">
                        <?php
                        $rsql=mysqli_query($conn,"SELECT * FROM paketsoalserentak WHERE id_paket='$id'");
                        $r=mysqli_fetch_array($rsql);
                        ?>
                        <form
                            class='stdform stdform2'
                            method="POST"
                            action=''
                            enctype='multipart/form-data'>
                            <div class='input-group'>
                                <span class='input-group-addon'>
                                    <i class='fa fa-text-height'></i>
                                </span>
                                <input
                                    type='text'
                                    name='judul'
                                    class='form-control'
                                    placeholder='Judul Paket'
                                    value="<?php echo $r['judul']; ?>"
                                    required="required">
                            </div>
                            <br>
                            <div class='input-group'>
                                <span class='input-group-addon'>
                                    <i class='fa fa-clock-o'></i>
                                </span>
                                <input
                                    type='text'
                                    name='waktu'
                                    value='<?php echo $r['waktu']; ?>'
                                    class='form-control'
                                    placeholder='Waktu Ujian dalam jam'
                                    required="required">

                            </div>
                            <br>
                            <div class='input-group'>
                                <span class='input-group-addon'>
                                    <i class='fa fa-text-height'></i>
                                </span>
                                <input
                                    type='date'
                                    name='tgl'
                                    class='form-control'
                                    placeholder='tanggal ujian'
                                    value='<?php echo $r['tgl']; ?>'
                                    required="required">
                            </div>
                            <br>
                            <div class='input-group'>
                                <span class='input-group-addon'>
                                    <i class='fa fa-text-height'></i>
                                </span>
                                <input
                                    type='time'
                                    name='jam'
                                    class='form-control'
                                    placeholder='jam mulai'
                                    value='<?php echo $r['jam']; ?>'
                                    required="required">
                            </div>
                            <br>
                            <input type="hidden" name="id" value="<?php echo $id; ?>">
                            <p class='stdformbutton'>
                                <button class='btn btn-primary' type="submit" name="save">Simpan</button>
                                <input
                                    type="button"
                                    value="Batal"
                                    onclick="self.history.back()"
                                    class='btn btn-warning btn-rounded'>
                            </p>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<?php
}
?>