<?php
include_once "../config/koneksi.php";
if (isset($_POST['save'])) {
    $judul=$_POST['judul'];
    $jam=$_POST['jam'];
    $jamulai=$_POST['jml'];
    $menit=$_POST['menit'];
    $tgl=$_POST['tgl'];
    $waktu=$jam.":".$menit;
    $sql=mysqli_query($conn,"INSERT INTO paketsoalserentak(judul, waktu, tgl, jam, dibuat, diubah, sta) VALUES ('$judul','$waktu','$tgl','$jamulai',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP,'nonaktif') ");
    if($sql){
        echo "<script> alert('Paket Berhasil ditambahkan'); document.location.href = '?module=paketsoalserentak';</script>";
    }
    else{
        echo "<script> alert('Terjadi Kesalahan');</script>";
    }
}
?>
<div class='content-wrapper'>
    <section class='content-header'>
        <h1>
            Daftar
            <small>Paket Soal</small>
        </h1>
        <ol class='breadcrumb'>
            <li>
                <a href='#'>
                    <i class='fa fa-dashboard'></i>
                    Dashboard</a>
            </li>
            <li class='active'>Paket Soal</li>
        </ol>
    </section>

    <section class='content'>
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class='box-title'>
                            Tambah Paket
                        </h3>
                    </div>
                    <div class="box-body">
                        <form
                            class='stdform stdform2'
                            method="POST"
                            action=''
                            enctype='multipart/form-data'>
                            <div class='input-group'>
                                <span class='input-group-addon'>
                                    <i class='fa fa-text-height'></i>
                                </span>
                                <input
                                    type='text'
                                    name='judul'
                                    class='form-control'
                                    placeholder='Judul Paket'
                                    required="required">
                            </div>
                            <br>
                            <div class='input-group'>
                                <span class='input-group-addon'>
                                    <i class='fa fa-clock-o'></i>
                                </span>
                                <input
                                    type='number'
                                    name='jam'
                                    class='form-control'
                                    placeholder='Waktu Ujian dalam jam'
                                    max='12'
                                    required="required"><input
                                    type='number'
                                    name='menit'
                                    class='form-control'
                                    placeholder='Waktu Ujian dalam menit'
                                    max='59'
                                    required="required">
                            </div>
                            <br>
                            <div class='input-group'>
                                <span class='input-group-addon'>
                                    <i class='fa fa-text-height'></i>
                                </span>
                                <input
                                    type='date'
                                    name='tgl'
                                    class='form-control'
                                    placeholder='Tanggal'
                                    required="required">
                            </div>
                            <br>
                            <div class='input-group'>
                                <span class='input-group-addon'>
                                    <i class='fa fa-text-height'></i>
                                </span>
                                <input
                                    type='time'
                                    name='tgl'
                                    class='form-control'
                                    placeholder='Jam Mulai'
                                    required="required">
                            </div>
                            <br>
                            <p class='stdformbutton'>
                                <button class='btn btn-primary' type="submit" name="save">Simpan</button>
								<input type=button value=Batal onclick=self.history.back() class='btn btn-warning btn-rounded'>  
                            </p>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>