<?php
include_once "../config/koneksi.php";
if(isset($_POST['save'])){
    $id=$_POST['id'];
    $jenis=$_POST['jenis'];
    $soal=$_POST['soal'];
    $p1=$_POST['p1'];
    $p2=$_POST['p2'];
    $p3=$_POST['p3'];
    $p4=$_POST['p4'];
    $p5=$_POST['p5'];
    if($jenis!="TKP"){
        $kunci=$_POST['kunci'];
        if($kunci!="na"){
            $na=0;
        }
        else {
            $na=5;
        }
        if($kunci!="nb"){
            $nb=0;
        }
        else {
            $nb=5;
        }
        if($kunci!="nc"){
            $nc=0;
        }
        else {
            $nc=5;
        }
        if($kunci!="nd"){
            $nd=0;
        }
        else {
            $nd=5;
        }
        if($kunci!="ne"){
            $ne=0;
        }
        else {
            $ne=5;
        }
    }
    else{
        $na=$_POST['na'];
        $nb=$_POST['nb'];
        $nc=$_POST['nc'];
        $nd=$_POST['nd'];
        $ne=$_POST['ne'];
    }
    $sql=mysqli_query($conn,"INSERT INTO soalserentak(id_paket, soal, a, na, b, nb, c, nc, d, nd, e, ne, jenis) VALUES('$id','$soal','$p1','$na','$p2','$nb','$p3','$nc','$p4','$nd','$p5','$ne','$jenis') ");
    $nql=mysqli_query($conn,"UPDATE paketsoalserentak set diubah=CURRENT_TIMESTAMP WHERE id_paket='$id' ");
    if($sql){
        echo "<script> alert('Soal Berhasil ditambahkan'); document.location.href = '?module=tampilsoalserentak&id=$id&jenis=$jenis';</script>";
    }
    else{
        echo "<script> alert('Terjadi Kesalahan');</script>";
    }
}
if(isset($_GET['id'])&&isset($_GET['jenis'])){
    $id=$_GET['id'];
    $jenis=$_GET['jenis'];
    $j = array('a','b','c','d','e');
?>
<div class='content-wrapper'>
    <section class='content-header'>
        <h1>
            Tambah
            <small>Soal</small>
        </h1>
        <ol class='breadcrumb'>
            <li>
                <a href='#'>
                    <i class='fa fa-dashboard'></i>
                    Dashboard</a>
            </li>
            <li class='active'>Paket Soal</li>
        </ol>
    </section>

    <section class='content'>
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class='box-title'>
                            Tambah Soal
                        </h3>
                    </div>
                    <?php
                    if($jenis!="TKP"){
                    ?>
                    <div class="box-body">
                        <form
                            class='stdform stdform2'
                            method="POST"
                            action=''
                            enctype='multipart/form-data'>
                            <div id="myTab">
                                <div id="nav">
                                    <a class="btn btn-default" href="#" rel="soal">Soal</a>
                                    <a class="btn btn-default" href="#" rel="a">A</a>
                                    <a class="btn btn-default" href="#" rel="b">B</a>
                                    <a class="btn btn-default" href="#" rel="c">C</a>
                                    <a class="btn btn-default" href="#" rel="d">D</a>
                                    <a class="btn btn-default" href="#" rel="e">E</a>
                                    <a class="btn btn-default" href="#" rel="kunci">Kunci</a>
                                </div>
                                <div class="isi" id="soal">

                                    <label>
                                        Soal
                                    </label>
                                    <div class='form-group'>
                                        <textarea
                                            class='form-control richtext'
                                            name='soal'
                                            rows='3'
                                            placeholder='Isi soal ...'></textarea>
                                    </div>
                                </div>
                                <!-- <br> -->
                                <div class="isi" id="a">
                                    <label>
                                        Pilihan A
                                    </label>
                                    <div class='form-group'>
                                        <textarea
                                            class='form-control richtextsimple'
                                            name='p1'
                                            rows='3'
                                            placeholder='pilihan A'></textarea>
                                    </div>
                                </div>
                                <!-- <br> -->
                                <div class="isi" id="b">

                                    <label>
                                        Pilihan B
                                    </label>
                                    <div class='form-group'>

                                        <textarea
                                            class='form-control richtextsimple'
                                            name='p2'
                                            rows='3'
                                            placeholder='pilihan B'></textarea>
                                    </div>
                                </div>
                                <!-- <br> -->
                                <div class="isi" id="c">
                                    <label>
                                        Pilihan C
                                    </label>
                                    <div class='form-group'>
                                        <textarea
                                            class='form-control richtextsimple'
                                            name='p3'
                                            rows='3'
                                            placeholder='pilihan C'></textarea>
                                    </div>
                                </div>

                                <!-- <br> -->
                                <div class="isi" id="d">
                                    <label>
                                        Pilihan D
                                    </label>
                                    <div class='form-group'>
                                        <textarea
                                            class='form-control richtextsimple'
                                            name='p4'
                                            rows='3'
                                            placeholder='pilihan D'></textarea>
                                    </div>

                                </div>
                                <!-- <br> -->
                                <div class="isi" id="e">
                                    <label>
                                        Pilihan E
                                    </label>
                                    <div class='form-group'>
                                        <textarea
                                            class='form-control richtextsimple'
                                            name='p5'
                                            rows='3'
                                            placeholder='pilihan E'></textarea>
                                    </div>
                                </div>
                                <!-- <br> -->
                                <div class="isi" id="kunci">
                                    <label>
                                        Kunci
                                    </label>
                                    <div class='form-group'>

                                        <select name='kunci' class='form-control'>
                                            <option value="na">A</option>
                                            <option value="nb">B</option>
                                            <option value="nc">C</option>
                                            <option value="nd">D</option>
                                            <option value="ne">E</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <br><br>
                            <input type="hidden" name="id" value="<?php echo $id;?>">
                            <input type="hidden" name="jenis" value="<?php echo $jenis;?>">
                            <p class='stdformbutton'>
                                <button class='btn btn-primary' type="submit" name="save">Simpan</button>
                                <input
                                    type="button"
                                    value="Batal"
                                    onclick="self.history.back()"
                                    class='btn btn-warning btn-rounded'>

                            </p>
                        </form>
                    </div>
                <?php
                    }
                    else{
                        ?>
                    <div class="box-body">
                        <form
                            class='stdform stdform2'
                            method="POST"
                            action=''
                            enctype='multipart/form-data'>
                            <label>
                                Soal
                            </label>
                            <div class='form-group'>
                                <textarea
                                    class='form-control richtext'
                                    name='soal'
                                    rows='3'
                                    placeholder='Isi soal ...'></textarea>
                            </div>
                            <br>
                            <label>
                                Pilihan A
                            </label>
                            <div class='form-group'>
                                <textarea
                                    class='form-control richtextsimple'
                                    name='p1'
                                    rows='3'
                                    placeholder='pilihan A'></textarea>
                                <select name='na' class='form-control' required="required">
                                    <option selected="selected" disabled="disabled">-- Pilih Nilai --</option>
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                </select>
                            </div>
                            <br>

                            <label>
                                Pilihan B
                            </label>
                            <div class='form-group'>

                                <textarea
                                    class='form-control richtextsimple'
                                    name='p2'
                                    rows='3'
                                    placeholder='pilihan B'></textarea>
                                <select name='nb' class='form-control' required="required">
                                    <option selected="selected" disabled="disabled">-- Pilih Nilai --</option>
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                </select>
                            </div>
                            <br>
                            <label>
                                Pilihan C
                            </label>
                            <div class='form-group'>
                                <textarea
                                    class='form-control richtextsimple'
                                    name='p3'
                                    rows='3'
                                    placeholder='pilihan C'></textarea>
                                <select name='nc' class='form-control' required="required">
                                    <option selected="selected" disabled="disabled">-- Pilih Nilai --</option>
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                </select>
                            </div>
                            <br>
                            <label>
                                Pilihan D
                            </label>
                            <div class='form-group'>
                                <textarea
                                    class='form-control richtextsimple'
                                    name='p4'
                                    rows='3'
                                    placeholder='pilihan D'></textarea>
                                <select name='nd' class='form-control' required="required">
                                    <option selected="selected" disabled="disabled">-- Pilih Nilai --</option>
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                </select>
                            </div>
                            <br>
                            <label>
                                Pilihan E
                            </label>
                            <div class='form-group'>
                                <textarea
                                    class='form-control richtextsimple'
                                    name='p5'
                                    rows='3'
                                    placeholder='pilihan E'></textarea>
                                <select name='ne' class='form-control' required="required">
                                    <option selected="selected" disabled="disabled">-- Pilih Nilai --</option>
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                </select>
                            </div>
                            <br>
                            <input type="hidden" name="id" value="<?php echo $id;?>">
                            <input type="hidden" name="jenis" value="<?php echo $jenis;?>">
                            <p class='stdformbutton'>
                                <button class='btn btn-primary' type="submit" name="save">Simpan</button>
                                <input
                                    type="button"
                                    value="Batal"
                                    onclick="self.history.back()"
                                    class='btn btn-warning btn-rounded'>

                            </p>
                        </form>
                    </div>
                    <?php
                    }
                    ?>
                </div>
            </div>
        </div>
    </section>
</div>
<?php
}?>