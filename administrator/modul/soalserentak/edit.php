<?php
include_once "../config/koneksi.php";
if(isset($_POST['save'])){
    $id=$_POST['id'];
    $soal=$_POST['soal'];
    $p1=$_POST['p1'];
    $p2=$_POST['p2'];
    $p3=$_POST['p3'];
    $p4=$_POST['p4'];
    $p5=$_POST['p5'];
    $paket=$_POST['paket'];
    $jenis=$_POST['jenis'];
    if($jenis!="TKP"){
        $kunci=$_POST['kunci'];
        if($kunci!="na"){
            $na=0;
        }
        else {
            $na=5;
        }
        if($kunci!="nb"){
            $nb=0;
        }
        else {
            $nb=5;
        }
        if($kunci!="nc"){
            $nc=0;
        }
        else {
            $nc=5;
        }
        if($kunci!="nd"){
            $nd=0;
        }
        else {
            $nd=5;
        }
        if($kunci!="ne"){
            $ne=0;
        }
        else {
            $ne=5;
        }
    }
    else{
        $na=$_POST['na'];
        $nb=$_POST['nb'];
        $nc=$_POST['nc'];
        $nd=$_POST['nd'];
        $ne=$_POST['ne'];
    }
    $sql=mysqli_query($conn,"UPDATE soal set soal='$soal', a='$p1', na='$na', b= '$p2', nb='$nb', c='$p3', nc='$nc', d='$p4', nd='$nd', e='$p5', ne='$ne' where id_soal='$id' ");
    $nql=mysqli_query($conn,"UPDATE paketsoalserentak set diubah=CURRENT_TIMESTAMP WHERE id_paket='$paket' ");
    if($sql){
        echo "<script> alert('Soal Berhasil diubah'); document.location.href = '?module=tampilsoalserentak&id=$paket&jenis=$jenis';</script>";
    }
    else{
        echo "<script> alert('Terjadi Kesalahan');</script>";
    }
}
if(isset($_GET['id'])){
    $id=$_GET['id'];
    $j = array('a','b','c','d','e');
    $rsql=mysqli_query($conn,"SELECT * FROM soalserentak WHERE id_soal='$id'");
    $rd=mysqli_fetch_array($rsql);
    $jenis=$rd['jenis'];
    $id_paket=$rd['id_paket'];
?>
<div class='content-wrapper'>
    <section class='content-header'>
        <h1>
            Tambah
            <small>Soal</small>
        </h1>
        <ol class='breadcrumb'>
            <li>
                <a href='#'>
                    <i class='fa fa-dashboard'></i>
                    Dashboard</a>
            </li>
            <li class='active'>Paket Soal</li>
        </ol>
    </section>

    <section class='content'>
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class='box-title'>
                            Tambah Soal
                        </h3>
                    </div>
                    <?php
                    if($jenis!="TKP"){
                    ?>
                    <div class="box-body">
                        <form
                            class='stdform stdform2'
                            method="POST"
                            action=''
                            enctype='multipart/form-data'>
                            <label>
                                Soal
                            </label>
                            <div class='form-group'>
                                <textarea
                                    class='form-control richtext'
                                    name='soal'
                                    rows='3'
                                    placeholder='Isi soal ...'><?php echo $rd['soal']; ?></textarea>
                            </div>
                            <br>
                            <label>
                                Pilihan A
                            </label>
                            <div class='form-group'>
                                <textarea
                                    class='form-control richtextsimple'
                                    name='p1'
                                    rows='3'
                                    placeholder='pilihan A'><?php echo $rd['a']; ?></textarea>
                            </div>
                            <br>

                            <label>
                                Pilihan B
                            </label>
                            <div class='form-group'>

                                <textarea
                                    class='form-control richtextsimple'
                                    name='p2'
                                    rows='3'
                                    placeholder='pilihan B'><?php echo $rd['b']; ?></textarea>
                            </div>
                            <br>
                            <label>
                                Pilihan C
                            </label>
                            <div class='form-group'>
                                <textarea
                                    class='form-control richtextsimple'
                                    name='p3'
                                    rows='3'
                                    placeholder='pilihan C'><?php echo $rd['c']; ?></textarea>
                            </div>
                            <br>
                            <label>
                                Pilihan D
                            </label>
                            <div class='form-group'>
                                <textarea
                                    class='form-control richtextsimple'
                                    name='p4'
                                    rows='3'
                                    placeholder='pilihan D'><?php echo $rd['d']; ?></textarea>
                            </div>
                            <br>
                            <label>
                                Pilihan E
                            </label>
                            <div class='form-group'>
                                <textarea
                                    class='form-control richtextsimple'
                                    name='p5'
                                    rows='3'
                                    placeholder='pilihan E'><?php echo $rd['e']; ?></textarea>
                            </div>
                            <br>
                            <label>
                                Kunci
                            </label>
                            <div class='form-group'>

                                <select name='kunci' class='form-control'>
                                    <option value="na" <?php if($rd['na']=="5"){echo "selected";} ?>>A</option>
                                    <option value="nb" <?php if($rd['nb']=="5"){echo "selected";} ?>>B</option>
                                    <option value="nc" <?php if($rd['nc']=="5"){echo "selected";} ?>>C</option>
                                    <option value="nd" <?php if($rd['nd']=="5"){echo "selected";} ?>>D</option>
                                    <option value="ne" <?php if($rd['ne']=="5"){echo "selected";} ?>>E</option>
                                </select>
                            </div>
                            <input type="hidden" name="id" value="<?php echo $id;?>">
                            <input type="hidden" name="jenis" value="<?php echo $jenis;?>">
                            <input type="hidden" name="paket" value="<?php echo $id_paket;?>">
                            <p class='stdformbutton'>
                                <button class='btn btn-primary' type="submit" name="save">Simpan</button>
                                <input
                                    type="button"
                                    value="Batal"
                                    onclick="self.history.back()"
                                    class='btn btn-warning btn-rounded'>

                            </p>
                        </form>
                    </div>
                    <?php
                    }
                    else{
                        ?>
                        <div class="box-body">
                        <form
                            class='stdform stdform2'
                            method="POST"
                            action=''
                            enctype='multipart/form-data'>
                            <label>
                                Soal
                            </label>
                            <div class='form-group'>
                                <textarea
                                    class='form-control richtext'
                                    name='soal'
                                    rows='3'
                                    placeholder='Isi soal ...'><?php echo $rd['soal']; ?></textarea>
                            </div>
                            <br>
                            <label>
                                Pilihan A
                            </label>
                            <div class='form-group'>
                                <textarea
                                    class='form-control richtextsimple'
                                    name='p1'
                                    rows='3'
                                    placeholder='pilihan A'><?php echo $rd['a']; ?></textarea>
                                <select name='na' class='form-control' required>
                                    <option selected disabled>-- Pilih Nilai --</option>
                                    <option value="1" <?php if($rd['na']=="1"){ echo "selected";} ?>>1</option>
                                    <option value="2" <?php if($rd['na']=="2"){ echo "selected";} ?>>2</option>
                                    <option value="3" <?php if($rd['na']=="3"){ echo "selected";} ?>>3</option>
                                    <option value="4" <?php if($rd['na']=="4"){ echo "selected";} ?>>4</option>
                                    <option value="5" <?php if($rd['na']=="5"){ echo "selected";} ?>>5</option>
                                </select>
                            </div>
                            <br>

                            <label>
                                Pilihan B
                            </label>
                            <div class='form-group'>

                                <textarea
                                    class='form-control richtextsimple'
                                    name='p2'
                                    rows='3'
                                    placeholder='pilihan B'><?php echo $rd['b']; ?></textarea>
                                    <select name='nb' class='form-control' required>
                                    <option selected disabled>-- Pilih Nilai --</option>
                                    <option value="1" <?php if($rd['nb']=="1"){ echo "selected";} ?>>1</option>
                                    <option value="2" <?php if($rd['nb']=="2"){ echo "selected";} ?>>2</option>
                                    <option value="3" <?php if($rd['nb']=="3"){ echo "selected";} ?>>3</option>
                                    <option value="4" <?php if($rd['nb']=="4"){ echo "selected";} ?>>4</option>
                                    <option value="5" <?php if($rd['nb']=="5"){ echo "selected";} ?>>5</option>
                                </select>
                            </div>
                            <br>
                            <label>
                                Pilihan C
                            </label>
                            <div class='form-group'>
                                <textarea
                                    class='form-control richtextsimple'
                                    name='p3'
                                    rows='3'
                                    placeholder='pilihan C'><?php echo $rd['c']; ?></textarea>
                                    <select name='nc' class='form-control' required>
                                    <option selected disabled>-- Pilih Nilai --</option>
                                    <option value="1" <?php if($rd['nc']=="1"){ echo "selected";} ?>>1</option>
                                    <option value="2" <?php if($rd['nc']=="2"){ echo "selected";} ?>>2</option>
                                    <option value="3" <?php if($rd['nc']=="3"){ echo "selected";} ?>>3</option>
                                    <option value="4" <?php if($rd['nc']=="4"){ echo "selected";} ?>>4</option>
                                    <option value="5" <?php if($rd['nc']=="5"){ echo "selected";} ?>>5</option>
                                </select>
                            </div>
                            <br>
                            <label>
                                Pilihan D
                            </label>
                            <div class='form-group'>
                                <textarea
                                    class='form-control richtextsimple'
                                    name='p4'
                                    rows='3'
                                    placeholder='pilihan D'><?php echo $rd['d']; ?></textarea>
                                    <select name='nd' class='form-control' required>
                                    <option selected disabled>-- Pilih Nilai --</option>
                                    <option value="1" <?php if($rd['nd']=="1"){ echo "selected";} ?>>1</option>
                                    <option value="2" <?php if($rd['nd']=="2"){ echo "selected";} ?>>2</option>
                                    <option value="3" <?php if($rd['nd']=="3"){ echo "selected";} ?>>3</option>
                                    <option value="4" <?php if($rd['nd']=="4"){ echo "selected";} ?>>4</option>
                                    <option value="5" <?php if($rd['nd']=="5"){ echo "selected";} ?>>5</option>
                                </select>
                            </div>
                            <br>
                            <label>
                                Pilihan E
                            </label>
                            <div class='form-group'>
                                <textarea
                                    class='form-control richtextsimple'
                                    name='p5'
                                    rows='3'
                                    placeholder='pilihan E'><?php echo $rd['e']; ?></textarea>
                                    <select name='ne' class='form-control' required>
                                    <option selected disabled>-- Pilih Nilai --</option>
                                    <option value="1" <?php if($rd['ne']=="1"){ echo "selected";} ?>>1</option>
                                    <option value="2" <?php if($rd['ne']=="2"){ echo "selected";} ?>>2</option>
                                    <option value="3" <?php if($rd['ne']=="3"){ echo "selected";} ?>>3</option>
                                    <option value="4" <?php if($rd['ne']=="4"){ echo "selected";} ?>>4</option>
                                    <option value="5" <?php if($rd['ne']=="5"){ echo "selected";} ?>>5</option>
                                </select>
                            </div>
                            <br>
                            <input type="hidden" name="id" value="<?php echo $id;?>">
                            <input type="hidden" name="jenis" value="<?php echo $jenis;?>">
                            <input type="hidden" name="paket" value="<?php echo $id_paket;?>">
                            <p class='stdformbutton'>
                                <button class='btn btn-primary' type="submit" name="save">Simpan</button>
                                <input
                                    type="button"
                                    value="Batal"
                                    onclick="self.history.back()"
                                    class='btn btn-warning btn-rounded'>

                            </p>
                        </form>
                    </div>
                        <?php
                    }
                    ?>
                </div>
            </div>
        </div>
    </section>
</div>
<?php
}?>