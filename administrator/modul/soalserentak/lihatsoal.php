
<?php
include_once "../config/koneksi.php";
if(isset($_GET['id'])&&isset($_GET['jenis'])){
    $id=$_GET['id'];
    $jenis=$_GET['jenis'];
    $j = array('a','b','c','d','e');
?>
<script>
function import_data(){
   var formdata = new FormData();      
   var file = $('#file')[0].files[0];
   formdata.append('file', file);
   $.each($('#modal_upload form').serializeArray(), function(a, b){
      formdata.append(b.name, b.value);
   });
   $.ajax({
      url: 'modul/mod_guru/upload.php',
      data: formdata,
      processData: false,
      contentType: false,
      type: 'POST',
      success: function(data) {
         if(data=="ok"){
           $('#modal_upload').modal('hide');
           document.location = "?module=soal";
         }else{
            alert(data);
         }
      },
      error: function(data){
         alert('Tidak dapat mengimport data!');
      }
   });
   return false;
}

</script>

<style>
    .example-modal .modal {
      position: relative;
      top: auto;
      bottom: auto;
      right: auto;
      left: auto;
      display: block;
      z-index: 1;
    }

    .example-modal .modal {
      background: transparent !important;
    }
  </style>
<div class='content-wrapper'>
    <section class='content-header'>
        <h1>
            Daftar
            <small>Paket Soal</small>
        </h1>
        <ol class='breadcrumb'>
            <li>
                <a href='#'>
                    <i class='fa fa-dashboard'></i>
                    Dashboard</a>
            </li>
            <li class='active'>Paket Soal</li>
        </ol>
    </section>

    <section class='content'>
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class='box-title'>
                            <a href='?module=tambahsoalserentak&id=<?php echo $id;?>&jenis=<?php echo $jenis;?>'>
                                <button type='button' class='btn btn-block btn-primary'>
                                    <i class='glyphicon glyphicon-pencil'></i>
                                    Tambah Soal</button>
                            </a>
                        </h3>
                        <button
                            type='button'
                            class='btn btn-info pull-right'
                            data-toggle='modal'
                            data-target='#modal_upload'>
                            <i class='fa fa-file-excel-o'></i>
                            upload dari excel
                        </button>
                    </div>
                    <div class="box-body">
                        <table id='example1' class='table table-bordered table-striped'>
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Soal</th>
                                    <th>Pilihan Berganda</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                            $sql=mysqli_query($conn,"SELECT * FROM `soalserentak` WHERE id_paket='$id' AND jenis='$jenis' ORDER BY id_soal ASC");
                            if(mysqli_num_rows($sql)){
                                $no=1;
                                while ($r=mysqli_fetch_array($sql)) {
                                    ?>
                                <tr>
                                    <td><?php echo $no; ?></td>
                                    <td><?php echo $r['soal']; ?></td>
                                    <td>
                                    <?php
                                    for ($i=0; $i < 5; $i++) {
                                        $a="n".$j[$i];
                                        $b=$j[$i];
                                        if($jenis!="TKP"){
                                            if($r[$a]=="5"){
                                                echo "<b>$b. $r[$b]</b><br>";
                                            }
                                            else{
                                                echo "$b. $r[$b]<br>";
                                            }
                                        }
                                        else{
                                            echo "$b. $r[$b]  -- (poin=$r[$a])<br>";
                                        }
                                    }
                                    ?>
                                    </td>
                                    <td>

                                        <a
                                            href="?module=editsoalserentak&id=<?php echo $r['id_soal']; ?>"
                                            class='btn btn-info'>
                                            <i class='fa fa-pencil'></i>
                                        </a>
                                        <a
                                            href="?module=hapussoalserentak&id=<?php echo $r['id_soal'];?>&paket=<?php echo $r['id_paket'];?>&jenis=<?php echo $r['jenis'];?>" class='btn btn-danger'>
                                            <i class='fa fa-trash'></i>
                                        </a>
                                    </td>
                                </tr>
                            <?php
                            $no++;
                                }
                            }
                            else{
                                ?>
                                <tr>
                                    <td colspan="7" class="text-center">Belum Ada Soal
                                        <?php echo $jenis; ?>
                                        Tersedia</td>
                                </tr>
                                <?php
                            }
                            ?>

                            </tbody>
                        </table>
                    </div>
                    <div class="box-footer">
                        <a href="?module=soal&id=<?php echo $id;?>" class="btn btn-flat btn-default">Kembali</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Modal uploa xls -->
<div class='modal modal-info fade' id='modal_upload'>
          <div class='modal-dialog'>
            <div class='modal-content'>
              <div class='modal-header'>
                
                  
                <h4 class='modal-title'>Upload EXCEL</h4>
              </div>
			  <form enctype='multipart/form-data'  onsubmit='return import_data()'>
  <input type="hidden" name="id" value="<?php echo $id; ?>">
  <input type="hidden" name="jenis" value="<?php echo $jenis; ?>">
              <div class='modal-body'>
            <div class='form-group has-feedback'>
		
			 <input type='file' class='file' id='file' name='file' required>
      </div>
              </div>
              <div class='modal-footer'>
				 <a href='../download/soal.xls' class='btn btn-outline pull-left'>contoh excel</a>
                 <button type='submit' class='btn btn-primary btn-save'>Upload</button>
              

              </div>
            </form>
			</div>
			
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->";
</div>
<?php
}?>
