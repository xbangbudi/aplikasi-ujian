<?php
include_once "../config/koneksi.php";
if(isset($_GET['id'])){
    $id=$_GET['id'];
$twk=mysqli_num_rows(mysqli_query($conn,"SELECT * FROM soal WHERE id_paket='$id' and jenis='TWK' "));
$tiu=mysqli_num_rows(mysqli_query($conn,"SELECT * FROM soal WHERE id_paket='$id' and jenis='TIU' "));
$tkp=mysqli_num_rows(mysqli_query($conn,"SELECT * FROM soal WHERE id_paket='$id' and jenis='TKP' "));
?>
<div class='content-wrapper'>
    <section class='content-header'>
        <h1>
            Paket Soal
            <small></small>
        </h1>
        <ol class='breadcrumb'>
            <li>
                <a href='#'>
                    <i class='fa fa-dashboard'></i>
                    Dashboard</a>
            </li>
            <li class='active'>Paket Soal</li>
        </ol>
    </section>
    
    <section class='content'>
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class='box-title'>
                            Jenis Soal
                        </h3>
                    </div>
                    <div class="box-body">
                        <a href="?module=tampilsoal&id=<?php echo $id; ?>&jenis=TWK">
                            <button type="button" class="btn btn-block btn-flat btn-primary">Paket TWK - Tes Wawasan Kebangsaan (<?php echo $twk; ?> Soal)</button>
                        </a><br>
                        <a href="?module=tampilsoal&id=<?php echo $id; ?>&jenis=TIU">
                            <button type="button" class="btn btn-block btn-flat btn-success">Paket TIU - Tes Intelegensia Umum (<?php echo $tiu; ?> Soal)</button>
                        </a><br>
                        <a href="?module=tampilsoal&id=<?php echo $id; ?>&jenis=TKP">
                            <button type="button" class="btn btn-block btn-flat btn-info">Paket TKP - Test Karakteristik Pribadi (<?php echo $tkp; ?> Soal)</button>
                        </a>
                    </div>
                    <div class="box-footer">
                        <a href="?module=paketsoal" class="btn btn-flat btn-default">Kembali</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<?php
}?>