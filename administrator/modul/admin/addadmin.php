<?php
include_once "../config/koneksi.php";
$text="";
if(isset($_POST['create'])){
    $user=$_POST['user'];
    $username=mysqli_real_escape_string($conn,$user);
    $password=md5($username);
    $rsql=mysqli_query($conn,"SELECT * FROM admin WHERE username='$username'");
    if(mysqli_num_rows($rsql)){
        $text="Username <i>\"$username\"</i> telah digunakan.";
    }
    else{
        $sql=mysqli_query($conn,"INSERT INTO admin(username, password, akses) VALUES('$username','$password','user')");
        if($sql){
            echo "<script> alert('Akun berhasil dibuat'); document.location.href = '?module=admin';</script>";
        }
        else{
        echo "<script> alert('Gagal membuat akun. Terjadi kesalahan');</script>";
        }
    }
}
?>
<div class='content-wrapper'>
    <section class='content-header'>
        <h1>
            Data
            <small>Admin</small>
        </h1>
        <ol class='breadcrumb'>
            <li>
                <a href='#'>
                    <i class='fa fa-dashboard'></i>
                    Dashboard</a>
            </li>
            <li class='active'>Admin</li>
        </ol>
    </section>

    <section class='content'>
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class='box-title'>
                            Tambah Data Administrator
                        </h3>
                    </div>
                    <div class="box-body">
                        <form action="" method="POST">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Username</label>
                                <input
                                    type="text"
                                    maxlength="10"
                                    name="user"
                                    class="form-control"
                                    id="exampleInputEmail1"
                                    placeholder="Masukkan Username" required>
                                    <div class="form-group has-error">
                                        <span class="help-block"><?php echo $text; ?></span>
                                    </div>
                                    <p class="help-block">Maksimal Username 10 karakter.<br>
                                        Password akan dibuat secara otomatis 
                                    </p>
                            </div>
                    </div>
                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary" name="create"><i class="fa fa-check"></i> Buat Akun</button>
                        <a class="btn btn-warning" href="?module=admin"><i class="fa fa-mail-reply"></i> Kembali</a>
                      </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
</div>