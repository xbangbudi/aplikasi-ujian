<?php
include_once "../config/koneksi.php";
?>
<div class='content-wrapper'>
    <section class='content-header'>
        <h1>
            Data
            <small>Admin</small>
        </h1>
        <ol class='breadcrumb'>
            <li>
                <a href='#'>
                    <i class='fa fa-dashboard'></i>
                    Dashboard</a>
            </li>
            <li class='active'>Admin</li>
        </ol>
    </section>
    
    <section class='content'>
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class='box-title'>
                            Data Administrator
                        </h3>
                        <a href='?module=tambahadmin'>
                                <button type='button' class='btn btn-primary pull-right'>
                                    <i class='glyphicon glyphicon-plus'></i>
                                    Tambah Admin</button>
                            </a>
                    </div>
                    <div class="box-body">
                        <table id='example1' class='table table-bordered table-striped'>
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Username</th>
                                    <th>Password(trenkripsi)</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                            $sql=mysqli_query($conn,"SELECT * FROM `admin` ORDER BY id DESC");
                            if(mysqli_num_rows($sql)){
                                $no=1;
                                while ($r=mysqli_fetch_array($sql)) {
                                    ?>
                                <tr>
                                    <td><?php echo $no; ?></td>
                                    <td><?php echo $r['username']; ?></td>
                                    <td><?php echo $r['password']; ?></td>
                                    <td>
                                        <a href="?module=resetadmin&id=<?php echo $r['id']; ?>&action=reset" class='btn btn-info'>
                                            <i class='fa  fa-print'></i> <span>Reset Password</span>
                                        </a>
                                        <a
                                            href="?module=resetadmin&id=<?php echo $r['id'];?>&action=delete"
                                            class='btn btn-danger'
                                            onclick="return confirm('Menghapus akun admin akan mengakibatkan tidak ditemukannya username akun pada daerah konfirmasi pembayaran. Yakin ingin melanjutkan?');">
                                            <i class='fa fa-trash'></i>
                                        </a>
                                    </td>
                                </tr>
                            <?php
                            $no++;
                                }
                            }
                            else{
                                ?>
                                <tr>
                                    <td colspan="7" class="text-center">Belum Ada Administrator</td>
                                </tr>
                                <?php
                            }
                            ?>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>