<?php
include_once "../config/koneksi.php";
if(isset($_GET['kd_jenis'])&&isset($_GET['kd_tryout'])&&isset($_GET['kd_ujian'])){
    $kd_jenis=$_GET['kd_jenis'];
    $kd_to=$_GET['kd_tryout'];
    $kd_uji=$_GET['kd_ujian'];
    $j = array('a','b','c','d','e');

$queryKeterangan = mysqli_query($conn,
                    "SELECT a.keterangan
                    FROM ref_jenis_soal a
                    LEFT JOIN ref_soal b
                    ON a.kd_jenis = b.kd_jenis
                    WHERE a.kd_jenis = $kd_jenis");
$Keterangan = mysqli_fetch_array($queryKeterangan);
?>
<script>
   function counts() {
       var n=$("input:checked").length;
       $("#ttl").text(n);
   }
    
</script>
<div class='content-wrapper'>
    <section class='content-header'>
        <h1>
            Daftar
            <small>Bank Soal</small>
        </h1>
        <ol class='breadcrumb'>
            <li>
                <a href='#'>
                    <i class='fa fa-dashboard'></i>
                    Dashboard</a>
            </li>
            <li class='active'>Bank Soal</li>
        </ol>
    </section>

    <section class='content'>
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class='box-title'>
                            
                        </h3>
                        <!-- <button type='button' class='btn btn-info pull-right' data-toggle='modal'
                        data-target='#modal_upload'> <i class='fa fa-file-excel-o'></i> upload dari
                        excel </button> -->
                    </div>
                    <div class="box-body">
                        <form
                            action="?module=proses_ta&kd_jenis=<?= $kd_jenis?>&kd_tryout=<?= $kd_to?>&kd_ujian=<?= $kd_uji?>"
                            method="post">
                            <table id='example1' class='table table-bordered table-striped'>
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Soal</th>
                                        <th>Pilihan Berganda</th>
                                        <th>Pilih</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                            $sql=mysqli_query($conn,"SELECT * FROM `ref_soal` WHERE kd_jenis='$kd_jenis' ORDER BY kd_soal ASC");
                            $nos=0;
                            if(mysqli_num_rows($sql)){
                                $no=1;
                                while ($data=mysqli_fetch_array($sql)) {
                                    $cql=mysqli_query($conn,"SELECT * FROM `ta_tryout` WHERE kd_jenis='$kd_jenis' AND kd_soal=".$data['kd_soal']." AND kd_tryout='$kd_to' AND kd_ujian='$kd_uji'");
                                    $rd=mysqli_num_rows($cql);
                                    if(!$rd){
                                    ?>
                                    <tr>
                                        <td><?php echo $no; ?></td>
                                        <td><?php echo $data['soal']; ?></td>
                                        <td>
                                        <?php
                                    for ($i=0; $i < 5; $i++) {
                                        $a="n".$j[$i];
                                        $b=$j[$i];
                                        if($kd_jenis != 3){
                                            if($data[$a]=="5"){
                                                echo "<b>$b. $data[$b]</b><br>";
                                            }
                                            else{
                                                echo "$b. $data[$b]<br>";
                                            }
                                        }
                                        else{
                                            echo "$b. $data[$b]  -- (poin=$data[$a])<br>";
                                        }
                                    }
                                    ?>
                                        </td>
                                        <td>
                                            <input id="checkout" class="case" rel="<?= $no ?>" type="checkbox" name="kd_soal[]" value="<?= $data['kd_soal']?>" onchange="counts()" >
                                        </td>
                                    </tr>
                                <?php
                                    }
                            $no++;
                            $nos++;
                                }
                            }
                            else{
                                ?>
                                    <tr>
                                        <td colspan="7" class="text-center">Belum Ada Soal
                                            <?=$Keterangan["keterangan"] ?>
                                            Tersedia</td>
                                    </tr>
                                    <?php
                            }
                            ?>

                                </tbody>
                            </table>
                        </div>
                        <div class="box-footer">
                            <span id="ttl">0</span> dari <?= $nos?> tercentang
                            <input type='hidden' name='kd_tryout' value='<?= $kd_to?>'>
                            <input type='hidden' name='kd_ujian' value='<?= $kd_uji?>'>
                            <input type='hidden' name='kd_jenis' value='<?= $kd_jenis?>'>
                            <button type="submit" name='save' class='btn btn-info pull-right'>Simpan</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
   
</section>
<?php
}?>