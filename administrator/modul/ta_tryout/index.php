<?php
include_once "../config/koneksi.php";
?>
<div class='content-wrapper'>
    <section class='content-header'>
        <h1>
            Data Tryout
        </h1>
        <ol class='breadcrumb'>
            <li>
                <a href='#'>
                    <i class='fa fa-dashboard'></i>
                    Dashboard</a>
            </li>
            <li class='active'>Data Tryout</li>
        </ol>
    </section>
    
    <section class='content'>
        <div class="row">
        <?php 
        if(isset($_GET['kd_ujian'])){
            $kd_uji=$_GET['kd_ujian'];
        }
        else {
            $kd_uji="";
        }
            $sql=mysqli_query($conn,"SELECT * FROM `ref_tryout` ORDER BY kd_tryout ASC");
            // print_r($data);exit;
        if(mysqli_num_rows($sql)){
            while ($data=mysqli_fetch_array($sql)) {
        ?>
            <div class="col-md-4">
                <div class="small-box bg-primary">
                <div class="inner">
                    <h3><?=$data["keterangan"]?></h3>
                    <p><?=$data["jam"]?></p>
                </div>
                <div class="icon">
                    <i class="fa fa-cube"></i>
                </div>
                <a href="?module=edit_soalta&kd_tryout=<?= $data['kd_tryout']?>&kd_ujian=<?= $kd_uji ?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
        <?php } } ?>
        </div>
    </section>
</div>
