<?php
include_once "../config/koneksi.php";
if (isset($_POST['save'])) {
    $kode=$_POST['kode'];
    $judul=$_POST['judul'];
    $kd_ujian=$_POST['kd_ujian'];
    $sql=mysqli_query($conn,"INSERT INTO ref_paket(kd_ujian, kd_paket, keterangan) VALUES ('$kd_ujian', '$kode','$judul') ");
    if($sql){
        echo "<script> alert('Paket Berhasil ditambahkan'); document.location.href = '?module=ref_paket';</script>";
    }
    else{
        echo "<script> alert('Terjadi Kesalahan');</script>";
    }
}
$kd_ujian=$_GET['kd_ujian'];

$setId = mysqli_query($conn,"SELECT max(kd_paket) FROM ref_paket WHERE kd_ujian = $kd_ujian");
        $no = mysqli_fetch_array($setId);
        if ($no) {
            $nomor = $no[0];
            $urut = (int) $nomor;
            $kode = $urut + 1;
        }else{
            $kode = '1';
        }
// var_dump($query["kd_ujian"]);exit;

?>
<div class='content-wrapper'>
    <section class='content-header'>
        <h1>
            Daftar
            <small>Paket Soal</small>
        </h1>
        <ol class='breadcrumb'>
            <li>
                <a href='#'>
                    <i class='fa fa-dashboard'></i>
                    Dashboard</a>
            </li>
            <li class='active'>Paket Soal</li>
        </ol>
    </section>

    <section class='content'>
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class='box-title'>
                            Tambah Paket
                        </h3>
                    </div>
                    <div class="box-body">
                        <form class='stdform stdform2' method="POST" action='' enctype='multipart/form-data'>
                            <div class='input-group'>
                                <span class='input-group-addon'>
                                    <i class='fa fa-pencil'></i>
                                </span>
                                <input type='hidden' name='kd_ujian' value='<?=$kd_ujian?>' class='form-control'>
                                <input type='hidden' name='kode' value='<?=$kode?>' class='form-control'>
                                <input type='text' name='judul' class='form-control' placeholder='Nama Paket' required="required">
                            </div>
                            <br>
                            <p class='stdformbutton'>
                                <button class='btn btn-primary' type="submit" name="save">Simpan</button>
								<input type=button value=Batal onclick=self.history.back() class='btn btn-warning btn-rounded'>  
                            </p>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>