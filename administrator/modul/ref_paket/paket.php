<?php
include_once "../config/koneksi.php";
    $kd_ujian = $_GET['kd_ujian'];
    $namaUjian = mysqli_query($conn, "SELECT a.keterangan
                                FROM ref_jenis_ujian a
                                RIGHT JOIN ref_paket b
                                ON a.kd_ujian = b.kd_ujian
                                WHERE a.kd_ujian = $kd_ujian");
    $dataUjian=mysqli_fetch_assoc($namaUjian);
    // echo "<pre>";
    // print_r($dataUjian);
    // echo "</pre>";
    // exit();

?>
<div class='content-wrapper'>
    <section class='content-header'>
        <h1>
            Daftar Paket <?=ucwords(strtolower($dataUjian["keterangan"]))?>
        </h1>
        <ol class='breadcrumb'>
            <li>
                <a href='#'>
                    <i class='fa fa-dashboard'></i>
                    Dashboard</a>
            </li>
            <li class='active'>Paket Soal</li>
        </ol>
    </section>
    <section class='content'>
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class='box-title'>
                            <a href='?module=add_paket&kd_ujian=<?= $kd_ujian ?>'>
                                <button type='button' class='btn btn-block btn-primary'>
                                    <i class='glyphicon glyphicon-pencil'></i>
                                    Tambah Paket</button>
                            </a>
                        </h3>
                    </div>
                    <div class="box-body table-responsive">
                        <table id='example1' class='table table-bordered table-striped'>
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Nama Paket</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                        <?php
                            $sql=mysqli_query($conn,"SELECT * FROM `ref_paket` WHERE kd_ujian = $kd_ujian ORDER BY kd_paket ASC");
                            if(mysqli_num_rows($sql)){
                                $no=1;
                                while ($data=mysqli_fetch_array($sql)) {
                                    ?>
                                <tr>
                                    <td><?php echo $no; ?></td>
                                    <td><?php echo $data['keterangan']; ?></td>
                                    <td>
                                        <!-- <a href="export/soal.php?id=<?php echo $data['kd_paket']; ?>" class='btn btn-info'>
                                            <i class='fa  fa-print'></i>
                                        </a> -->
                                        <a href="?module=edit_paket&id=<?php echo $data['kd_paket']; ?>&kd_ujian=<?= $kd_ujian ?>" class='btn btn-info'>
                                            <i class='fa fa-pencil'></i>
                                        </a>
                                        <a
                                            href="?module=hapus_paket&id=<?php echo $data['kd_paket'];?>&kd_ujian=<?= $kd_ujian ?>"
                                            class='btn btn-danger'
                                            onclick="return confirm('Yakin ingin menghapus paket ini?');">
                                            <i class='fa fa-trash'></i>
                                        </a>
                                    </td>
                                </tr>
                            <?php
                            $no++;
                                }
                            }
                            else{
                                ?>
                                <tr>
                                    <td colspan="7" class="text-center">Belum Ada Paket Tersedia</td>
                                </tr>
                                <?php
                            }
                            ?>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
