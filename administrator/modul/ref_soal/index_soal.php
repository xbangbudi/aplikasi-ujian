
<?php
include_once "../config/koneksi.php";
if(isset($_GET['kd_jenis'])){
    $kd_jenis=$_GET['kd_jenis'];
    $j = array('a','b','c','d','e');

$queryKeterangan = mysqli_query($conn,
                    "SELECT a.keterangan
                    FROM ref_jenis_soal a
                    LEFT JOIN ref_soal b
                    ON a.kd_jenis = b.kd_jenis
                    WHERE a.kd_jenis = $kd_jenis");
$Keterangan = mysqli_fetch_array($queryKeterangan);
?>
<div class='content-wrapper'>
    <section class='content-header'>
        <h1>
            Daftar
            <small>Bank Soal</small>
        </h1>
        <ol class='breadcrumb'>
            <li>
                <a href='#'>
                    <i class='fa fa-dashboard'></i>
                    Dashboard</a>
            </li>
            <li class='active'>Bank Soal</li>
        </ol>
    </section>

    <section class='content'>
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class='box-title'>
                            <a href='?module=add_soal&kd_jenis=<?=$kd_jenis;?>'>
                                <button type='button' class='btn btn-block btn-primary'>
                                    <i class='glyphicon glyphicon-pencil'></i>
                                    Tambah Soal</button>
                            </a>
                        </h3>
                        <!-- <button
                            type='button'
                            class='btn btn-info pull-right'
                            data-toggle='modal'
                            data-target='#modal_upload'>
                            <i class='fa fa-file-excel-o'></i>
                            upload dari excel
                        </button> -->
                    </div>
                    <div class="box-body">
                        <table id='example1' class='table table-bordered table-striped'>
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Soal</th>
                                    <th>Pilihan Berganda</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                            $sql=mysqli_query($conn,"SELECT * FROM `ref_soal` WHERE kd_jenis='$kd_jenis' ORDER BY kd_soal ASC");
                            if(mysqli_num_rows($sql)){
                                $no=1;
                                while ($data=mysqli_fetch_array($sql)) {
                                    ?>
                                <tr>
                                    <td><?php echo $no; ?></td>
                                    <td><?php echo $data['soal']; ?></td>
                                    <td>
                                    <?php
                                    for ($i=0; $i < 5; $i++) {
                                        $a="n".$j[$i];
                                        $b=$j[$i];
                                        if($kd_jenis != 3){
                                            if($data[$a]=="5"){
                                                echo "<b>$b. $data[$b]</b><br>";
                                            }
                                            else{
                                                echo "$b. $data[$b]<br>";
                                            }
                                        }
                                        else{
                                            echo "$b. $data[$b]  -- (poin=$data[$a])<br>";
                                        }
                                    }
                                    ?>
                                    </td>
                                    <td>

                                        <a
                                            href="?module=edit_soal&kd_soal=<?= $data['kd_soal']; ?>&kd_jenis=<?= $data['kd_jenis'];?>"
                                            class='btn btn-info'>
                                            <i class='fa fa-pencil'></i>
                                        </a>
                                        <a
                                            href="?module=hapus_soal&kd_soal=<?= $data['kd_soal'];?>&kd_jenis=<?= $data['kd_jenis'];?>" class='btn btn-danger' onclick="return confirm('Yakin ingin menghapus data ini?');">
                                            <i class='fa fa-trash'></i>
                                        </a>
                                    </td>
                                </tr>
                            <?php
                            $no++;
                                }
                            }
                            else{
                                ?>
                                <tr>
                                    <td colspan="7" class="text-center">Belum Ada Soal <?=$Keterangan["keterangan"] ?> Tersedia</td>
                                </tr>
                                <?php
                            }
                            ?>

                            </tbody>
                        </table>
                    </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php
}?>
