<?php
include_once "../config/koneksi.php";
$kd_ujian = $_GET['kd_ujian'];
$kd_paket = $_GET['kd_paket'];
$namaUjian = mysqli_query($conn, "SELECT a.keterangan
                                FROM ref_jenis_ujian a
                                LEFT JOIN ta_harga_paket b
                                ON a.kd_ujian = b.kd_ujian
                                WHERE a.kd_ujian = $kd_ujian");
    $dataUjian=mysqli_fetch_assoc($namaUjian);
?>
<script>
    function counts() {
        var n = $("input:checked").length;
        $("#ttl").text(n);
    }
</script>
<div class='content-wrapper'>
    <section class='content-header'>
        <h1>
            Daftar Tryout
            <?=ucwords(strtolower($dataUjian["keterangan"]))?>
        </h1>
        <ol class='breadcrumb'>
            <li>
                <a href='#'>
                    <i class='fa fa-dashboard'></i>
                    Dashboard</a>
            </li>
            <li class='active'>Paket TO</li>
        </ol>
    </section>

    <section class='content'>
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <form
                        action="?module=proses_hasil&kd_ujian=<?= $kd_ujian?>&kd_paket=<?= $kd_paket?>"
                        method="post">
                        <div class="box-body">
                            <table id='example1' class='table table-bordered table-striped'>
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Nama Tryout</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                            $sql=mysqli_query($conn,"SELECT *
                                                FROM ref_tryout
                                                ");
                                                $nos=0;
                            if(mysqli_num_rows($sql)){
                                $no=1;
                                while ($r=mysqli_fetch_array($sql)) {
                                    $aql=mysqli_query($conn,"SELECT *
                                    FROM ta_hasil WHERE kd_ujian='$kd_ujian' AND kd_paket='$kd_paket' AND kd_tryout=".$r['kd_tryout']);
                                    $ck=mysqli_num_rows($aql);
                                    if(!$ck){
                                ?>
                                    <tr>
                                        <td><?= $no; ?></td>
                                        <td><?= $r['keterangan']; ?></td>
                                        <td>
                                            <input
                                                type="checkbox"
                                                name="pil[]"
                                                value="<?= $r['kd_tryout'] ?>"
                                                id=""
                                                onchange="counts()">
                                        </td>
                                    </tr>
                                <?php
                            $no++;
                            $nos++;
                                    }
                                }
                            }
                            else{
                                ?>
                                    <tr>
                                        <td colspan="7" class="text-center">Belum Ada Paket Tersedia</td>
                                    </tr>
                                    <?php
                            }
                            ?>

                                </tbody>
                            </table>
                        </div>
                        <div class="box-footer">
                            <span id="ttl">0</span>
                            dari
                            <?= $nos?>
                            tercentang
                            <button type="submit" name='save' class='btn btn-info pull-right'>Simpan</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>