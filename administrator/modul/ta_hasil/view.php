<?php
include_once "../config/koneksi.php";
$kd_ujian = $_GET['kd_ujian'];
$kd_paket = $_GET['kd_paket'];
$namaUjian = mysqli_query($conn, "SELECT a.keterangan
                                FROM ref_jenis_ujian a
                                LEFT JOIN ta_harga_paket b
                                ON a.kd_ujian = b.kd_ujian
                                WHERE a.kd_ujian = $kd_ujian");
    $dataUjian=mysqli_fetch_assoc($namaUjian);
?>
<div class='content-wrapper'>
    <section class='content-header'>
        <h1>
            Daftar Tryout
            <?=ucwords(strtolower($dataUjian["keterangan"]))?>
        </h1>
        <ol class='breadcrumb'>
            <li>
                <a href='#'>
                    <i class='fa fa-dashboard'></i>
                    Dashboard</a>
            </li>
            <li class='active'>Paket TO</li>
        </ol>
    </section>

    <section class='content'>
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <a
                            href="?module=list_to&kd_ujian=<?= $kd_ujian ?>&kd_paket=<?= $kd_paket?>"
                            class='btn btn-info'>
                            <i class='fa fa-plus'></i>
                            Kelompokkan Tryout
                        </a>
                    </div>
                    <div class="box-body">
                        <table id='example1' class='table table-bordered table-striped'>
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Nama Tryout</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                            $sql=mysqli_query($conn,"SELECT *
                                                FROM ta_hasil
                                                WHERE kd_ujian='$kd_ujian' AND kd_paket='$kd_paket'");
                            if(mysqli_num_rows($sql)){
                                $no=1;
                                while ($r=mysqli_fetch_array($sql)) {
                                    $anql=mysqli_fetch_array(mysqli_query($conn,"SELECT keterangan FROM ref_tryout WHERE kd_tryout=".$r['kd_tryout']));
                                ?>
                                <tr>
                                    <td><?= $no; ?></td>
                                    <td><?= $anql['keterangan']; ?></td>
                                    <td>
                                        <a
                                            href="?module=del_ta&id=<?= $r['id'] ?>&kd_paket=<?= $kd_paket?>&kd_ujian=<?= $kd_ujian?>"
                                            class='btn btn-danger'
                                            onclick="return confirm('Yakin ingin menghapus data ini?');">
                                            <i class="fa fa-trash"></i>
                                        </a>
                                    </td>
                                </tr>
                            <?php
                            $no++;
                                }
                            }
                            else{
                                ?>
                                <tr>
                                    <td colspan="7" class="text-center">Belum Ada Paket Tersedia</td>
                                </tr>
                                <?php
                            }
                            ?>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>