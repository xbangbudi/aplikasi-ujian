<?php
include_once "../config/koneksi.php";
?>
<div class='content-wrapper'>
    <section class='content-header'>
        <h1>
            Daftar
            <small>Paket Soal</small>
        </h1>
        <ol class='breadcrumb'>
            <li>
                <a href='#'>
                    <i class='fa fa-dashboard'></i>
                    Dashboard</a>
            </li>
            <li class='active'>Paket Soal</li>
        </ol>
    </section>
    
    <section class='content'>
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class='box-title'>
                           <b>Silahkan Pilih Ujian Terlebih Dahulu</b>
                        </h3>
                        <hr>
                    </div>
                    <div class="box-body table-responsive">
                        <div class="row">
                            <div class="col-md-12">
                            <?php 
                            $sql=mysqli_query($conn,"SELECT * FROM `ref_jenis_ujian` ORDER BY kd_ujian ASC");
                                // print_r($data);exit;
                            if(mysqli_num_rows($sql)){
                                while ($data=mysqli_fetch_array($sql)) {
                            ?>
                                <div class="col-md-6">
                                    <div class="small-box bg-gray">
                                        <div class="inner">
                                            <h3><?=$data['keterangan']?></h3>
                                        </div>
                                        <div class="icon">
                                            <i class="fa fa-archive"></i>
                                        </div>
                                            <a href="?module=list_hasil&kd_ujian=<?= $data['kd_ujian']?>" class="small-box-footer"><i class="fa fa-plus-square"></i> Tambah Paket</a>
                                    </div>
                                </div>
                                <?php } }?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
