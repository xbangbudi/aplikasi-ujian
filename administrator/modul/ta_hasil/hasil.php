<?php
include_once "../config/koneksi.php";
$kd_ujian = $_GET['kd_ujian'];
$namaUjian = mysqli_query($conn, "SELECT a.keterangan
                                FROM ref_jenis_ujian a
                                LEFT JOIN ta_harga_paket b
                                ON a.kd_ujian = b.kd_ujian
                                WHERE a.kd_ujian = $kd_ujian");
    $dataUjian=mysqli_fetch_assoc($namaUjian);
?>
<div class='content-wrapper'>
    <section class='content-header'>
        <h1>
            Daftar Paket Tryout <?=ucwords(strtolower($dataUjian["keterangan"]))?>
        </h1>
        <ol class='breadcrumb'>
            <li>
                <a href='#'>
                    <i class='fa fa-dashboard'></i>
                    Dashboard</a>
            </li>
            <li class='active'>Paket TO</li>
        </ol>
    </section>
    
    <section class='content'>
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-body">
                        <table id='example1' class='table table-bordered table-striped'>
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Nama Paket</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                            $sql=mysqli_query($conn,"SELECT *
                                                FROM ref_paket
                                                WHERE kd_ujian = $kd_ujian ORDER BY kd_paket ASC");
                            if(mysqli_num_rows($sql)){
                                $no=1;
                                while ($r=mysqli_fetch_array($sql)) {
                                ?>
                                <tr>
                                    <td><?= $no; ?></td>
                                    <td><?= $r['keterangan']; ?></td>
                                    <td>
                                        <a href="?module=list_to&kd_ujian=<?= $r['kd_ujian'] ?>&kd_paket=<?= $r['kd_paket'];?>" class='btn btn-info'>
                                            <i class='fa fa-plus'></i> Kelompokkan Tryout
                                        </a> 
                                        <a href="?module=view_to&kd_ujian=<?= $r['kd_ujian'] ?>&kd_paket=<?= $r['kd_paket'];?>" class='btn btn-primary'>
                                            <i class='fa fa-eye'></i> Lihat Tryout
                                        </a>
                                    </td>
                                </tr>
                            <?php
                            $no++;
                                }
                            }
                            else{
                                ?>
                                <tr>
                                    <td colspan="7" class="text-center">Belum Ada Paket Tersedia</td>
                                </tr>
                                <?php
                            }
                            ?>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>