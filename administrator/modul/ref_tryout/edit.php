<?php
include_once "../config/koneksi.php";
if (isset($_POST['save'])) {
    $kode=$_POST['kode'];
    $judul=$_POST['judul'];
    $sql=mysqli_query($conn,"UPDATE ref_tryout SET keterangan='$judul' WHERE kd_tryout='$kode' ");
    if($sql){
        echo "<script> alert('Data Berhasil diubah'); document.location.href = '?module=ref_tryout';</script>";
    }
    else{
        echo "<script> alert('Terjadi Kesalahan');</script>";
    }
}
if(isset($_GET['id'])){
    $id=$_GET['id'];
?>
<div class='content-wrapper'>
    <section class='content-header'>
        <h1>
            Daftar
            <small>Data Tryout</small>
        </h1>
        <ol class='breadcrumb'>
            <li>
                <a href='#'>
                    <i class='fa fa-dashboard'></i>
                    Dashboard</a>
            </li>
            <li class='active'>Data Tryout</li>
        </ol>
    </section>

    <section class='content'>
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class='box-title'>
                            Edit Paket
                        </h3>
                    </div>
                    <div class="box-body">
                        <?php
                        $rsql=mysqli_query($conn,"SELECT * FROM ref_tryout WHERE kd_tryout='$id'");
                        $r=mysqli_fetch_array($rsql);
                        ?>
                        <form
                            class='stdform stdform2'
                            method="POST"
                            action=''
                            enctype='multipart/form-data'>
                            <div class='input-group'>
                                <span class='input-group-addon'>
                                    <i class='fa fa-pencil'></i>
                                </span>
                                <input type='hidden' name='kode' value='<?= $r["kd_tryout"] ?>' class='form-control'>
                                <input type='text' name='judul' value='<?= $r["keterangan"] ?>' class='form-control' required="required">
                            </div>
                            <p class='stdformbutton'>
                            <br>
                                <button class='btn btn-primary' type="submit" name="save">Simpan</button>
                                <input
                                    type="button"
                                    value="Batal"
                                    onclick="self.history.back()"
                                    class='btn btn-warning btn-rounded'>
                            </p>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<?php
}
?>