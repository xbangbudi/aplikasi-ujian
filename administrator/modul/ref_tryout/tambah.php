<?php
include_once "../config/koneksi.php";
if (isset($_POST['save'])) {
    $kode = $_POST['kode'];
    $keterangan = $_POST['keterangan'];
    $jam = $_POST['jam'];
    $menit = $_POST['menit'];
    $waktu = $jam.":".$menit;
    $sql=mysqli_query($conn,"INSERT INTO ref_tryout(kd_tryout, keterangan, jam) VALUES ('$kode','$keterangan', '$waktu') ");
    if($sql){
        echo "<script> alert('Paket Berhasil ditambahkan'); document.location.href = '?module=ref_tryout';</script>";
    }
    else{
        echo "<script> alert('Terjadi Kesalahan');</script>";
    }
}
$setId = mysqli_query($conn,"SELECT max(kd_tryout) FROM ref_tryout");
        $no = mysqli_fetch_array($setId);
        if ($no) {
            $nomor = $no[0];
            $urut = (int) $nomor;
            $kode = $urut + 1;
        }else{
            $kode = '1';
        }
?>
<div class='content-wrapper'>
    <section class='content-header'>
        <h1>
            Tambah
            <small>Data Tryout</small>
        </h1>
        <ol class='breadcrumb'>
            <li>
                <a href='#'>
                    <i class='fa fa-dashboard'></i>
                    Dashboard</a>
            </li>
            <li class='active'>Data Tryout</li>
        </ol>
    </section>

    <section class='content'>
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                    </div>
                    <div class="box-body">
                        <form class='stdform stdform2' method="POST" action='' enctype='multipart/form-data'>
                            <div class='input-group'>
                                <span class='input-group-addon'>
                                    <i class='fa fa-pencil'></i>
                                </span>
                                <input type='hidden' name='kode' value='<?=$kode?>' class='form-control'>
                                <input type='text' name='keterangan' class='form-control' placeholder='Keterangan Tryout' required="required">
                            </div>
                            <br>
                            <div class='input-group'>
                                <span class='input-group-addon'>
                                    <i class='fa fa-clock-o'></i>
                                </span>
                                <input type='number' name='jam' class='form-control' placeholder='Waktu Ujian dalam jam' max='12' required="required">
                                <input type='number' name='menit' class='form-control' placeholder='Waktu Ujian dalam menit' max='59' required="required">
                            </div>
                            <br>
                            <p class='stdformbutton'>
                                <button class='btn btn-primary' type="submit" name="save">Simpan</button>
								<input type=button value=Batal onclick=self.history.back() class='btn btn-warning btn-rounded'>  
                            </p>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>