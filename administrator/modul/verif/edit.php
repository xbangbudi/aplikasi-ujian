<?php
include_once "../config/koneksi.php";
if (isset($_POST['save'])) {
    $kode=$_POST['kode'];
    $judul=$_POST['judul'];
    $kd_ujian=$_POST['kd_ujian'];
    $sql=mysqli_query($conn,"UPDATE ref_paket SET keterangan='$judul' WHERE kd_paket='$kode' AND kd_ujian = '$kd_ujian'");
    if($sql){
        echo "<script> alert('Paket Berhasil diubah'); document.location.href = '?module=ref_paket';</script>";
    }
    else{
        echo "<script> alert('Terjadi Kesalahan');</script>";
    }
}
if(isset($_GET['id']) && isset($_GET['kd_ujian'])){
    $id=$_GET['id'];
    $kd_ujian=$_GET['kd_ujian'];
?>
<div class='content-wrapper'>
    <section class='content-header'>
        <h1>
            Daftar
            <small>Paket Soal</small>
        </h1>
        <ol class='breadcrumb'>
            <li>
                <a href='#'>
                    <i class='fa fa-dashboard'></i>
                    Dashboard</a>
            </li>
            <li class='active'>Paket Soal</li>
        </ol>
    </section>

    <section class='content'>
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class='box-title'>
                            Edit Paket
                        </h3>
                    </div>
                    <div class="box-body">
                        <?php
                        $rsql=mysqli_query($conn,"SELECT * FROM ref_paket WHERE kd_paket='$id' AND kd_ujian='$kd_ujian'");
                        $r=mysqli_fetch_array($rsql);
                        ?>
                        <form
                            class='stdform stdform2'
                            method="POST"
                            action=''
                            enctype='multipart/form-data'>
                            <div class='input-group'>
                                <span class='input-group-addon'>
                                    <i class='fa fa-pencil'></i>
                                </span>
                                <input type='hidden' name='kd_ujian' value='<?= $r["kd_ujian"] ?>' class='form-control'>
                                <input type='hidden' name='kode' value='<?= $r["kd_paket"] ?>' class='form-control'>
                                <input type='text' name='judul' value='<?= $r["keterangan"] ?>' class='form-control' required="required">
                            </div>
                            <p class='stdformbutton'>
                            <br>
                                <button class='btn btn-primary' type="submit" name="save">Simpan</button>
                                <input
                                    type="button"
                                    value="Batal"
                                    onclick="self.history.back()"
                                    class='btn btn-warning btn-rounded'>
                            </p>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<?php
}
?>