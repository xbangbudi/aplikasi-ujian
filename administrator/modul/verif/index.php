<?php
include_once "../config/koneksi.php";
?>
<div class='content-wrapper'>
    <section class='content-header'>
        <h1>
            Daftar
            <small>User Belum Dikonfirmasi</small>
        </h1>
        <ol class='breadcrumb'>
            <li>
                <a href='#'>
                    <i class='fa fa-dashboard'></i>
                    Dashboard</a>
            </li>
            <li class='active'>User Belum Dikonfirmasi</li>
        </ol>
    </section>
    
    <section class='content'>
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-body table-responsive">
                        <div class="row">
                            <div class="col-md-12">
                              <table class="table table-striped">
                                    <tr>
                                        <th>Username</th>
                                        <th>Nama</th>
                                        <th>E-mail</th>
                                        <th>No. Handphone</th>
                                        <th>Paket Tryout</th>
                                        <th>Status</th>
                                        <th>Aksi</th>
                                    </tr>
                                    <?php 
                                    $sql=mysqli_query($conn,"SELECT a.*, b.keterangan
                                                                FROM user a
                                                                LEFT JOIN ref_paket b
                                                                ON a.kd_paket = b.kd_paket
                                                                AND a.kd_ujian = b.kd_ujian 
                                                                ORDER BY id ASC");
                                        // print_r($data);exit;
                                    if(mysqli_num_rows($sql)){
                                        while ($data=mysqli_fetch_array($sql)) {
                                    ?>
                                    <tr>
                                        <td><?= $data['kode_reg'] ?></td>
                                        <td><?= $data['nama'] ?></td>
                                        <td><?= $data['email'] ?></td>
                                        <td><?= $data['no_hp'] ?></td>
                                        <td><?= $data['keterangan'] ?></td>
                                        <td>
                                        <?php 
                                        if($data['status_pembayaran'] == 0){ ?>
                                            <div class="btn btn-danger">BELUM MELAKUKAN PEMBAYARAN</div> 
                                        <?php }elseif($data['status_pembayaran'] == 1){ ?>
                                            <div class="btn btn-warning">BELUM DIKONFIRMASI</div> 
                                        <?php }else{ ?>
                                            <div class="btn btn-success">SUDAH DIKONFIRMASI</div> 
                                        <?php  }?>
                                        </td>
                                        <td>
                                        <a class="btn btn-info" href="?module=detail_transaksi&id_user=<?= $data['id']?>">Lihat Detail</a>
                                        </td>
                                    </tr>
                                        <?php } ?>
                              </table>  
                                <?php  }?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
