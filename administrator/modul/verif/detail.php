<?php
include_once "../config/koneksi.php";
    $id_user = $_GET['id_user'];
    // echo "<pre>";
    // print_r($dataUjian);
    // echo "</pre>";
    // exit();


?>
<div class='content-wrapper'>
    <section class='content-header'>
        <h1>
            Detail Transaksi
        </h1>
        <ol class='breadcrumb'>
            <li>
                <a href='#'>
                    <i class='fa fa-dashboard'></i>
                    Dashboard</a>
            </li>
            <li class='active'>Detail Transaksi</li>
        </ol>
    </section>
    <section class='content'>
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-body">
                        <div class="card">
                            <div class="card-body">
                                <?php
                                    $query = "SELECT a.*, b.kode_reg, c.nama_bank
                                                FROM ta_pembayaran a
                                                LEFT JOIN user b
                                                ON a.id_user = b.id
                                                LEFT JOIN ref_bank c
                                                ON a.id_bank = c.id
                                                WHERE id_user = $id_user";
                                    $exec = mysqli_query($conn, $query);
                                    $data = mysqli_fetch_assoc($exec);
                                ?>
                                <table class="table table-stripped">
                                    <tr>
                                        <th>Username</th>
                                        <td>:</td>
                                        <td><?= $data['kode_reg'] ?></td>
                                        <th align="center">Bukti Pembayaran</th>
                                    </tr>
                                    <tr>
                                        <th>Nama Pengirim</th>
                                        <td>:</td>
                                        <td><?= $data['nama_pengirim'] ?></td>
                                        <td rowspan="4">
                                        <?="<img src='../siswa/dashboard/img/bukti/".$data['foto']."'style='width:300px; height:250px;'>"?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>Waktu</th>
                                        <td>:</td>
                                        <td><?= date('d-m-Y H:i:s',$data['tgl_bayar']) ?></td>
                                    </tr>
                                    <tr>
                                        <th>Jumlah Transfer</th>
                                        <td>:</td>
                                        <td>Rp. <?= number_format($data['jumlah'],0,",","."); ?></td>
                                    </tr>
                                    <tr>
                                        <th>Rekening Tujuan</th>
                                        <td>:</td>
                                        <td><?= $data['nama_bank'] ?></td>
                                    </tr>
                                </table>
                            </div>
                            <hr>
                            <div class="card-footer">
                                <a href="?module=verifikasi_user&id=<?=$id_user?>" class="btn btn-success btn-sm"><i class="fa fa-check"></i> Verifikasi</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
