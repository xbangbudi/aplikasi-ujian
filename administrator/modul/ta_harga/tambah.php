<?php
include_once "../config/koneksi.php";

if(isset($_POST['create'])){
    $id = $_POST['kode'];
    $kd_ujian = $_POST['kd_ujian'];
    $kd_paket=$_POST['kd_paket'];
    $harga=$_POST['harga'];
    $diskon=$_POST['diskon'];
    $sql=mysqli_query($conn,"INSERT INTO ta_harga_paket(id, kd_ujian, kd_paket, jumlah, diskon) VALUES ('$id','$kd_ujian','$kd_paket','$harga','$diskon')");
    if($sql){
        echo "<script> alert('Paket Berhasil ditambahkan'); document.location.href = '?module=list_harga&kd_ujian=$kd_ujian';</script>";
    }
    else{
        echo "<script> alert('Paket gagal ditambahkan. Terjadi kesalahan');";
    }
}

$kd_ujian=$_GET['kd_ujian'];

$setId = mysqli_query($conn,"SELECT max(id) FROM ta_harga_paket WHERE kd_ujian = $kd_ujian");
        $no = mysqli_fetch_array($setId);
        if ($no) {
            $nomor = $no[0];
            $urut = (int) $nomor;
            $kode = $urut + 1;
        }else{
            $kode = '1';
        }
?>
<div class='content-wrapper'>
    <section class='content-header'>
        <h1>
            Daftar
            <small>Harga Paket TO</small>
        </h1>
        <ol class='breadcrumb'>
            <li>
                <a href='#'>
                    <i class='fa fa-dashboard'></i>
                    Dashboard</a>
            </li>
            <li class='active'>Paket TO</li>
        </ol>
    </section>

    <section class='content'>
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class='box-title'>
                            Tambah Paket
                        </h3>
                    </div>
                    <div class="box-body">
                        <form action="" method="POST">
                            <div class="form-group">
                                <label for="cmb" class="col-sm-2 control-label">Jenis Paket</label>
                                <div class="col-sm-10">
                                    <input type="hidden" class="form-control" name="kode" value="<?=$kode?>">
                                    <input type="hidden" class="form-control" name="kd_ujian" value="<?=$kd_ujian?>">
                                    <select class="form-control" name="kd_paket">
                                        <option selected="selected" disabled="disabled">-- Pilih Jenis Paket --</option>
                                        <?php
                                            $query = mysqli_query($conn,"SELECT a.*
                                                                    FROM ref_paket a
                                                                    LEFT JOIN ta_harga_paket b
                                                                    ON a.kd_ujian = b.kd_ujian
                                                                    AND a.kd_paket = b.kd_paket
                                                                    WHERE a.kd_ujian = $kd_ujian
                                                                    AND b.kd_paket IS NULL");
                                            // $query = mysqli_query($conn,"SELECT * FROM ref_paket");
                                                                    // $paket=mysqli_fetch_assoc($query);
                                                                    // var_dump($paket);exit;
                                            while ($paket=mysqli_fetch_assoc($query)){
                                                echo "<option value='$paket[kd_paket]'>$paket[keterangan]</option>";
                                            }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="harga" class="col-sm-2 control-label">Harga Paket</label>
                                <div class="col-sm-10">
                                    <input type="number" class="form-control" id="harga" name="harga" placeholder="Harga Paket">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="diskon" class="col-sm-2 control-label">Diskon dalam persen(%)</label>
                                <div class="col-sm-10">
                                    <input type="number" class="form-control" id="diskon" name="diskon" placeholder="Diskon dalam persen(%)">
                                </div>
                            </div>
                        </div>
                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary" name="create">
                                <i class="fa fa-check"></i>
                                Simpan</button>
                            <a class="btn btn-warning" href="?module=hargapaket">
                                <i class="fa fa-mail-reply"></i>
                                Batal</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
</div>