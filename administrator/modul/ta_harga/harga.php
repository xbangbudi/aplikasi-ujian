<?php
include_once "../config/koneksi.php";
$kd_ujian = $_GET['kd_ujian'];
$namaUjian = mysqli_query($conn, "SELECT a.keterangan
                                FROM ref_jenis_ujian a
                                LEFT JOIN ta_harga_paket b
                                ON a.kd_ujian = b.kd_ujian
                                WHERE a.kd_ujian = $kd_ujian");
    $dataUjian=mysqli_fetch_assoc($namaUjian);
?>
<div class='content-wrapper'>
    <section class='content-header'>
        <h1>
            Daftar Harga Paket Tryout <?=ucwords(strtolower($dataUjian["keterangan"]))?>
        </h1>
        <ol class='breadcrumb'>
            <li>
                <a href='#'>
                    <i class='fa fa-dashboard'></i>
                    Dashboard</a>
            </li>
            <li class='active'>Paket TO</li>
        </ol>
    </section>
    
    <section class='content'>
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class='box-title'>
                            <a href='?module=tambahhargapaket&kd_ujian=<?= $kd_ujian ?>'>
                                <button type='button' class='btn btn-block btn-primary'>
                                    <i class='glyphicon glyphicon-pencil'></i>
                                    Tambah Paket</button>
                            </a>
                        </h3>
                    </div>
                    <div class="box-body">
                        <table id='example1' class='table table-bordered table-striped'>
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Nama Paket</th>
                                    <th>Harga</th>
                                    <th>Diskon dalam persen(%)</th>
                                    <th>Total</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                            $sql=mysqli_query($conn,"SELECT a.*, b.keterangan
                                                FROM ta_harga_paket a
                                                RIGHT JOIN ref_paket b
                                                ON a.kd_ujian = b.kd_ujian
                                                AND a.kd_paket = b.kd_paket
                                                WHERE a.kd_ujian = $kd_ujian ORDER BY id ASC");
                            if(mysqli_num_rows($sql)){
                                $no=1;
                                while ($r=mysqli_fetch_array($sql)) {
                                    $hitung = $r['jumlah']*$r['diskon']/100;
                                    $total =  $r['jumlah']-$hitung;
                                    ?>
                                <tr>
                                    <td><?= $no; ?></td>
                                    <td><?= $r['keterangan']; ?></td>
                                    <td>Rp. <?= number_format($r['jumlah'],0,",","."); ?></td>
                                    <td><?= $r['diskon'];?> %</td>
                                    <td>Rp. <?= number_format($total,0,",","."); ?></td>
                                    <td>
                                        <a href="?module=edithargapaket&id=<?= $r['id']; ?>&kd_ujian=<?= $r['kd_ujian'] ?>&kd_paket=<?= $r['kd_paket'];?>" class='btn btn-info'>
                                            <i class='fa fa-pencil'></i>
                                        </a>
                                        <a
                                            href="?module=hapushargapaket&id=<?= $r['id'];?>&kd_ujian=<?= $r['kd_ujian'] ?>&kd_paket=<?= $r['kd_paket'];?>"
                                            class='btn btn-danger'
                                            onclick="return confirm('Yakin ingin menghapus paket ini?');">
                                            <i class='fa fa-trash'></i>
                                        </a>
                                    </td>
                                </tr>
                            <?php
                            $no++;
                                }
                            }
                            else{
                                ?>
                                <tr>
                                    <td colspan="7" class="text-center">Belum Ada Paket Tersedia</td>
                                </tr>
                                <?php
                            }
                            ?>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>