<?php
include_once "../config/koneksi.php";
if(isset($_POST['create'])){
    $id = $_POST['kode'];
    $kd_ujian = $_POST['kd_ujian'];
    $kd_paket=$_POST['kd_paket'];
    $harga=$_POST['harga'];
    $diskon=$_POST['diskon'];
    $sql=mysqli_query($conn,"UPDATE ta_harga_paket SET jumlah='$harga', diskon='$diskon' WHERE id='$id' AND kd_ujian='$kd_ujian' AND kd_paket='$kd_paket' ");
    if($sql){
        echo "<script> alert('Paket Berhasil diubah'); document.location.href = '?module=list_harga&kd_ujian=$kd_ujian';</script>";
    }
    else{
        echo "<script> alert('Paket gagal ditambahkan. Terjadi kesalahan');";
    }
}
if(isset($_GET['id']) && isset($_GET['kd_paket']) && isset($_GET['kd_ujian'])){
    $id=$_GET['id'];
    $kd_ujian=$_GET['kd_ujian'];
    $kd_paket=$_GET['kd_paket'];
    $sql=mysqli_query($conn,"SELECT * FROM ta_harga_paket WHERE id='$id' AND kd_ujian='$kd_ujian' AND kd_paket='$kd_paket'");
    if(mysqli_num_rows($sql)){
        $r=mysqli_fetch_array($sql);
?>
<div class='content-wrapper'>
    <section class='content-header'>
        <h1>
            Daftar
            <small>Harga Paket TO</small>
        </h1>
        <ol class='breadcrumb'>
            <li>
                <a href='#'>
                    <i class='fa fa-dashboard'></i>
                    Dashboard</a>
            </li>
            <li class='active'>Paket TO</li>
        </ol>
    </section>

    <section class='content'>
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class='box-title'>
                            Ubah Paket
                        </h3>
                    </div>
                    <div class="box-body">
                    <form action="" method="POST">
                                    <input type="hidden" class="form-control" name="kode" value="<?=$r['id']?>">
                                    <input type="hidden" class="form-control" name="kd_ujian" value="<?=$r['kd_ujian']?>">
                                    <input type="hidden" class="form-control" name="kd_paket" value="<?=$r['kd_paket']?>">
                            <div class="form-group">
                                <label for="harga" class="col-sm-2 control-label">Harga Paket</label>
                                <div class="col-sm-10">
                                    <input type="number" class="form-control" id="harga" name="harga" value="<?=$r['jumlah']?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="diskon" class="col-sm-2 control-label">Diskon dalam persen(%)</label>
                                <div class="col-sm-10">
                                    <input type="number" class="form-control" id="diskon" name="diskon" value="<?=$r['diskon']?>">
                                </div>
                            </div>
                        </div>
                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary" name="create">
                                <i class="fa fa-check"></i>
                                Simpan</button>
                            <a class="btn btn-warning" href="?module=hargapaket">
                                <i class="fa fa-mail-reply"></i>
                                Batal</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
</div>
<?php
    }
}
else{
    echo "document.location.href = '?module=hargapaket';</script>"; 
}
?>