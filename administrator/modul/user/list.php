<?php
include_once "../config/koneksi.php";
?>
<div class='content-wrapper'>
    <section class='content-header'>
        <h1>
            Data
            <small>Request Paket</small>
        </h1>
        <ol class='breadcrumb'>
            <li>
                <a href='#'>
                    <i class='fa fa-dashboard'></i>
                    Dashboard</a>
            </li>
            <li class='active'>Request</li>
        </ol>
    </section>
    
    <section class='content'>
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class='box-title'>
                            Data Request Paket
                        </h3>
                    </div>
                    <div class="box-body">
                    <table class="table table-striped">
                                    <tr>
                                        <th>Username</th>
                                        <th>Nama</th>
                                        <th>E-mail</th>
                                        <th>No. Handphone</th>
                                        <th>Paket Tryout</th>
                                    </tr>
                                    <?php 
                                    $sql=mysqli_query($conn,"SELECT a.*, b.keterangan
                                                                FROM user a
                                                                LEFT JOIN ref_paket b
                                                                ON a.kd_paket = b.kd_paket
                                                                AND a.kd_ujian = b.kd_ujian 
                                                                ORDER BY id ASC");
                                        // print_r($data);exit;
                                    if(mysqli_num_rows($sql)){
                                        while ($data=mysqli_fetch_array($sql)) {
                                    ?>
                                    <tr>
                                        <td><?= $data['kode_reg'] ?></td>
                                        <td><?= $data['nama'] ?></td>
                                        <td><?= $data['email'] ?></td>
                                        <td><?= $data['no_hp'] ?></td>
                                        <td><?= $data['keterangan'] ?></td>
                                    </tr>
                                        <?php } ?>
                              </table>  
                                <?php  }?>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>