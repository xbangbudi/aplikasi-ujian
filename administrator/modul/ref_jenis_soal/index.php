<?php
include_once "../config/koneksi.php";
?>
<div class='content-wrapper'>
    <section class='content-header'>
        <h1>
            Daftar
            <small>Bank Soal</small>
        </h1>
        <ol class='breadcrumb'>
            <li>
                <a href='#'>
                    <i class='fa fa-dashboard'></i>
                    Dashboard</a>
            </li>
            <li class='active'>Bank Soal</li>
        </ol>
    </section>
    
    <section class='content'>
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class='box-title'>
                            <a href='?module=add_jenis_soal'>
                                <button type='button' class='btn btn-block btn-primary'>
                                    <i class='glyphicon glyphicon-pencil'></i> 
                                    Tambah Jenis Soal</button>
                            </a>
                        </h3>
                    </div>
                    <div class="box-body table-responsive">
                        <table id='example1' class='table table-bordered table-striped'>
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Keterangan Jenis Soal</th>
                                    <th>Soal</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                            $sql=mysqli_query($conn,"SELECT * FROM `ref_jenis_soal` ORDER BY kd_jenis ASC");
                            $cekIsi = mysqli_num_rows($sql);
                            if($cekIsi > 0){
                                $no=1;
                                while ($data=mysqli_fetch_array($sql)) {
                                    ?>
                                <tr>
                                    <td><?php echo $no; ?></td>
                                    <td><?php echo $data['keterangan']; ?></td>
                                    <td>
                                        <?php $jumlahSoal = mysqli_query($conn,"SELECT * FROM `ref_soal` WHERE kd_jenis =".$data['kd_jenis']);
                                        $totalSoal = mysqli_num_rows($jumlahSoal);?>
                                    <a href=#" class='btn btn-info disabled btn-block'> <?= $totalSoal ?> Soal
                                        </a>
                                    <a href="?module=ref_soal&kd_jenis=<?php echo $data['kd_jenis']; ?>" target = "_self" class='btn btn-success btn-block'>
                                            <i class='fa fa-plus'></i> Tambah Soal
                                        </a>
                                    </td>
                                    <td>
                                        <a href="?module=edit_jenis_soal&id=<?php echo $data['kd_jenis']; ?>" class='btn btn-info'>
                                            <i class='fa fa-pencil'></i>
                                        </a>
                                        <a
                                            href="?module=hapus_jenis_soal&id=<?php echo $data['kd_jenis'];?>"
                                            class='btn btn-danger'
                                            onclick="return confirm('Yakin ingin menghapus data ini?');">
                                            <i class='fa fa-trash'></i>
                                        </a>
                                    </td>
                                </tr>
                            <?php
                            $no++;
                                }
                            }
                            else{
                                ?>
                                <tr>
                                    <td colspan="7" class="text-center">Belum Ada Data Tersedia</td>
                                </tr>
                                <?php
                            }
                            ?>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
