<?php
include_once "../config/koneksi.php";
if (isset($_POST['save'])) {
    $kode=$_POST['kode'];
    $judul=$_POST['judul'];
    $sql=mysqli_query($conn,"INSERT INTO ref_jenis_soal(kd_jenis, keterangan) VALUES ('$kode','$judul') ");
    if($sql){
        echo "<script> alert('Paket Berhasil ditambahkan'); document.location.href = '?module=ref_jenis_soal';</script>";
    }
    else{
        // echo $sql;exit;
        echo "<script> alert('Terjadi Kesalahan');</script>";
    }
}
$setId = mysqli_query($conn,"SELECT max(kd_jenis) FROM ref_jenis_soal");
        $no = mysqli_fetch_array($setId);
        if ($no) {
            $nomor = $no[0];
            $urut = (int) $nomor;
            $kode = $urut + 1;
        }else{
            $kode = '1';
        }
?>
<div class='content-wrapper'>
    <section class='content-header'>
        <h1>
            Daftar
            <small>Data Jenis Soal</small>
        </h1>
        <ol class='breadcrumb'>
            <li>
                <a href='#'>
                    <i class='fa fa-dashboard'></i>
                    Dashboard</a>
            </li>
            <li class='active'>Data Jenis Soal</li>
        </ol>
    </section>

    <section class='content'>
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class='box-title'>
                            Tambah Data
                        </h3>
                    </div>
                    <div class="box-body">
                        <form class='stdform stdform2' method="POST" action='' enctype='multipart/form-data'>
                            <div class='input-group'>
                                <span class='input-group-addon'>
                                    <i class='fa fa-pencil'></i>
                                </span>
                                <input type='hidden' name='kode' value='<?=$kode?>' class='form-control'>
                                <input type='text' name='judul' class='form-control' placeholder='Jenis Soal' required="required">
                            </div>
                            <br>
                            <p class='stdformbutton'>
                                <button class='btn btn-primary' type="submit" name="save">Simpan</button>
								<input type=button value=Batal onclick=self.history.back() class='btn btn-warning btn-rounded'>  
                            </p>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>