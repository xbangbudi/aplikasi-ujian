<?php
include_once "../config/koneksi.php";
?>
<div class='content-wrapper'>
    <section class='content-header'>
        <h1>
            Daftar
            <small>Paket Soal</small>
        </h1>
        <ol class='breadcrumb'>
            <li>
                <a href='#'>
                    <i class='fa fa-dashboard'></i>
                    Dashboard</a>
            </li>
            <li class='active'>Paket Soal</li>
        </ol>
    </section>
    
    <section class='content'>
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class='box-title'>
                        <?php $cekData = mysqli_query($conn,"SELECT * FROM `ref_jenis_ujian`");
                                $cekJumlah = mysqli_num_rows($cekData);
                                if($cekJumlah == 2):
                                    echo '';
                                else: ?>
                            <a href='?module=add_jenis_ujian'>
                                <button type='button' class='btn btn-block btn-primary'>
                                    <i class='glyphicon glyphicon-pencil'></i> 
                                    Tambah Ujian</button>
                            </a>
                            <?php endif; ?>
                        </h3>
                    </div>
                    <div class="box-body table-responsive">
                        <table id='example1' class='table table-bordered table-striped'>
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Nama Ujian</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                            $sql=mysqli_query($conn,"SELECT * FROM `ref_jenis_ujian` ORDER BY kd_ujian ASC");
                            if(mysqli_num_rows($sql)){
                                $no=1;
                                while ($data=mysqli_fetch_array($sql)) {
                                    ?>
                                <tr>
                                    <td><?php echo $no; ?></td>
                                    <td><?php echo $data['keterangan']; ?></td>
                                    <td>
                                        <!-- <a href="export/soal.php?id=<?php echo $data['kd_ujian']; ?>" class='btn btn-info'>
                                            <i class='fa  fa-print'></i>
                                        </a> -->
                                        <a href="?module=edit_jenis_ujian&id=<?php echo $data['kd_ujian']; ?>" class='btn btn-info'>
                                            <i class='fa fa-pencil'></i>
                                        </a>
                                        <a
                                            href="?module=hapus_jenis_ujian&id=<?php echo $data['kd_ujian'];?>"
                                            class='btn btn-danger'
                                            onclick="return confirm('Yakin ingin menghapus data ini?');">
                                            <i class='fa fa-trash'></i>
                                        </a>
                                    </td>
                                </tr>
                            <?php
                            $no++;
                                }
                            }
                            else{
                                ?>
                                <tr>
                                    <td colspan="7" class="text-center">Belum Ada Data Tersedia</td>
                                </tr>
                                <?php
                            }
                            ?>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
