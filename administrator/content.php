 <?php
include "../config/koneksi.php";
include "../config/library.php";

if(isset($_GET['module'])){  
  $page=$_GET['module'];
   
  //Proses Switch
  switch ($page) {
    case 'home':
    include "modul/mod_umum/home.php";
    break;
    case 'trade':
      include "modul/akuntan/count.php";
      break;
    case 'paketsoal':
      include "modul/paket/paket.php";
      break;
    case 'tambahpaket':
      include "modul/paket/tambah.php";
      break;
    case 'editpaket':
      include "modul/paket/edit.php";
      break;
    case 'hapuspaket':
      include "modul/paket/hapus.php";
      break;
    case 'soal':
      include "modul/soal/soal.php";
      break;
    case 'tampilsoal':
      include "modul/soal/lihatsoal.php";
      break;
    case 'tambahsoal':
      include "modul/soal/tambah.php";
      break;
    case 'editsoal':
      include "modul/soal/edit.php";
      break;
    case 'hapussoal':
      include "modul/soal/hapus.php";
      break;
    case 'request':
      include "modul/request/list.php";
      break;
    case 'datauser':
      include "modul/user/list.php";
      break;
    case 'admin':
      include "modul/admin/data.php";
      break;
    case 'tambahadmin':
      include "modul/admin/addadmin.php";
      break;
    case 'resetadmin':
      include "modul/admin/setting.php";
      break;
    case 'deleteadmin':
      include "modul/admin/setting.php";
      break;
    case 'hargapaket':
      include "modul/ta_harga/index.php";
      break;
    case 'list_harga':
      include "modul/ta_harga/harga.php";
      break;
    case 'tambahhargapaket':
      include "modul/ta_harga/tambah.php";
      break;
    case 'edithargapaket':
      include "modul/ta_harga/edit.php";
      break;
    case 'hapushargapaket':
      include "modul/ta_harga/hapus.php";
      break;
    case 'ta_hasil':
      include "modul/ta_hasil/index.php";
      break;
    case 'list_hasil':
      include "modul/ta_hasil/hasil.php";
      break;
    case 'list_to':
      include "modul/ta_hasil/tryout.php";
      break;
    case 'tambahhargapaket':
      include "modul/ta_hasil/tambah.php";
      break;
    case 'edithargapaket':
      include "modul/ta_hasil/edit.php";
      break;
    case 'hapushargapaket':
      include "modul/ta_hasil/hapus.php";
      break;
    case 'view_to':
      include "modul/ta_hasil/view.php";
      break;
    case 'proses_hasil':
      include "modul/ta_hasil/add.php";
      break;
    case 'del_ta':
      include "modul/ta_hasil/delete.php";
      break;
    case 'confirm':
      include "modul/request/view.php";
      break;
    case 'checked':
      include "modul/request/konfirmasi.php";
      break;
    case 'config':
      include "modul/pengaturan/config.php";
      break;
    case 'addrekening':
      include "modul/pengaturan/tambahbank.php";
      break;
    case 'editrekening':
      include "modul/pengaturan/edit.php";
    case 'paketsoalserentak':
      include "modul/serentak/view.php";
      break;
      case 'tambahpaketserentak':
      include "modul/serentak/tambah.php";
      break;
    case 'editpaketserentak':
      include "modul/serentak/edit.php";
      break;
    case 'hapuspaketserentak':
      include "modul/serentak/hapus.php";
      break;
    case 'soalserentak':
      include "modul/soalserentak/soal.php";
      break;
    case 'tampilsoalserentak':
      include "modul/soalserentak/lihatsoal.php";
      break;
    case 'tambahsoalserentak':
      include "modul/soalserentak/tambah.php";
      break;
    case 'editsoalserentak':
      include "modul/soalserentak/edit.php";
      break;
    case 'hapussoalserentak':
      include "modul/soalserentak/hapus.php";
      break;
    case 'ref_paket':
      include "modul/ref_paket/index.php";
      break;
    case 'list_paket':
      include "modul/ref_paket/paket.php";
      break;
    case 'add_paket':
      include "modul/ref_paket/tambah.php";
      break;
    case 'edit_paket':
      include "modul/ref_paket/edit.php";
      break;
    case 'hapus_paket':
      include "modul/ref_paket/hapus.php";
      break;
    case 'ref_tryout':
      include "modul/ref_tryout/index.php";
      break;
    case 'add_tryout':
      include "modul/ref_tryout/tambah.php";
      break;
    case 'edit_tryout':
      include "modul/ref_tryout/edit.php";
      break;
    case 'hapus_tryout':
      include "modul/ref_tryout/hapus.php";
      break;
    case 'ref_jenis_ujian':
      include "modul/ref_jenis_ujian/index.php";
      break;
    case 'add_jenis_ujian':
      include "modul/ref_jenis_ujian/tambah.php";
      break;
    case 'edit_jenis_ujian':
      include "modul/ref_jenis_ujian/edit.php";
      break;
    case 'hapus_jenis_ujian':
      include "modul/ref_jenis_ujian/hapus.php";
      break;
    case 'ref_jenis_soal':
      include "modul/ref_jenis_soal/index.php";
      break;
    case 'add_jenis_soal':
      include "modul/ref_jenis_soal/tambah.php";
      break;
    case 'edit_jenis_soal':
      include "modul/ref_jenis_soal/edit.php";
      break;
    case 'hapus_jenis_soal':
      include "modul/ref_jenis_soal/hapus.php";
      break;
    case 'ref_soal':
      include "modul/ref_soal/index_soal.php";
      break;
    case 'add_soal':
      include "modul/ref_soal/tambah.php";
      break;
    case 'edit_soal':
      include "modul/ref_soal/edit.php";
      break;
    case 'hapus_soal':
      include "modul/ref_soal/hapus.php";
      break;
    case 'ta_tryout':
      include "modul/ta_tryout/index.php";
      break;
    case 'add_soalta':
      include "modul/ta_tryout/tambah.php";
      break;
    case 'edit_soalta':
      include "modul/ta_tryout/edit.php";
      break;
    case 'hapus_soalta':
      include "modul/ta_tryout/hapus.php";
      break;
    case 'pilih_jenis':
      include "modul/ta_tryout/jenis.php";
      break;
    case 'view_soalta':
      include "modul/ta_tryout/view.php";
      break;
    case 'proses_ta':
      include "modul/ta_tryout/proses.php";
      break;
    case 'verif':
      include "modul/verif/index.php";
      break;
    case 'detail_transaksi':
        include "modul/verif/detail.php";
        break;
    case 'verifikasi_user':
        include "modul/verif/terima.php";
        break;
    default:
      include "404.html";
      break;
  }
}
else{
  include "modul/mod_umum/home.php";
}


