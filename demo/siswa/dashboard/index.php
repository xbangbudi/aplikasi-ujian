<?php
    session_start();
    if($_SESSION['status'] != 'LOGIN'){
        header("location:../index.html");
    }
?>

<!DOCTYPE html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title></title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="../../kit/css/bootstrap.css">
    <link rel="stylesheet" href="../../kit/css/font-awesome.css">
    <link rel="stylesheet" href="../../kit/css/hover.css">
    <link href="css/dataTables.css">
    <link rel="stylesheet" href="style.css">

</head>
<body>
    <div id="sideNavigation" class="sidenav">
        <?php include "menu.php"; ?>
    </div>

    <nav class="topnav">
        <a href="#" onclick="openNav()">
            <svg width="30" height="30" id="icoOpen">
                <path d="M0,5 30,5" stroke="#000" stroke-width="5"/>
                <path d="M0,14 30,14" stroke="#000" stroke-width="5"/>
                <path d="M0,23 30,23" stroke="#000" stroke-width="5"/>
            </svg>
        </a>
    </nav>

    <div id="main">
    <?php 
    $halaman = $_GET['halaman'];

        if(!$halaman){
          header('location:index.php?halaman=1');
        }else if($halaman == '1'){
            include "home.php";
        }else if($halaman == '2'){
            include "riwayat.php";
            }else{
                include "tidak_ditemukan.php";
            }
        ?>

    </div>
    <script src="../../kit/js/bootstrap.js"></script>
    <script src="../../kit/js/jquery.js"></script>
    <script src="js/jquery.dataTables.js"></script>
    <script src="js/bootstrap.dataTables.js"></script>
    <script>
        $(document).ready(function () {
            $('#tabel1').DataTable();
        });
    </script>
</body>
</html>