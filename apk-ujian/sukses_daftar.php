<?php
include "../config/koneksi.php";
if(isset($_GET['id'])){
$id=$_GET['id'];
$rsql=mysqli_query($conn,"SELECT paket FROM request WHERE kode_reg='$id' ");
if(mysqli_num_rows($rsql)){
    $rd=mysqli_fetch_array($rsql);
    $ids=$rd['paket'];
}
else{
    $ids="";
}
$sql=mysqli_query($conn,"SELECT * FROM paket WHERE id='$ids' ");
    if(mysqli_num_rows($sql)){
        $r=mysqli_fetch_array($sql);
        $harga=$r['harga'];
        $diskon=$r['diskon'];
        $potong=$harga/100*$diskon;
        $total=$harga-$potong;
    }
    else {
        $total="00";
    }
?>
<!DOCTYPE html>
<!--[if gt IE 8]><!-->
<html class="no-js">
    <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Pendaftaran Suksess</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link
            rel="stylesheet"
            href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
        <link rel="stylesheet" href="kit/css/font-awesome.css">
        <link rel="stylesheet" href="kit/css/hover.css">
        <style>
            body {
                background-color: blueviolet;
            }

            .tabel,
            td,
            th,
            tr {
                border-collapse: collapse;
                border: 1px solid white;
                padding: 3px;
            }

            .kotak {
                border: 1px solid #f3f3f3;
                padding: 5px;
                border-radius: 3px;
                text-align: center;
            }

            hr {
                margin-top: 2px;
            }
        </style>

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script
            src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
        <script
            src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    </head>
    <body>
        <div
            class="jumbotron jumbotron-fluid"
            style="text-align: center; background-color: blueviolet; color: white;">
            <h1>Selamat!<br>
                <small>Pendaftaran Anda Berhasil</small>
            </h1>
        </div>
        <div class="d-flex justify-content-center">
            <div  style="text-align: center; color: white; margin-top: -50px;">
                <h4>Total yang harus dibayar</h4>
                <h3>
                    <b>Rp. <?php echo $total; ?>,-</b>
                </h3><br>
                <h4>Kode Registrasi</h4>
                <h3>
                    <b><?php echo $id; ?></b>
                </h3><br>
                <h4>Anda Dapat Mengirim Biaya Pendaftaran Melalui Rekening BANK berikut</h4>
                <button
                    type="button"
                    class="btn btn-primary"
                    data-toggle="modal"
                    data-target="#myModal">
                    Lihat Daftar Bank
                </button><br><br>
                <br>
                <a href="unggah_bukti.html" class="btn btn-primary">Unggah Bukti Pembayaran
                    <i class="fa fa-arrow-right"></i>
                </a>
            </div>
        </div>
        <div class="modal fade" id="myModal">
                <div class="modal-dialog modal-lg modal-dialog-centered">
                    <div class="modal-content">

                        <!-- Modal Header -->
                        <div class="modal-header">
                            <h4 class="modal-title">Daftar Rekening</h4>
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>

                        <!-- Modal body -->
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="kotak">
                                        Bank<hr>
                                        <table style="font-size: 11pt;">
                                        <?php
                            $sql=mysqli_query($conn,"SELECT * FROM `rekening` WHERE jenis='bank' ORDER BY id ASC");
                            if(mysqli_num_rows($sql)){
                                $no=1;
                                while ($r=mysqli_fetch_array($sql)) {
                                    $id=$r['id'];
                                    ?>
                                <tr>
                                    <td><?php echo $r['nama']; ?></td>
                                    <td><?php echo $r['no_rekening']; ?></td>
                                    <td><?php echo $r['atasnama']; ?></td>
                                </tr>
                            <?php
                            $no++;
                                }
                            }
                            ?>
                                        </table>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="kotak">
                                        OVO<hr>
                                        <?php
                                        $sql=mysqli_query($conn,"SELECT * FROM `rekening` WHERE jenis='ovo' ORDER BY id ASC");
                                        if(mysqli_num_rows($sql)){
                                            $r=mysqli_fetch_array($sql)
                                        ?>
                                        <img src="images/<?php echo $r['gambar']; ?>" width="60%"><br>
                                        No. HP : <?php echo $r['gambar']; ?>
                                        <?php
                                        }
                                        ?>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="kotak">
                                        GO-PAY<hr>
                                        <?php
                                        $sql=mysqli_query($conn,"SELECT * FROM `rekening` WHERE jenis='gopay' ORDER BY id ASC");
                                        if(mysqli_num_rows($sql)){
                                            $r=mysqli_fetch_array($sql)
                                        ?>
                                        <img src="images/<?php echo $r['gambar']; ?>" width="60%"><br>
                                        No. HP : <?php echo $r['gambar']; ?>
                                        <?php
                                        }
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- Modal footer -->
                        <div class="modal-footer">
                            <button type="button" class="btn btn-danger" data-dismiss="modal">Tutup</button>
                        </div>

                    </div>
                </div>
            </div>

        <script src="" async="async" defer="defer"></script>
    </body>
</html>
<?php
}
?>