<?php
include "../config/koneksi.php";
if(isset($_POST['kirim'])){
    $kode=$_POST['kode'];
    $gambar=$_FILES['foto-bukti']['name']; //nama gambarnya
    $type=$_FILES['foto-bukti']['type']; //type filenya
    $semen=$_FILES['foto-bukti']['tmp_name']; //nama gambar pada temporary files
    $dir="images/"; //direktorinya
    $sql=mysqli_query($conn,"SELECT foto from request WHERE kode_reg='$kode'");
    $cek=mysqli_num_rows($sql);
    $rd=mysqli_fetch_array($sql);
    $foto=$rd['foto'];
    if($foto==""){
    if($type=='image/jpeg' || $type=='image/png'){
        // memindahakan gamabrnya ke dalam folder images
        $upload=move_uploaded_file($semen, $dir.$gambar);

        // memeriksa apakah berhasil terupload
        if($upload){

            // menyimpan ke database
            $result=mysqli_query($conn,"UPDATE request SET tgl_bayar=CURRENT_TIMESTAMP, foto='$gambar' WHERE kode_reg='$kode' ");

            // memeriksa apakah tersimpan ke database
            if($result){
                // mengembalikan ke halaman data
                echo "<script> alert('Bukti pembayaran berasil di unggah. Silahkan tunggu akun anda melalui email'); document.location.href = 'unggah_bukti.html';</script>";
            }
            else{

                unlink($dir.$gambar); // menghapus gambar yg terupload ke folder images supaya tidak terjadi tabrakan nama file saat upload selanjutnya

                echo "<script> alert('Unggah gambar gagal'); document.location.href = 'unggah_bukti.html';</script>";
            }
        }
    }
}
else {
    echo "<script> alert('Unggah gambar gagal! Anda sudah pernah mengupload bukti pembayaran, silahkan cek email anda'); document.location.href = 'unggah_bukti.html';</script>";
}
}

?>