<?php
include "../../config/koneksi.php";
    ?>
<div class="row">
    <div class="col-md-10">
        <div class="card" style="margin-left:5px;margin-right:5px">
            <div class="card-body">
                <?php 
            $username = $_SESSION['kode_reg']; 
            $query = "SELECT id, status_pembayaran
            FROM user
            WHERE kode_reg='$username'";
            $cekQuery = mysqli_query($conn, $query)or die(mysqli_error($conn));
            $cekStatus = mysqli_fetch_assoc($cekQuery);
            if($cekStatus["status_pembayaran"] == 0){ 
                $_SESSION['status_bayar']="false";
                ?>
                <div class="alert alert-danger" role="alert">
                    Segera selesaikan pembayaran anda,
                    <a href="pembayaran.php?id=<?=$cekStatus['id']?>" class="alert-link">klik disini</a>
                    untuk melakukan pembayaran.
                </div>
                <?php }
                else if($cekStatus["status_pembayaran"] == 1){
                    $_SESSION['status_bayar']="wait";
                }
                else{
                    $_SESSION['status_bayar']="true";
                }
                 ?>
                <div class="card-title">
                    <h5>List Tryout</h5>
                </div>

                <?php 
                $query = "SELECT a.*, b.keterangan, c.*
                            FROM user a
                            LEFT JOIN ref_paket b
                            ON a.kd_paket = b.kd_paket
                            AND a.kd_ujian = b.kd_ujian
                            LEFT JOIN ta_hasil c
                            ON a.kd_paket = c.kd_paket
                            AND a.kd_ujian = c.kd_ujian
                            WHERE a.kode_reg='$username'";
                            $cek = mysqli_query($conn, $query)or die(mysqli_error($conn));
                            ?>
                <div class="row">
                    <?php
                    $no=1;
                                        while ($rd=mysqli_fetch_array($cek)) {
                                
                                            ?>
                    <div class="col-xs-6">
                        <a href="../../tryout/?to=<?= $rd['kd_tryout'] ?>" rel="noopener noreferrer">
                        <div class="card">
                            <div class="card-body">
                                <div class="card-title"><?= "Tryout ".$no?></div>
                                Waktu Ujian : <?php $aql=mysqli_query($conn,"SELECT jam FROM ref_tryout WHERE kd_tryout=".$rd['kd_tryout']); $ard= mysqli_fetch_assoc($aql); echo $ard['jam']; ?><br>
                            </div>
                        </div>
                    </div></a>&nbsp;
                    <?php
                    $no++;
                }
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>