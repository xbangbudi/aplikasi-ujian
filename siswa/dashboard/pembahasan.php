<div class="container-fluid">
<?php 
    $username = $_SESSION['kode_reg']; 
    $query = "SELECT id, status_pembayaran
                FROM user
                WHERE kode_reg='$username'";
    $cekQuery = mysqli_query($conn, $query)or die(mysqli_error($conn));
    $cekStatus = mysqli_fetch_assoc($cekQuery);
    if($cekStatus["status_pembayaran"] != 2){ 
        if($cekStatus["status_pembayaran"] == 0){
        ?>
        <div class="alert alert-danger" role="alert">
            Segera selesaikan pembayaran anda, <a href="?page=pilih_bank&id=<?=$cekStatus['id']?>" class="alert-link">klik disini</a> untuk melakukan pembayaran.
        </div>
        <?php 
        }
        else if($cekStatus["status_pembayaran"] == 1){
        ?>
        <div class="alert alert-warning" role="alert">
            Sedang menunggu konfirmasi dari admin
        </div>
        <?php
        }
    }else{
        $kd_to=$_GET['id'];
        $kd_uji=$dataAtas['kd_ujian'];
        $cek=mysqli_num_rows(mysqli_query($conn,"SELECT * from ta_riwayat where kd_ujian='$kd_uji' and id_user='".$cekStatus['id']."' and kd_tryout='$kd_to' and status='1'"));
        if($cek){
    ?>
    <div class="row">
        <div class="col-md-12">
            <div class="card shadow">
                <div class="card-header">
                    Pembahasan soal tryout <?=$_GET['id']?>
                </div>
                <div class="card-body">
                <?php
                
                $rn=3;
                $j = array('a','b','c','d','e');
                $num=1;
                 for ($i=1; $i <= $rn  ; $i++) { 
                    $sqlrd=mysqli_query($conn,"SELECT a.*, b.*, c.keterangan FROM ta_tryout a LEFT JOIN ref_soal b ON a.kd_soal=b.kd_soal AND a.kd_jenis=b.kd_jenis LEFT JOIN ref_jenis_soal c ON b.kd_jenis=c.kd_jenis  WHERE kd_tryout='$kd_to' and kd_ujian='$kd_uji' and a.kd_jenis='$i'");
                    $rsd=mysqli_fetch_assoc($sqlrd);
                    ?>
                    <h5><b><?=$rsd['keterangan']?></b></h6>
                    <?php
                     if(mysqli_num_rows($sqlrd)){
                         
                        while ($rd=mysqli_fetch_array($sqlrd)) {
                            ?>
                    
                    <p><?=$num.". ".$rd['soal']?></p>
                    <?php
                    for ($bi=0; $bi < 5; $bi++) {
                        $b=$j[$bi];
                        $a="n".$b;

                        if($i!= 3 ){
                            if($rd[$a]=="5"){
                                echo "<b>$b. $rd[$b]</b><br>";
                            }
                            else{
                                echo "$b. $rd[$b]<br>";
                            }
                        }
                        else{
                            echo "$b. $rd[$b]  -- (poin=$rd[$a])<br>";
                        }
                    }
                    $num++;
                    echo "<br><br>";
                        }
                    }
                }
                    ?>
                    <br>
                    <br>
                </div>
            </div>
        </div>
    </div>
    <?php
        }
        else{
            ?>
            <div class="alert alert-primary" role="alert">
                Selesaikan tryout anda terlebih dahulu
            </div>
            <?php
        }
}
?>
</div>