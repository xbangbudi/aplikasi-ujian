<?php include_once "header.php"; ?>
<div class="row">
    <div class="col-12">
        <nav class="topnav">
            <a href="#" onclick="openNav()">
                <svg width="30" height="30" id="icoOpen">
                    <path d="M0,5 30,5" stroke="#000" stroke-width="5"/>
                    <path d="M0,14 30,14" stroke="#000" stroke-width="5"/>
                    <path d="M0,23 30,23" stroke="#000" stroke-width="5"/>
                </svg>
            </a>
        </nav>
        <div class="row">
            <div class="col-3">
                <?php include "sidenav.php" ?>
            </div>
            <div class="col-9">
                <div id="main">
                    <?php
                    if(isset($_GET['page'])){
                        $page=$_GET['page'];

                        switch ($page) {
                            case 'dashboard':
                                include "profil.php";
                                break;
                            case 'paket':
                                include "page/paket/paket.php";
                                break;

                            default:
                                echo "404";
                                break;
                        }
                    }
                    else{
                     include "profil.php";
                    } 
                     ?>
                </div>
            </div>
        </div>
    </div>
</div>
<?php include_once "footer.php"; ?>
