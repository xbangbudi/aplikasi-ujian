<div class="row">
    <div class="col-md-12">
        <div class="card" style="margin-left:5px;margin-right:5px">
            <div class="card-body">
            <?php 
                $username = $_SESSION['kode_reg']; 
                $query = "SELECT id, status_pembayaran
                            FROM user
                            WHERE kode_reg='$username'";
                $cekQuery = mysqli_query($conn, $query)or die(mysqli_error($conn));
                $cekStatus = mysqli_fetch_assoc($cekQuery);
                if($cekStatus["status_pembayaran"] == 0){ ?>
                    <div class="alert alert-danger" role="alert">
                        Segera selesaikan pembayaran anda, <a href="?page=pilih_bank&id=<?=$cekStatus['id']?>" class="alert-link">klik disini</a> untuk melakukan pembayaran.
                    </div>
                <?php } ?>
                <div class="card-title">
                    <h5>Selamat Datang di Simulasi Ujian CPNS</h5>
                </div>
                <hr>
                
                    
                <?php 
                    $query = "SELECT a.*, b.keterangan
                            FROM user a
                            LEFT JOIN ref_paket b
                            ON a.kd_paket = b.kd_paket
                            AND a.kd_ujian = b.kd_ujian
                            WHERE a.kode_reg='$username'";
                    $cek = mysqli_query($conn, $query)or die(mysqli_error($conn));
                    while($data = mysqli_fetch_array($cek)){
                ?>
                <table width="100%" cellpadding="10" cellspacing="0" border="0">
                    <tr>
                        <td width="20%">Username</td>
                        <td width="5%">:</td>
                        <td><?php echo $data['kode_reg']; ?></td>
                    </tr>
                    <tr>
                        <td>Nama Lengkap</td>
                        <td>:</td>
                        <td><?php echo $data['nama']; ?></td>
                    </tr>
                    <tr>
                        <td>No. Handphone</td>
                        <td>:</td>
                        <td><?php echo $data['no_hp']; ?></td>
                    </tr>
                    <tr>
                        <td>Paket Tryout</td>
                        <td>:</td>
                        <td><?php echo $data['keterangan']; ?></td>
                    </tr>
                    <tr>
                        <td>Status Pembayaran</td>
                        <td>:</td>
                        <td><?php 
                            if($data['status_pembayaran'] == 0){ ?>
                                <div class="badge badge-danger">BELUM MELAKUKAN PEMBAYARAN</div> 
                            <?php }elseif($data['status_pembayaran'] == 1){ ?>
                                <div class="badge badge-warning">SEDANG DALAM KONFIRMASI</div> 
                            <?php }else{ ?>
                                <div class="badge badge-success">LUNAS</div> 
                            <?php  }?>
                        </td>
                    </tr>
                    <?php } ?>
                </table>
            </div>
        </div>
    </div>
</div>