$( document ).ready(function() {
    // var btnKumpul=$('#kumpul');
    // btnKumpul.hide();
    // console.log( "ready!" );
    (function($) {
            
            var akdto=$('.kdto').val();
            var ajenis=$('.ambil:first').attr('jenis');
            var asoal=$('.ambil:first').attr('soal');
            var noSoal=$('.ambil:first').attr('no');
            $('.ambil').addClass('btn-primary');
        // console.log(noSoal)
        $.ajax({
            url : 'soal.php',
            type : 'POST',
            data : 'kd_tryout='+akdto+'&jenis='+ajenis+'&kd_soal='+asoal+'&no='+noSoal,
            success:function(hasils){
                $('#soal').html(hasils);
                console.log($('#soal').html(hasils));
                $('.ambil:first').removeClass('btn-primary');
                $('.ambil:first').addClass('btn-warning');
            },
            error: function(){
                alert("Terjadi kesalahan!");
            }
        });
    })(jQuery);
    
    $('.ambil').click(function(){
        var ss=$('#timer').text();
        var kdto=$('.kdto').val();
        var jenis=$(this).attr('jenis');
        var soal=$(this).attr('soal');
        var noSoal=$(this).attr('no');
        // console.log(noSoal)
        
        $('.ambil').removeClass('btn-primary');
        $('.ambil').removeClass('btn-warning');
        $('.ambil').addClass('btn-primary');
        $(this).addClass('btn-warning');
        $.ajax({
            url : 'soal.php',
            type : 'POST',
            data : 'kd_tryout='+kdto+'&jenis='+jenis+'&kd_soal='+soal+'&time='+ss+'&no='+noSoal,
            success:function(hasils){
                $('#soal').html(hasils);
                console.log($('#soal').html(hasils))
            },
            error: function(){
                alert("Terjadi kesalahan!");
            }
        });
    })

$(document).on('click','#jawab',function(){
    var z=$("input:radio[name='emotion']:checked").val();
    var a=$('.id_soal').val();
    var b=$('.kd_to').val();
    var c=$('.kd_soal').val();
    var d=$('.kd_jenis').val();
    var t=$('#timer').text();
    if(z!=null){
        $.ajax({
            url : 'jawab1.php',
            type : 'POST',
            data : 'jb='+z+'&id_soal='+a+'&kd_to='+b+'&kd_soal='+c+'&kd_jenis='+d+'&timer='+t,
            success:function(hasils){
                $('a[soal="'+c+'"][jenis="'+d+'"]').removeClass('btn-primary');
                $('a[soal="'+c+'"][jenis="'+d+'"]').addClass('btn-success');
                console.log(hasils);
            },
            error: function(){
                alert("Terjadi kesalahan!");
            }
        });
    }
    
});
$(document).on('click','#kumpul',function(){
    var s=$('#timer').text();
    var x=$('.kd_to').val();
    $.ajax({
        url : 'jawab.php',
        type : 'POST',
        data : 'time='+s+'&kd_tryout='+x,
        success:function(hasils){
            $('#hasilx').html(hasils);
            console.log('berhasil');
        },
        error: function(){
            alert("Terjadi kesalahan!");
        }
    });
});
});
