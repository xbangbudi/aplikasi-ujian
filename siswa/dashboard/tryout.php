<?php
if(isset($_GET['id'])&&$_GET['page']=="tryout"){
?>
<div class="row">
    <div class="col-md-12">
        <div class="card" style="margin-left:5px;margin-right:5px">
            <div class="card-body">
            <?php 
                $username = $_SESSION['kode_reg']; 
                $query = "SELECT id, status_pembayaran
                            FROM user
                            WHERE kode_reg='$username'";
                $cekQuery = mysqli_query($conn, $query)or die(mysqli_error($conn));
                $cekStatus = mysqli_fetch_assoc($cekQuery);
                if($cekStatus["status_pembayaran"] == 0){ ?>
                    <div class="alert alert-danger" role="alert">
                        Segera selesaikan pembayaran anda, <a href="?page=pilih_bank&id=<?=$cekStatus['id']?>" class="alert-link">klik disini</a> untuk melakukan pembayaran.
                    </div>
                <?php } ?>
                <div class="card-title">
                    <h5>keterangan Tryout</h5>
                </div>
                <hr>
                
                    
                <?php 
                    $query = "  SELECT a.*, c.*, b.*, d.*, count(d.kd_tryout) as jumlah 
                                FROM user a
                                LEFT JOIN ta_hasil c
                                ON a.kd_paket = c.kd_paket
                                AND a.kd_ujian = c.kd_ujian
                                LEFT JOIN ref_tryout b
                                ON c.kd_tryout=b.kd_tryout
                                LEFT JOIN ta_tryout d
                                ON a.kd_ujian = d.kd_ujian
                                AND c.kd_tryout = d.kd_tryout
                                WHERE a.kode_reg='$username' AND c.kd_tryout='$_GET[id]' ";
                    $cek = mysqli_query($conn, $query)or die(mysqli_error($conn));
                    $data = mysqli_fetch_array($cek)
                ?>
                <table width="100%" cellpadding="10" cellspacing="0" border="0">
                    <tr>
                        <td width="20%">Judul Tryout</td>
                        <td width="5%">:</td>
                        <td><?=$data['keterangan']?></td>
                    </tr>
                    <tr>
                        <td>Waktu Ujian</td>
                        <td>:</td>
                        <td><?=$data['jam']?></td>
                    </tr>
                    <tr>
                        <td>Jumlah Soal</td>
                        <td>:</td>
                        <td><?=$data['jumlah']?></td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</div>
<?php
}
?>