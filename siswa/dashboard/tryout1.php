<!-- Begin Page Content -->
<div class="container-fluid">
<?php 
    $username = $_SESSION['kode_reg']; 
    $query = "SELECT id, status_pembayaran
                FROM user
                WHERE kode_reg='$username'";
    $cekQuery = mysqli_query($conn, $query)or die(mysqli_error($conn));
    $cekStatus = mysqli_fetch_assoc($cekQuery);
    if($cekStatus["status_pembayaran"] != 2){ 
        if($cekStatus["status_pembayaran"] == 0){
        ?>
        <div class="alert alert-danger" role="alert">
            Segera selesaikan pembayaran anda, <a href="?page=pilih_bank&id=<?=$cekStatus['id']?>" class="alert-link">klik disini</a> untuk melakukan pembayaran.
        </div>
        <?php 
        }
        else if($cekStatus["status_pembayaran"] == 1){
        ?>
        <div class="alert alert-warning" role="alert">
            Sedang menunggu konfirmasi dari admin
        </div>
        <?php
        }
    }else{ 

    unset($_SESSION['mulai_waktu']);
    require_once "terjawab.php";
    $cek= new cekin();
    require_once "../../config/koneksi.php";
?>


    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Try Out</h1>
    </div>

    <!-- Content Row -->
    <div class="row">

        <div class="col-lg-8 mb-4">

            <!-- Illustrations -->
            <div id="soal" class="card shadow mb-4">
                
            </div>

        </div>

        <div class="col-lg-4 mb-4">

            <!-- Illustrations -->
            <div class="card shadow mb-4">
                <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold text-primary">No soal</h6>
                    <b>Sisa Waktu :
                            <span id="timer"></span></b>
                </div>
                <div class="card-body">
                    <div class="text-center">
                    </div>

                    <div class="table-responsive">
                        <table class="table table" id="dataTable" width="100%" cellspacing="0" cellpadding="0">
                            <tbody>
                            <?php
                            if(isset($_SESSION["mulai_waktu"])){
                                $telah_berlalu = time() - $_SESSION["mulai_waktu"];
                                if($telah_berlalu==0){
                                    $_SESSION['st']=="habis";
                                }
                                }
                               else {
                                $_SESSION["mulai_waktu"] = time();
                                $_SESSION['st']= "mulai";
                                $telah_berlalu = 0;
                            }
                            $id_user=$_SESSION['id'];
                            $kd_uji=$_SESSION['kd_ujian'];
                            $kd_to=$_GET['id'];
                            $sql_riwayat=mysqli_query($conn,"SELECT * FROM ta_riwayat where id_user='$id_user' and kd_ujian='$kd_uji' and kd_tryout='$kd_to'");
                            if(mysqli_num_rows($sql_riwayat)){
                            $rds=mysqli_fetch_assoc($sql_riwayat);
                            $wto=$rds['sisa_waktu'];
                            $tm=explode(":",$wto);
                            $jam=$tm[0];
                                    $mnt=$tm[1];
                                    $dt=$tm[2];
                                    $tjam=$jam * 3600;
                                    $tmnt=$mnt * 60;
                                    $twaktu=$tjam+$tmnt+$dt;
                        }
                            else{
                                $time=mysqli_query($conn,"SELECT jam FROM ref_tryout WHERE kd_tryout='$kd_to'");
                                $rd=mysqli_fetch_assoc($time);
                                $jams=$rd['jam'];
                                $tm=explode(":",$jams);
                                $jam=$tm[0];
                                        $mnt=$tm[1];
                                        $dt=$tm[2];
                                        $tjam=$jam * 3600;
                                        $tmnt=$mnt * 60;
                                        $twaktu=$tjam+$tmnt+$dt;
                                $riwayat_buat=mysqli_query($conn,"INSERT INTO ta_riwayat(kd_ujian, id_user, kd_tryout, sisa_waktu)VALUES ('$kd_uji','$id_user','$kd_to', '$jams') ") ;
                            }
                            $jumlah_soal = $conn->query("SELECT a.*, b.* FROM ta_tryout a LEFT JOIN ref_soal b ON a.kd_soal=b.kd_soal AND a.kd_jenis=b.kd_jenis  WHERE kd_tryout=".$_GET['id']." and kd_ujian=".$dataAtas['kd_ujian']) or die(mysqli_error($conn));
                                $jumlah=$jumlah_soal->num_rows;
                                if($jumlah > 0){
                                    $num=1;
                                    while($hasil=$jumlah_soal->fetch_assoc()){
                                        $kd_jenis[$num]=$hasil['kd_jenis'];
                                        $kd_soal[$num]=$hasil['kd_soal'];
                                        $num++;
                                    }
                                }
                            if($jumlah>=10){
                                $z=10;
                            }
                            else{
                                $z=$jumlah;
                            }
                            ?>
                            <input type="hidden" class="kdto" value="<?=$_GET['id']?>">
                            <?php
                            for($i=1;$i<=$z;$i++){
                            ?>
                            
                                <tr>
                                    <td>
                                    <?php
                                    if($i<=$jumlah){
                                    ?>
                                        <a id="button" no="<?=$i?>" href="#" jenis="<?=$kd_jenis[$i]?>" soal="<?=$kd_soal[$i]?>" class="btn <?php echo $cek->cekterjawab($_GET['id'],$kd_soal[$i],$kd_jenis[$i]); ?> btn-circle btn-sm ambil">
                              <?=$i?>
                            </a>
                            <?php
                                }
                            ?>
                                    </td>
                                    <td>
                                    <?php
                                    if($i+10<=$jumlah){
                                    ?>
                                        <a href="#" no="<?=$i+10?>" jenis="<?=$kd_jenis[$i+10]?>" soal="<?=$kd_soal[$i+10]?>" class="btn <?php echo $cek->cekterjawab($_GET['id'],$kd_soal[$i+10],$kd_jenis[$i+10]); ?> btn-circle btn-sm ambil">
                              <?=$i+10?>
                            </a>
                            <?php
                            }
                            ?>
                                    </td>
                                    <td>
                                    <?php
                                    if($i+20<=$jumlah){
                                    ?>
                                        <a id="button" no="<?=$i+20?>" href="#" jenis="<?=$kd_jenis[$i+20]?>" soal="<?=$kd_soal[$i+20]?>"   class="btn <?php echo $cek->cekterjawab($_GET['id'],$kd_soal[$i+20],$kd_jenis[$i+20]); ?> btn-circle btn-sm ambil">
                              <?=$i+20?>
                            </a>
                            <?php }
                            ?>
                                    </td>
                                    <td>
                                    <?php
                                    if($i+30<=$jumlah){
                                    ?>
                                        <a id="button" no="<?=$i+30?>" href="#" jenis="<?=$kd_jenis[$i+30]?>" soal="<?=$kd_soal[$i+30]?>"   class="btn <?php echo $cek->cekterjawab($_GET['id'],$kd_soal[$i+30],$kd_jenis[+30]); ?> btn-circle btn-sm ambil">
                              <?=$i+30?>
                            </a>
                            <?php }
                            ?>
                                    </td>
                                    <td>
                                    <?php
                                    if($i+40<=$jumlah){
                                    ?>
                                        <a id="button" no="<?=$i+40?>" href="#" jenis="<?=$kd_jenis[$i+40]?>" soal="<?=$kd_soal[$i+40]?>" class="btn <?php echo $cek->cekterjawab($_GET['id'],$kd_soal[$i+40],$kd_jenis[$i+40]); ?> btn-circle btn-sm ambil">
                              <?=$i+40?>
                            </a>
                            <?php }
                            ?>
                                    </td>
                                    <td>
                                    <?php
                                    if($i+50<=$jumlah){
                                    ?>
                                        <a id="button" no="<?=$i+50?>" href="#" jenis="<?=$kd_jenis[$i+50]?>" soal="<?=$kd_soal[$i+50]?>"   class="btn <?php echo $cek->cekterjawab($_GET['id'],$kd_soal[$i+50],$kd_jenis[$i+50]); ?> btn-circle btn-sm ambil">
                              <?=$i+50?>
                            </a>
                            <?php }
                            ?>
                                    </td>
                                    <td>
                                    <?php
                                    if($i+60<=$jumlah){
                                    ?>
                                        <a id="button" no="<?=$i+60?>" href="#" jenis="<?=$kd_jenis[$i+60]?>" soal="<?=$kd_soal[$i+60]?>"   class="btn <?php echo $cek->cekterjawab($_GET['id'],$kd_soal[$i+60],$kd_jenis[$i+60]); ?> btn-circle btn-sm ambil">
                              <?=$i+60?>
                            </a>
                            <?php }
                            ?>
                                    </td>
                                    <td>
                                    <?php
                                    if($i+70<=$jumlah){
                                    ?>
                                        <a id="button" no="<?=$i+70?>" href="#" jenis="<?=$kd_jenis[$i+70]?>" soal="<?=$kd_soal[$i+70]?>"   class="btn <?php echo $cek->cekterjawab($_GET['id'],$kd_soal[$i+70],$kd_jenis[$i+70]); ?> btn-circle btn-sm ambil">
                              <?=$i+70?>
                            </a>
                            <?php }
                            ?>
                                    </td>
                                    <td>
                                    <?php
                                    if($i+80<=$jumlah){
                                    ?>
                                        <a id="button" no="<?=$i+80?>" href="#" jenis="<?=$kd_jenis[$i+80]?>" soal="<?=$kd_soal[$i+80]?>"   class="btn <?php echo $cek->cekterjawab($_GET['id'],$kd_soal[$i+80],$kd_jenis[$i+80]); ?> btn-circle btn-sm ambil">
                              <?=$i+80?>
                            </a>
                            <?php }
                            ?>
                                    </td>
                                    <td>
                                    <?php
                                    if($i+90<=$jumlah){
                                    ?>
                                        <a id="button" no="<?=$i+90?>" href="#" jenis="<?=$kd_jenis[$i+90]?>" soal="<?=$kd_soal[$i+90]?>"   class="btn <?php echo $cek->cekterjawab($_GET['id'],$kd_soal[$i+90],$kd_jenis[$i+90]); ?> btn-circle btn-sm ambil">
                              <?=$i+90?>
                            </a>
                            <?php }
                            ?>
                                    </td>
                                </tr>
                            <?php
                            }
                            ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

        </div>
    </div>

</div>
<div class="modal fade" id="myModal">
        <div class="modal-dialog modal-lg modal-dialog-centered">
            <div class="modal-content">

                <!-- Modal Header -->
                <div class="modal-header">
                    <h4 class="modal-title">Skor Hasil</h4>
                    <a
                        href=""
                        onclick=" return document.location.href = '#'"
                        type="button"
                        class="close"
                        data-dismiss="modal">&times;</a>
                </div>

                <!-- Modal body -->
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm">
                            <div class="kotak">
                                <!-- <h3>jawaban</h3> -->
                                <div id="hasilx">
                                    Mohon ditunggu...
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                    <!-- Modal footer -->
                    <div class="modal-footer">
                        <a
                            href=""
                            onclick=" return document.location.href = '#'"
                            class="btn btn-danger"
                            data-dismiss="modal">Tutup</a>
                    </div>
            </div>

        </div>

                        <?php } ?>
</div>
<!-- /.container-fluid -->