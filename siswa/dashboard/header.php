<?php
    session_start();
    include "../../config/koneksi.php";
    if($_SESSION['status_login'] != 'success'){
        header("location:../index.php");
        // echo $_SESSION["status_pembayaran"];die();
      }

  $username = $_SESSION['kode_reg']; 
  $query = "SELECT a.*, b.keterangan
            FROM user a
            LEFT JOIN ref_paket b
            ON a.kd_paket = b.kd_paket
            AND a.kd_ujian = b.kd_ujian
            WHERE a.kode_reg='$username'";
  $cek = mysqli_query($conn, $query)or die(mysqli_error($conn));
  $dataAtas = mysqli_fetch_array($cek);
  ?>
<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Dashboard User</title>

  <!-- Custom fonts for this template-->
  <link href="assets/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- Custom styles for this template-->
  <link href="assets/css/sb-admin-2.min.css" rel="stylesheet">
  <?php
  if(isset($_GET['page']) && $_GET['page']=="tryout"){
    ?>
    <style type="text/css">
            .input-hidden {
                position: absolute;
                left: -9999px;
            }
            input[type=radio]:checked + label > img {
                border: 0 solid #fff;
                border-radius: 100%;
                box-shadow: 0 0 3px 3px blue;
            }

            input[type=radio] + label > img {
                border: 0 dashed #444;
                width: 150px;
                height: 150px;
                transition: 500ms all;
            }

            input[type=radio]:checked + label > img {
                transform: rotateZ(-10deg) rotateX(10deg);
            }
    </style>
    <?php
  }
  ?>
</head>

<body id="page-top">

<!-- Page Wrapper -->
<div id="wrapper">

<!-- Sidebar -->
<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

<!-- Sidebar - Brand -->
<a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.html">
  <div class="sidebar-brand-icon rotate-n-15">
    <i class="fas fa-laugh-wink"></i>
  </div>
  <div class="sidebar-brand-text mx-3">TryOut CPNS</div>
</a>

<!-- Divider -->
<hr class="sidebar-divider">

<!-- Nav Item - Charts -->
<li class="nav-item">
  <a class="nav-link" href="index.php">
    <i class="fas fa-fw fa-tachometer-alt"></i>
    <span>Dashboard</span></a>
</li>

<!-- Divider -->
<hr class="sidebar-divider">

<!-- Heading -->
<div class="sidebar-heading">
  Paket
</div>

<!-- Nav Item - Pages Collapse Menu -->
<li class="nav-item">
  <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
    <i class="fas fa-book"></i>
    <span>Paket TryOut</span>
  </a>
  <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
    <div class="bg-white py-2 collapse-inner rounded">
      <h6 class="collapse-header">Paket</h6>
      <?php
      $query = "SELECT a.*, c.*, b.*
                FROM user a
                LEFT JOIN ta_hasil c
                ON a.kd_paket = c.kd_paket
                AND a.kd_ujian = c.kd_ujian
                LEFT JOIN ref_tryout b
                ON c.kd_tryout=b.kd_tryout
                WHERE a.kode_reg='$username'";
      $cek = mysqli_query($conn, $query)or die(mysqli_error($conn));
        while($data = mysqli_fetch_array($cek)){
      ?>
      <a class="collapse-item" href="?page=tryout&id=<?=$data['kd_tryout']?>"><?=$data['keterangan'] ?></a>
      <!-- <a class="collapse-item" href="#">TryOut 2</a>
      <a class="collapse-item" href="#">TryOut 3</a>
      <a class="collapse-item" href="#">TryOut 4</a>
      <a class="collapse-item" href="#">TryOut 5</a> -->
      <?php
        }
      ?>
    </div>
  </div>
</li>

<!-- Divider -->
<hr class="sidebar-divider">

<!-- Heading -->
<div class="sidebar-heading">
  Pembahasan
</div>

<!-- Nav Item - Pages Collapse Menu -->
<li class="nav-item">
  <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseThree" aria-expanded="true" aria-controls="collapseTwo">
    <i class="fas fa-book"></i>
    <span>Pembahasan Paket</span>
  </a>
  <div id="collapseThree" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
    <div class="bg-white py-2 collapse-inner rounded">
      <h6 class="collapse-header">Paket</h6>
      <?php
      $query = "SELECT a.*, c.*, b.*
                FROM user a
                LEFT JOIN ta_hasil c
                ON a.kd_paket = c.kd_paket
                AND a.kd_ujian = c.kd_ujian
                LEFT JOIN ref_tryout b
                ON c.kd_tryout=b.kd_tryout
                WHERE a.kode_reg='$username'";
      $cek = mysqli_query($conn, $query)or die(mysqli_error($conn));
        while($data = mysqli_fetch_array($cek)){
      ?>
      <a class="collapse-item" href="?page=kunci&id=<?=$data['kd_tryout']?>"><?=$data['keterangan'] ?></a>
      <!-- <a class="collapse-item" href="#">TryOut 2</a>
      <a class="collapse-item" href="#">TryOut 3</a>
      <a class="collapse-item" href="#">TryOut 4</a>
      <a class="collapse-item" href="#">TryOut 5</a> -->
      <?php
        }
      ?>
    </div>
  </div>
</li>

<!-- Divider -->
<hr class="sidebar-divider">

<!-- Heading -->
<div class="sidebar-heading">
  Help
</div>

<!-- Nav Item - Charts -->
<li class="nav-item">
  <a class="nav-link" href="https://wa.me/6281269438538" target="_blank">
    <i class="fas fa-phone-alt"></i>
    <span>Hubungi Admin</span></a>
</li>

<hr class="sidebar-divider">
<!-- Nav Item - Tables -->
<li class="nav-item">
  <a class="nav-link" href="../logout.php">
    <i class="fas fa-sign-out-alt"></i>
    <span>Logout</span></a>
</li>

<!-- Divider -->
<hr class="sidebar-divider d-none d-md-block">

<!-- Sidebar Toggler (Sidebar) -->
<div class="text-center d-none d-md-inline">
  <button class="rounded-circle border-0" id="sidebarToggle"></button>
</div>

</ul>
<!-- End of Sidebar -->

<!-- Content Wrapper -->
<div id="content-wrapper" class="d-flex flex-column">

<!-- Main Content -->
<div id="content">

  <!-- Topbar -->
  <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

    <!-- Sidebar Toggle (Topbar) -->
    <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
      <i class="fa fa-bars"></i>
    </button>


    <!-- Topbar Navbar -->
    <ul class="navbar-nav ml-auto">

      <div class="topbar-divider d-none d-sm-block"></div>

      <!-- Nav Item - User Information -->
      <li class="nav-item dropdown no-arrow">
        <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          <span class="mr-2 d-none d-lg-inline text-gray-600 small"><?= $dataAtas['nama']; ?></span>
          <img class="img-profile rounded-circle" src="../../foto_user/student.png">
        </a>
        <!-- Dropdown - User Information -->
        <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
          <div class="dropdown-divider"></div>
          <a class="dropdown-item" href="#" data-toggle="modal" data-target="#logoutModal">
            <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
            Logout
          </a>
        </div>
      </li>
    </ul>

  </nav>