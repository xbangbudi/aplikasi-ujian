<!-- Sidebar -->
<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

<!-- Sidebar - Brand -->
<a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.html">
  <div class="sidebar-brand-icon rotate-n-15">
    <i class="fas fa-laugh-wink"></i>
  </div>
  <div class="sidebar-brand-text mx-3">TryOut CPNS</div>
</a>

<!-- Divider -->
<hr class="sidebar-divider">

<!-- Nav Item - Charts -->
<li class="nav-item">
  <a class="nav-link" href="index.php">
    <i class="fas fa-fw fa-tachometer-alt"></i>
    <span>Dashboard</span></a>
</li>

<!-- Divider -->
<hr class="sidebar-divider">

<!-- Heading -->
<div class="sidebar-heading">
  Paket
</div>

<!-- Nav Item - Pages Collapse Menu -->
<li class="nav-item">
  <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
    <i class="fas fa-book"></i>
    <span>Paket TryOut</span>
  </a>
  <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
    <div class="bg-white py-2 collapse-inner rounded">
      <h6 class="collapse-header">Paket</h6>
      <?php
      $query = "SELECT a.*, b.kd_tryout
      FROM user a
      LEFT JOIN ta_tryout b
      ON a.kd_paket = b.kd_paket
      AND a.kd_ujian = b.kd_ujian
      WHERE a.kode_reg='$username'";
      $cek = mysqli_query($conn, $query)or die(mysqli_error($conn));
        while($data = mysqli_fetch_array($cek)){
      ?>
      <a class="collapse-item" href="?page=tryout&id=<?=$data['kd_tryout']?>">TryOut <?=$no?></a>
      <!-- <a class="collapse-item" href="#">TryOut 2</a>
      <a class="collapse-item" href="#">TryOut 3</a>
      <a class="collapse-item" href="#">TryOut 4</a>
      <a class="collapse-item" href="#">TryOut 5</a> -->
      <?php
        }
      ?>
    </div>
  </div>
</li>

<!-- Divider -->
<hr class="sidebar-divider">

<div class="sidebar-heading">
  Pembahasan
</div>

<!-- Nav Item - Pages Collapse Menu -->
<li class="nav-item">
  <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
    <i class="fas fa-book"></i>
    <span>Paket TryOut</span>
  </a>
  <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
    <div class="bg-white py-2 collapse-inner rounded">
      <h6 class="collapse-header">Paket</h6>
      <?php
      $query = "SELECT a.*, b.kd_tryout
      FROM user a
      LEFT JOIN ta_tryout b
      ON a.kd_paket = b.kd_paket
      AND a.kd_ujian = b.kd_ujian
      WHERE a.kode_reg='$username'";
      $cek = mysqli_query($conn, $query)or die(mysqli_error($conn));
        while($data = mysqli_fetch_array($cek)){
      ?>
      <a class="collapse-item" href="?page=kunci&id=<?=$data['kd_tryout']?>">TryOut <?=$no?></a>
      <!-- <a class="collapse-item" href="#">TryOut 2</a>
      <a class="collapse-item" href="#">TryOut 3</a>
      <a class="collapse-item" href="#">TryOut 4</a>
      <a class="collapse-item" href="#">TryOut 5</a> -->
      <?php
        }
      ?>
    </div>
  </div>
</li>

<!-- Divider -->
<hr class="sidebar-divider">

<!-- Heading -->
<div class="sidebar-heading">
  Help
</div>

<!-- Nav Item - Charts -->
<li class="nav-item">
  <a class="nav-link" href="index.php?page=hubungi">
    <i class="fas fa-phone-alt"></i>
    <span>Hubungi Admin</span></a>
</li>

<hr class="sidebar-divider">
<!-- Nav Item - Tables -->
<li class="nav-item">
  <a class="nav-link" href="#">
    <i class="fas fa-sign-out-alt"></i>
    <span>Logout</span></a>
</li>

<!-- Divider -->
<hr class="sidebar-divider d-none d-md-block">

<!-- Sidebar Toggler (Sidebar) -->
<div class="text-center d-none d-md-inline">
  <button class="rounded-circle border-0" id="sidebarToggle"></button>
</div>

</ul>
<!-- End of Sidebar -->