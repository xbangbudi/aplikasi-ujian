<?php if(isset($_GET['id']) && isset($_GET['id_bank'])){
    $kd_user = $_GET['id'];
    $id_bank = $_GET['id_bank'];

    $query = "SELECT * FROM ref_bank WHERE id = $id_bank";
    $execQuery = mysqli_query($conn, $query);
    $data = mysqli_fetch_assoc($execQuery); 
    $query = "SELECT a.*, b.keterangan, c.jumlah, c.diskon
                FROM user a
                LEFT JOIN ref_paket b
                ON a.kd_paket = b.kd_paket
                AND a.kd_ujian = b.kd_ujian
                LEFT JOIN ta_harga_paket c
                ON b.kd_paket = c.kd_paket
                AND b.kd_ujian = c.kd_ujian
                WHERE a.id='$kd_user'";
    $cek = mysqli_query($conn, $query) or die(mysqli_error($conn));
    $data2 = mysqli_fetch_array($cek);
    $hitung = $data2['jumlah']*$data2['diskon']/100;
    $total =  $data2['jumlah']-$hitung;
?>
<div class="card">
    <div class="card-header">
        <h5 class="font-weight-bold text-uppercase">Form Upload Bukti Pembayaran</h5>
    </div>
    <div class="card-body">
        <div class="card">
            <div class="card-header">
                <h5 class="font-weight-bold text-inline text-uppercase"><?= $data['nama_bank'] ?>  <a class="btn btn-success btn-sm float-right" href="?page=pilih_bank&id=<?=$kd_user?>">Pilih Bank Lain</a></h5>
            </div>
            <div class="card-body">
                <p class="card-text">
                    Anda memilih <b><?= $data2['keterangan'] ?></b> dengan harga <b>Rp. <?= number_format($total,0,",","."); ?></b>,
                    silahkan transfer ke nomor rekening dibawah ini:<br>
                    <b>No. Rekening</b> : <?= $data['no_rek'] ?> <br> 
                    <b>Atas Nama</b> : <?= $data['atas_nama'] ?> 
                </p>
            </div>
            <div class="card-footer">
                <span class="text-danger text-uppercase"><b>Pastikan Anda Mengunggah Foto Bukti Pembayaran Yang Telah Di Ambil Dengan Jelas</b></span>
            </div>
        </div><br>
    <?php include "form_pembayaran.php" ?>
    </div>
</div>
<?php 
}else{
    echo 'Get out from here!';
}?>
