<?php if(isset($_GET['id'])){
    $kd_user = $_GET['id']; ?>
    <h5 align="center">Silahkan Pilih Rekening Tujuan Terlebih Dahulu</h5><hr>
    <div class="col-md-12 card-deck">
        <?php
            $query = "SELECT * FROM ref_bank";
            $execQuery = mysqli_query($conn, $query);
            while($data = mysqli_fetch_assoc($execQuery)){
        ?>
        <div class="col-md-6">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title text-bold"><?= $data['nama_bank'] ?></h4><hr>
                    <p class="card-text"><b>No. Rekening</b> : <?= $data['no_rek'] ?> <br> <b>Atas Nama</b> : <?= $data['atas_nama'] ?> </p>
                </div>
                <div class="card-footer">
                    <a class="btn btn-success btn-block btn-sm" href="?page=pembayaran&id=<?=$kd_user?>&id_bank=<?=$data['id']?>">Upload Bukti</a>
                </div>
            </div>
        </div>
            <br>
    <?php } ?>
    </div>

<?php 
}else{
    echo 'Get out from here!';
}?>
