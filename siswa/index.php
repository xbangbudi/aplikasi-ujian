<!DOCTYPE html>
<html class="no-js">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Login | Tryout CPNS</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="../register/kit/css/bootstrap.css">
        <link rel="stylesheet" href="../register/kit/css/font-awesome.css">
        <link rel="stylesheet" href="../register/kit/css/hover.css">
        <link rel="stylesheet" href="../register/kit/fonts/Linearicons-Free-v1.0.0/icon-font.min.css">
        <link rel="stylesheet" href="util.css">
        <link rel="stylesheet" href="main.css">
    </head>
    <body>
    <div class="limiter">
		<div class="container-login100">
			<div class="wrap-login100 p-l-50 p-r-50 p-t-77 p-b-30">
				<form class="login100-form validate-form" action="login.php" method="POST">
					<span class="login100-form-title p-b-55">
						Login Aplikasi
					</span>

					<div class="wrap-input100 validate-input m-b-16" data-validate = "Username Tidak Boleh Kosong">
						<input class="input100" type="text" name="username" placeholder="Username">
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<span class="lnr lnr-user"></span>
						</span>
					</div>

					<div class="wrap-input100 validate-input m-b-16" data-validate = "Password Tidak Boleh Kosong">
						<input class="input100" type="password" name="pass" placeholder="Password">
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<span class="lnr lnr-lock"></span>
						</span>
                    </div>
                    
					<div class="container-login100-form-btn p-t-25">
						<button class="login100-form-btn">
							Login
						</button>
					</div>

					<!-- <div class="text-center w-full p-t-115">
						<span class="txt1">
							Not a member?
						</span>

						<a class="txt1 bo1 hov1" href="#">
							Sign up now							
						</a>
					</div> -->
				</form>
			</div>
		</div>
	</div>
    </body>
    <script src="../register/kit/js/bootstrap.js"></script>
    <script src="../register/kit/js/jquery.js"></script>
    <script src="../register/kit/js/main.js"></script>
</html>