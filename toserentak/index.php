<?php
include "../config/koneksi.php";
session_start();
if($_SESSION['status'] != 'LOGIN'){
    header("location:../user");
}
 
?>
<!DOCTYPE html>

<html class="no-js">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="kit/css/bootstrap.css">
        <link rel="stylesheet" href="kit/css/font-awesome.css">
        <link rel="stylesheet" href="kit/css/hover.css">
        <style>

            body {
                font-family: calibri;
                background-color: #347aeb;
            }

            .kotak {
                border: 1px solid #f3f3f3;
                padding: 9px;
                border-radius: 5px;
                margin-top: -15px;
                box-shadow: 1px 3px 5px;
                background-color: white;
            }

            .nilai {
                border: 1px solid #dd1e31;
                background-color: #dd1e31;
                padding: 9px;
                border-radius: 5px;
                margin-top: -15px;
                color: white;
            }

            .scroll {
                width: 100%;
                background-color: whitesmoke;
                padding: 10px;
                overflow: auto;
                height: 300px;
                border-radius: 5px;
            }

            #tombol {
                margin-bottom: 10px;
            }

            .opsi {
                width: 100px;
            }

            .input-hidden {
                position: absolute;
                left: -9999px;
            }

            input[type=radio]:checked + label > img {
                border: 0 solid #fff;
                border-radius: 100%;
                box-shadow: 0 0 3px 3px blue;
            }

            input[type=radio] + label > img {
                border: 0 dashed #444;
                width: 150px;
                height: 150px;
                transition: 500ms all;
            }

            input[type=radio]:checked + label > img {
                transform: rotateZ(-10deg) rotateX(10deg);
            }

            .header_soal {
                border: 1px solid #347aeb;
                background-color: #347aeb;
                padding: 9px;
                border-radius: 10px;
                color: white;
                text-align: center;
            }

            .soal {
                padding: 20px;
            }
            .preloader {
                position: fixed;
                top: 0;
                left: 0;
                width: 100%;
                height: 100%;
                z-index: 9999;
                background-color: #fff;
            }
            .preloader .loading {
                position: absolute;
                left: 50%;
                top: 50%;
                transform: translate(-50%,-50%);
                font: 14px arial;
            }
        </style>
    </head>
    <body>
        <div class="preloader">
            <div class="loading">
                <img src="konten/preloader.gif" width="80">
                <p>Harap Tunggu</p>
            </div>
        </div>
    <?php
    if(isset($_SESSION["mulai_waktu"])){
        $telah_berlalu = time() - $_SESSION["mulai_waktu"];
        if($telah_berlalu==0){
            $_SESSION['st']=="habis";
        }
        }
       else {
        $_SESSION["mulai_waktu"] = time();
        $_SESSION['st']= "mulai";
        $telah_berlalu = 0;
        }
        if(isset($_SESSION['identitas'])){
            $id=$_SESSION['identitas'];
            $query=mysqli_query($conn,"SELECT * FROM akun WHERE no_reg='$id'");
            $r=mysqli_fetch_array($query);
            if($r['status']=="biasa"){
                $kuota=$r['kuota']-$r['terpakai'];
            }
            else{
                $kuota="selamanya";
            }
            if($kuota>0 || $kuota=="selamanya"){
                $sqli=mysqli_query($conn,"INSERT INTO peserta(id_user,status) VALUES('$id','')");
                $pakai=$r['terpakai']+1;
                if($sqli){
                    $sqlir=mysqli_query($conn,"UPDATE akun SET terpakai='$pakai' WHERE no_reg='$id' ");
                    if($sqlir){
                        $paket=mysqli_query($conn,"SELECT * FROM paketsoalserentak WHERE sta='aktif' ORDER BY RAND() LIMIT 1");
                        $rpaket=mysqli_fetch_array($paket);
                $idpaket=$rpaket['id_paket'];
                $wto=$rpaket['waktu'];
                $tm=explode(":",$wto);
                // if(isset($tm[0])){
                    $jam=$tm[0];
                // }
                // else{
                //     $jam=0;
                // }
                // if(isset($tm[1])){
                    $mnt=$tm[1];
                // }
                // else{
                //     $mnt=0;
                // }
                // if(isset($tm[2])){
                    $dt=$tm[2];
                // }
                // else{
                //     $dt=0;
                // }
                $tjam=$jam * 3600;
                $tmnt=$mnt * 60;
                $twaktu=$tjam+$tmnt+$dt;
                $qsoal_twk=mysqli_query($conn,"SELECT * FROM soalserentak WHERE id_paket='$idpaket' AND jenis='TWK'  ORDER BY RAND() LIMIT 35");
                $qsoal_tiu=mysqli_query($conn,"SELECT * FROM soalserentak WHERE id_paket='$idpaket' AND jenis='TIU'  ORDER BY RAND() LIMIT 30");
                $qsoal_tkp=mysqli_query($conn,"SELECT * FROM soalserentak WHERE id_paket='$idpaket' AND jenis='TKP'  ORDER BY RAND() LIMIT 35");
                $no=1;
                $a=0;
                    }
                }
                
        ?>
        <div
            class="jumbotron jumbotron-fluid"
            style="text-align: center; background-color : #347aeb; color: white; font-family: calibri;">
            <hr style="height: 20px; background-color: white;">
            <hr style="height: 5px; background-color: white; margin-top: -10px;">
            <h2><img src="img/logo2.png" style="margin-top: -125px;"></h2>
        </div>
        <div class="container-fluid" style="margin-top: -50px;">
            <div class="row">
                <!-- Bilah Kiri -->
                <div class="col-sm-9">
                <form id="jawaban" name="jawaban" action="" method="POST">
                    <div class="kotak">
                            <?php
                        while ($rtwk=mysqli_fetch_array($qsoal_twk)) {
                            ?>
                            <div id="tsoal" class="soal<?php echo $no; ?>">
                                <div class="header_soal">
                                    <b>Soal
                                        <?php echo $no; ?>
                                        - TWK</b>
                                </div>
                                <br>
                                <div class="soal">
                                    <?php echo $rtwk['soal'];?>
                                </div>
                                <br>
                                <div class="row" style="padding: 20px;">
                                    <!-- Pilihan A -->
                                    <div class="col-sm-4">
                                        <input
                                            type="radio"
                                            class="input-hidden"
                                            name="jb<?php echo $no; ?>"
                                            id="jba<?php echo $no; ?>"
                                            value="a&<?php echo $rtwk['id_soal'] ?>">
                                        <label for="jba<?php echo $no; ?>">
                                            <img src="img/a.png" alt="A" style="width: 20px; height: 20px;"/>
                                            <?php echo $rtwk['a']; ?>
                                        </label><br>
                                    </div>

                                    <!-- Pilihan B -->
                                    <div class="col-sm-4">
                                        <input
                                            type="radio"
                                            class="input-hidden"
                                            name="jb<?php echo $no; ?>"
                                            id="jbb<?php echo $no; ?>"
                                            value="b&<?php echo $rtwk['id_soal'] ?>">
                                        <label for="jbb<?php echo $no; ?>">
                                            <img src="img/b.png" alt="B" style="width: 20px; height: 20px;"/>
                                            <?php echo $rtwk['b']; ?>
                                        </label><br>
                                    </div>

                                    <!-- Pilihan C -->
                                    <div class="col-sm-4">
                                        <input
                                            type="radio"
                                            class="input-hidden"
                                            name="jb<?php echo $no; ?>"
                                            id="jbc<?php echo $no; ?>"
                                            value="c&<?php echo $rtwk['id_soal'] ?>">
                                        <label for="jbc<?php echo $no; ?>">
                                            <img src="img/c.png" alt="C" style="width: 20px; height: 20px;"/>
                                            <?php echo $rtwk['c']; ?>
                                        </label><br>
                                    </div>

                                    <!-- Pilihan D -->
                                    <div class="col-sm-4">
                                        <input
                                            type="radio"
                                            class="input-hidden"
                                            name="jb<?php echo $no; ?>"
                                            id="jbd<?php echo $no; ?>"
                                            value="d&<?php echo $rtwk['id_soal'] ?>">
                                        <label for="jbd<?php echo $no; ?>">
                                            <img src="img/d.png" alt="D" style="width: 20px; height: 20px;"/>
                                            <?php echo $rtwk['d']; ?>
                                        </label><br>
                                    </div>

                                    <!-- Pilihan E -->
                                    <div class="col-sm-4">
                                        <input
                                            type="radio"
                                            class="input-hidden"
                                            name="jb<?php echo $no; ?>"
                                            id="jbe<?php echo $no; ?>"
                                            value="e&<?php echo $rtwk['id_soal'] ?>">
                                        <label for="jbe<?php echo $no; ?>">
                                            <img src="img/e.png" alt="E" style="width: 20px; height: 20px;"/>
                                            <?php echo $rtwk['e']; ?>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <?php
                            $no++; $a++;
                        }
                        while ($rtiu=mysqli_fetch_array($qsoal_tiu)) {
                            ?>
                            <div id="tsoal" class="soal<?php echo $no; ?>">
                                <div class="header_soal">
                                    <b>Soal
                                        <?php echo $no; ?>
                                        - TWK</b>
                                </div>
                                <br>
                                <div class="soal">
                                    <?php echo $rtiu['soal'];?>
                                </div>
                                <br>
                                <div class="row" style="padding: 20px;">
                                    <!-- Pilihan A -->
                                    <div class="col-sm-4">
                                        <input
                                            type="radio"
                                            class="input-hidden"
                                            name="jb<?php echo $no; ?>"
                                            id="jba<?php echo $no; ?>"
                                            value="a&<?php echo $rtiu['id_soal'] ?>">
                                        <label for="jba<?php echo $no; ?>">
                                            <img src="img/a.png" alt="A" style="width: 20px; height: 20px;"/>
                                            <?php echo $rtiu['a']; ?>
                                        </label><br>
                                    </div>

                                    <!-- Pilihan B -->
                                    <div class="col-sm-4">
                                        <input
                                            type="radio"
                                            class="input-hidden"
                                            name="jb<?php echo $no; ?>"
                                            id="jbb<?php echo $no; ?>"
                                            value="b&<?php echo $rtiu['id_soal'] ?>">
                                        <label for="jbb<?php echo $no; ?>">
                                            <img src="img/b.png" alt="B" style="width: 20px; height: 20px;"/>
                                            <?php echo $rtiu['b']; ?>
                                        </label><br>
                                    </div>

                                    <!-- Pilihan C -->
                                    <div class="col-sm-4">
                                        <input
                                            type="radio"
                                            class="input-hidden"
                                            name="jb<?php echo $no; ?>"
                                            id="jbc<?php echo $no; ?>"
                                            value="c&<?php echo $rtiu['id_soal'] ?>">
                                        <label for="jbc<?php echo $no; ?>">
                                            <img src="img/c.png" alt="C" style="width: 20px; height: 20px;"/>
                                            <?php echo $rtiu['c']; ?>
                                        </label><br>
                                    </div>

                                    <!-- Pilihan D -->
                                    <div class="col-sm-4">
                                        <input
                                            type="radio"
                                            class="input-hidden"
                                            name="jb<?php echo $no; ?>"
                                            id="jbd<?php echo $no; ?>"
                                            value="d&<?php echo $rtiu['id_soal'] ?>">
                                        <label for="jbd<?php echo $no; ?>">
                                            <img src="img/d.png" alt="D" style="width: 20px; height: 20px;"/>
                                            <?php echo $rtiu['d']; ?>
                                        </label><br>
                                    </div>

                                    <!-- Pilihan E -->
                                    <div class="col-sm-4">
                                        <input
                                            type="radio"
                                            class="input-hidden"
                                            name="jb<?php echo $no; ?>"
                                            id="jbe<?php echo $no; ?>"
                                            value="e&<?php echo $rtiu['id_soal'] ?>">
                                        <label for="jbe<?php echo $no; ?>">
                                            <img src="img/e.png" alt="A" style="width: 20px; height: 20px;"/>
                                            <?php echo $rtiu['e']; ?>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <?php
                            $no++; $a++;
                        }
                        while ($rtkp=mysqli_fetch_array($qsoal_tkp)) {
                            ?>
                            <div id="tsoal" class="soal<?php echo $no; ?>">
                                <div class="header_soal">
                                    <b>Soal
                                        <?php echo $no; ?>
                                        - TWK</b>
                                </div>
                                <br>
                                <div class="soal">
                                    <?php echo $rtkp['soal'];?>
                                </div>
                                <br>
                                <div class="row" style="padding: 20px;">
                                    <!-- Pilihan A -->
                                    <div class="col-sm-4">
                                        <input
                                            type="radio"
                                            class="input-hidden"
                                            name="jb<?php echo $no; ?>"
                                            id="jba<?php echo $no; ?>"
                                            value="a&<?php echo $rtkp['id_soal'] ?>">
                                        <label for="jba<?php echo $no; ?>">
                                            <img src="img/a.png" alt="A" style="width: 20px; height: 20px;"/>
                                            <?php echo $rtkp['a']; ?>
                                        </label><br>
                                    </div>

                                    <!-- Pilihan B -->
                                    <div class="col-sm-4">
                                        <input
                                            type="radio"
                                            class="input-hidden"
                                            name="jb<?php echo $no; ?>"
                                            id="jbb<?php echo $no; ?>"
                                            value="b&<?php echo $rtkp['id_soal'] ?>">
                                        <label for="jbb<?php echo $no; ?>">
                                            <img src="img/b.png" alt="B" style="width: 20px; height: 20px;"/>
                                            <?php echo $rtkp['b']; ?>
                                        </label><br>
                                    </div>

                                    <!-- Pilihan C -->
                                    <div class="col-sm-4">
                                        <input
                                            type="radio"
                                            class="input-hidden"
                                            name="jb<?php echo $no; ?>"
                                            id="jbc<?php echo $no; ?>"
                                            value="c&<?php echo $rtkp['id_soal'] ?>">
                                        <label for="jbc<?php echo $no; ?>">
                                            <img src="img/c.png" alt="C" style="width: 20px; height: 20px;"/>
                                            <?php echo $rtkp['c']; ?>
                                        </label><br>
                                    </div>

                                    <!-- Pilihan D -->
                                    <div class="col-sm-4">
                                        <input
                                            type="radio"
                                            class="input-hidden"
                                            name="jb<?php echo $no; ?>"
                                            id="jbd<?php echo $no; ?>"
                                            value="d&<?php echo $rtkp['id_soal'] ?>">
                                        <label for="jbd<?php echo $no; ?>">
                                            <img src="img/d.png" alt="D" style="width: 20px; height: 20px;"/>
                                            <?php echo $rtkp['d']; ?>
                                        </label><br>
                                    </div>

                                    <!-- Pilihan E -->
                                    <div class="col-sm-4">
                                        <input
                                            type="radio"
                                            class="input-hidden"
                                            name="jb<?php echo $no; ?>"
                                            id="jbe<?php echo $no; ?>"
                                            value="e&<?php echo $rtkp['id_soal'] ?>">
                                        <label for="jbe<?php echo $no; ?>">
                                            <img src="img/e.png" alt="E" style="width: 20px; height: 20px;"/>
                                            <?php echo $rtkp['e']; ?>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <?php
                            $no++; $a++;
                        }
                        ?>
                        <input type="hidden" id="ist" name="waktu">
                        </form>
                        <br><br>
                        <div style="text-align: right;">
                            <a id="next" href="#" class="btn btn-success">Soal Berikutnya
                                <i class="fa fa-arrow-right"></i>
                            </a>
                            <a  id="kumpul"
                            data-toggle="modal"
                            data-target="#myModal" href="#" class="btn btn-warning">Kumpulkan
                                <i class="fa fa-check"></i>
                            </a>
                        </div>
                    </div>

                </div>
                <!-- Bilah Kanan -->
                <div class="col-sm-3">
                    <div class="kotak">
                        <div class="nilai" style="margin-top: 1px; margin-bottom: 10px;">
                            <b>Sisa Waktu : <span id="timer"></span></b>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="scroll">
                                    <div id="buton">
                                        <?php
                                        for ($ax=1; $ax < $no; $ax++) { 
                                            ?>
                                        <a id="tombol" href="#" class="btn btn-primary" rel="<?php echo $ax; ?>"><?php echo $ax; ?></a>
                                        <?php
                                        }
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="myModal">
            <div class="modal-dialog modal-lg modal-dialog-centered">
                <div class="modal-content">

                    <!-- Modal Header -->
                    <div class="modal-header">
                        <h4 class="modal-title">Skor Hasil</h4>
                        <a href="" onclick=" return document.location.href = '../user'" type="button" class="close" data-dismiss="modal">&times;</a>
                    </div>

                    <!-- Modal body -->
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-sm">
                                <div class="kotak">
                                    <!-- <h3>jawaban</h3> -->
                                    <div id="hasilx">
                                        Mohon ditunggu...
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- Modal footer -->
                        <div class="modal-footer">
                            <a href="" onclick=" return document.location.href = '../user'" class="btn btn-danger" data-dismiss="modal">Tutup</a>
                        </div>

                    </div>
                </div>
            </div>
        <script src="kit/js/bootstrap.js"></script>
        <script src="kit/js/jquery.js"></script>
        <script src="kit/countdown/js/jquery.plugin.min.js"></script>
        <script src="kit/countdown/js/jquery.countdown.js"></script>
        <script
                src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
            <script
                src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
        <script src="konten/ajax.js"></script>
        <script>
            function waktuHabis(){
             $('#myModal').modal('show');
                $.ajax({
            url : 'konten/jawab.php',
            type : 'POST',
            data : $('#jawaban').serialize(),
            success:function(hasils){
                $('#hasilx').html(hasils);
                console.log('berhasil');
            },
            error: function(){
                alert("Terjadi kesalahan!");
            }
        });
             }
            function hampirHabis(periods){
                var s=$("#timer").text();
             $("#ist").val(s);
             if($.countdown.periodsToSeconds(periods) == 60){
             $(this).css({color:"yellow"});
             }
             }
            $(function(){
             var waktu = <?php echo $twaktu; ?>;
             var sisa_waktu = waktu - <?php echo $telah_berlalu ?>;
             var longWayOff = sisa_waktu;
             $("#timer").countdown({
             until: longWayOff,
             compact:true,
             onExpiry:waktuHabis,
             onTick: hampirHabis
             });
            //  var s=$("#timer").text();
            //  $("#ist").val(s);
             })
            </script>
    </body>
</html>
<?php
                        
            }
        }
?>