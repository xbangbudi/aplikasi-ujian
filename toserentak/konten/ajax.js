$(document).ready(function(){
    $(".preloader").fadeOut();

    $('.kotak #tsoal:not(:first)').hide();
    $('#buton a:first').addClass('active');

    
    $('#buton a').click(function(){
        $('#buton a').removeClass('active');
        $(this).addClass('active');
        $('.kotak #tsoal').hide();
        var target=$(this).attr('rel');
        $('.kotak .soal'+target).show();
    });
    $('#buton a:not(:last)').click(function(){
        $('#next').removeClass('disabled');
    });
    $('#buton a:last').click(function(){
        $('#next').addClass('disabled');
    });
    $('#next').click(function(){
        var n =$('#buton .active').attr('rel');
        var s=parseFloat(n)+1;
        $('#buton a').removeClass('active');
        $('.kotak #tsoal').hide();
        $('.kotak .soal'+s).show();
        $('#buton a[rel="'+ s +'"]').addClass('active');
        if($('#buton a:last').attr('rel')==s){
            $('#next').addClass('disabled');
        }
    });
    $('#kumpul').click(function(){
        $.ajax({
            url : 'konten/jawab.php',
            type : 'POST',
            data : $('#jawaban').serialize(),
            success:function(hasils){
                $('#hasilx').html(hasils);
                console.log('berhasil');
            },
            error: function(){
                alert("Terjadi kesalahan!");
            }
        });
    });
});
